**How to work on this repository**

1. This is a public repository currently. Make a copy of this repository to create a private repository. Give us the access to this private repository.

2. git pull private repository in your local system (Please do not ask us how to do this.). You must do this in your local system.

2. run composer install

3. make appropriate changes in 'includes/classes/condata.ini.php' as per your local system configuration (DB name and local instance name)

4. Go to {your_local_instance}/cadmin/login.php. username: admin; password: 123

5. Make changes in code as required to complete this assigment. 

6. Push your changes in your private repository. We'll review them there.


**What is required to be done for this assignment?**

*Background: cadmin/invited.php displays a list of entries from table 'invited' and table 'invited_user_email' after matching them with 
table 'custom16_user_email'. Check it out to understand how it works. 'invited' and 'invited_user_email' tables in DB schema above are related to each other using sid field and form one group I. 
All other tables are related to each other using user_id or email fields; and form another group II.*

*What do you need to do?*

Requirement: You're required to prepare a new page to display list of users (investigators/ CSCs) similar to invited.php, but with changes as given below.

*I. New file's name is invitationlist.php.

*II. Search needs to be carried our between two groups (as given in "Background" above using following criteria. query may include criteria A-F. For each criteria, there are some options, which have been given there. For your understanding, challenge is criteria # F.  This criteria has 6 types of options, which are given as 1-6.

*III. Search criteria fields are (names of search fields are similar as given in invited.php)

A. unsubscribed? : yes, no, all

B. Failed email? : yes, no, all

C. Role: Investigator or CSC

D. Therapeutic area: as selected

E. Countries: as selected

F. User status: users fitting within other search criteria and status as defined below (This is the new criteria that you must focus to achieve)

Based on the search criteria, it will display following User Types (each successive criteria should not include results obtained using earlier criteria).

1. Signed up and profile completed (from group II)
2.  users given in 'invited' and 'invited_user_email' DB table, who have signed up, but not completed profile  (from group I and II)
3. users given in 'invited' and 'invited_user_email' DB table, who have subscribed emails, but not signed up yet.  (from group I and II)
4. users NOT given in 'invited' and 'invited_user_email' DB table, and have not signed up / subscribed, but are given in profile of any coordinators (from group II)
5. users given in 'invited' and 'invited_user_email' DB table, who have not signed up / subscribed, but are given in profile of any coordinators (from group I and II)
6. users given in 'invited' and 'invited_user_email' DB table, and not in any of above categories (from group I)

Columns in the Table: Data will be displayed in invitationlist.php as per following columns

1. ID: For user type 1 & 2, it will be "signed/<user_id>". For user type 3, it will be "subs/<subscription_id>". For user type 4, it will be "inv/<CSC_user_id>". For user type 4 & 5, it will be "sid/subsid".
2. Email: as given in respective table
3. Role: For user type 1 & 2, it will be as per DB. For user type 3&6 it will be as given in 'invited' DB table. For user type 4&5, it will be "investigator"
4. Batch: For user type 1, who are in invited DB table, it will be as given in invited DB table. For user type 1 (not in invited DB table) & 4, it will be empty. For others, it will be as given in 'invited' DB table.
5. Data type: For user type 1, who are in invited DB table, it will be as given in invited DB table. For user type 1 (not in invited DB table) & 4, it will be "self". For others, it will be as given in 'invited' DB table.
6. TherArea: For user type 1, it will be as per user profile. For user type 4, it will be as per profile of CSC. For others, it will be as given in 'invited' DB table.
6. Name: For user type 1, it will be as per user profile. For user type 4, it will be as per profile of CSC. For others, it will be as given in 'invited' DB table.
7. Surname: For user type 1, it will be as per user profile. For user type 4, it will be as per profile of CSC. For others, it will be as given in 'invited' DB table.
8. Country: For user type 1, it will be as per user profile. For user type 4, it will be as per profile of CSC. For others, it will be as given in 'invited' DB table.
9. Status: For user type 1, it will be "profiled" - hyperlinked to profile of that user. For user type 2, it will be "registered". For user type 3, it will be "subscribed". For user type 4, it will be "CSC inv" - hyperlinked to profile of that CSC. For user type 5, it will be "invited/CSCinv" - hyperlinked to profile of that CSC. For user type 6, it will be "invited".

**What are we looking for in you?**

While, you may complete this assignment in your own way, we'll be interested to see your approach, e.g.

1. Do use existing code or write new codes for everything?
2. For your new codes, how efficient and crafty they are?
3. The time you take to complete this. It means, do you believe in submitting a code very quickly, but incomplete/full of bugs. Or, do you take long time to complete? Or, somewhere in between?
4. How well do you test all scenarios described above? You're expected to test all scenario in various ways. You can try to change some DB values to test.
5. What is your level of git usage?

In case of any questions, you can contact us. But, please work to resolve small issues by yourselves. We're looking for a developer who is quite communicative, responsive and eager to find solutions. That's the only way any developer will be able to work for a long term with us. 