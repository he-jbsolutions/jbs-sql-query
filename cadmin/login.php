<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Clinical Study Network | Admin Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            
            <h3>Welcome to Clinical Study Network</h3>
            <p> </p>
            <p>Login in. To manage users and customers.</p>
            <form class="m-t" role="form" action="login-action.php" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username" required="" name="admin-user">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" name="admin-password">
                </div>
                <input type="submit" class="btn btn-primary block full-width m-b" name="Login" value="Login" id="Login">

                <!--<a href="#"><small>Forgot password?</small></a>-->
                <p class="text-muted text-center"><!--<small>Do not have an account?--></small></p>
                <!--<a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>-->
            </form>
           <!-- <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>-->
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
