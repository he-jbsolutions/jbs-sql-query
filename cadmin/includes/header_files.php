    <link rel="shortcut icon" href="favicon.ico">  

    <link href='://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900,900italic,300italic,300' rel='stylesheet' type='text/css'> 

    <link href='://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>

    <!-- Global CSS -->

    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">   

    <!-- Plugins CSS -->    

    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css">

    <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">

    <!-- Theme CSS -->

    <link id="theme-style" rel="stylesheet" href="assets/css/styles.css">
	<link id="theme-style2" rel="stylesheet" href="includes/custom.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

