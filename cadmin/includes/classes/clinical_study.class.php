<?php
require("../myzone/includes/classes/myzone.class.php");


class clinical_study extends myzone{
	

public $dbCon;

	
	function __construct()
	{
	  	$this->dbCon=$this->connectDB();
	  	
	  	# Include the Autoloader (see "Libraries" for install instructions)
		require '../vendor/autoload.php';
		// use Mailgun\Mailgun;

		# Instantiate the client.
		$this->mailgunClient = new \Http\Adapter\Guzzle6\Client();
		$this->mailgun = new \Mailgun\Mailgun(MAILGUN_API_KEY, $this->mailgunClient);
	}
 
 
function test(){
		
		
	echo "hello";



}
	  
function page(){
			$file = $_SERVER['PHP_SELF'];
			$c_page = substr($file, strrpos($file,"/") + 1);
			$pg = strrev($c_page);
			$page = ucfirst(strrev(substr($pg, strrpos($pg,".") + 1)));
			return $page;
}//END OF FUNCTION
function curentPage(){
			$c_page = substr($_SERVER['SCRIPT_FILENAME'], strrpos($_SERVER['SCRIPT_FILENAME'],"\\") + 1);
			$pg = strrev($c_page);	
			$page = strrev($pg);
			return $c_page;
}
		
function admin_login(){
	
	
	
	if(!isset($_SESSION['admin_email']) and empty($_SESSION['admin_email'])){
	//echo "--->".$_SESSION['admin_email'];
		//echo $_SESSION['lemail'];exit;
	header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	
	unset($_SESSION['admin_email']);
				
	setcookie("PHPSESSID","",time()-3600,"/");
	return false;
	//header("location:login-test");
	//exit;
	}else{
		
		
		return true;
}

	
	

}//END function

function admin_login_action($inArr){
	
	
			//$this->printr($inArr);
			//exit;
			$user=trim($inArr['admin-user']);
			$password=trim($inArr['admin-password']);
			//echo
			$sql = "SELECT * from `custom16_admin_users` WHERE user_name='$user' AND password='".md5($password)."'"; 
			//exit;
			
			$stmt = $this->dbCon->prepare($sql);
			$stmt->execute();
			if($row=$stmt->rowCount()){
			
				while ($row = $stmt->fetch())
			  	{
			  
					  $res[]=$row;
			  	} 
				return $res;
			}else{
					
					return false;
			}
			
	
}//END function


function user_list(){
		
			//exit;
			// sql query as per explanation from https://stackoverflow.com/questions/2111384/sql-join-selecting-the-last-records-in-a-one-to-many-relationship
			// $sql="SELECT * FROM `custom16_signup_users`";
			$sql="SELECT u.*, ps1.status, p.campaign_detail AS plan, p.id AS plan_id
					FROM custom16_signup_users u
					JOIN plan_subscriptions ps1 ON (u.user_id = ps1.user_id)
					LEFT OUTER JOIN plan_subscriptions ps2 ON (u.user_id = ps2.user_id AND 
					    (ps1.date_created < ps2.date_created OR ps1.date_created = ps2.date_created AND ps1.id < ps2.id))
					LEFT JOIN plans p ON `ps1`.`campaign_id` = `p`.`id` 
					WHERE ps2.id IS NULL";
			// echo $sql;exit;
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
}

function problem_reported() {
	$sql="SELECT * FROM `report_problem_new` ORDER BY project_id DESC";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
}

function access_list(){
		
			//exit;
			$sql="SELECT * FROM `access_log`";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $acc[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $acc;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
}

function user_detail($inarray,$type){
		
			//exit;
            if($type == 'service_provider'){
                $sql="SELECT custom16_serviceproviders_details.*, custom16_signup_users.*,
                countries.country_name as country_name
                FROM `custom16_serviceproviders_details` 
                LEFT JOIN countries
				ON countries.id = custom16_serviceproviders_details. country_of_work
                LEFT JOIN custom16_signup_users
			    ON (custom16_signup_users.user_id=custom16_serviceproviders_details. user_id)
                WHERE custom16_serviceproviders_details.user_id =".$inarray;
                
            }else if($type == 'clinical_stud_coordinator'){
                $sql="SELECT custom16_csd_details.*, custom16_signup_users.*,
                countries.country_name as country_name,
                group_concat(therapeutic_areas.name) as therapeutic_name
                FROM `custom16_csd_details` 
                LEFT JOIN countries
				ON countries.id = custom16_csd_details. country_of_work
                LEFT JOIN custom16_signup_users
			    ON (custom16_signup_users.user_id=custom16_csd_details. user_id)
                JOIN therapeutic_areas
		        ON find_in_set(therapeutic_areas. id ,custom16_csd_details. therpeutic_area_of_work)
                WHERE custom16_csd_details.user_id =".$inarray;
            
            }else if($type == 'trial_manager'){
                /*$sql="SELECT * FROM `custom16_trialmanagers_details` 
                LEFT JOIN custom16_signup_users
			    ON (custom16_signup_users.user_id=custom16_trialmanagers_details. user_id)
                WHERE custom16_trialmanagers_details.user_id =".$inarray;*/
                
                $sql="SELECT custom16_trialmanagers_details.*, custom16_signup_users.*, 
                countries.country_name as country_name,
                group_concat(therapeutic_areas.name) as therapeutic_name
                FROM `custom16_trialmanagers_details` 
                LEFT JOIN countries
				ON countries.id = custom16_trialmanagers_details. country_of_work
                LEFT JOIN custom16_signup_users
			    ON (custom16_signup_users.user_id=custom16_trialmanagers_details. user_id)
                JOIN therapeutic_areas
		        ON find_in_set(therapeutic_areas. id ,custom16_trialmanagers_details. therapeutic_area)
                WHERE custom16_trialmanagers_details.user_id =".$inarray;
                
                
            }else if($type == 'investigator'){
                /*$sql="SELECT * FROM `custom16_investigator_details` 
                LEFT JOIN custom16_signup_users
			    ON (custom16_signup_users.user_id=custom16_investigator_details. user_id)
                WHERE custom16_investigator_details.user_id =".$inarray;*/
                
                $sql="SELECT custom16_investigator_details.*, custom16_signup_users.*, 
                countries.country_name as country_name,
                group_concat(therapeutic_areas.name) as therapeutic_name
                FROM `custom16_investigator_details` 
                LEFT JOIN countries
				ON countries.id = custom16_investigator_details. country_of_work
                LEFT JOIN custom16_signup_users
			    ON (custom16_signup_users.user_id=custom16_investigator_details. user_id)
                JOIN therapeutic_areas
		        ON find_in_set(therapeutic_areas. id ,custom16_investigator_details. therpeutic_area_of_work)
                WHERE custom16_investigator_details.user_id =".$inarray;
            }
            
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
}

function in_trails_list($inarray){
			//exit;
            $sql="SELECT * FROM `custom16_in_trails` 
            LEFT JOIN custom16_investigator_details
		    ON (custom16_investigator_details.user_id=custom16_in_trails. user_id)
            WHERE custom16_in_trails.user_id =".$inarray;
                
            
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
}


function in_site_list($inarray){
			//exit;
            $sql="SELECT * FROM `custom16_in_sites` 
            LEFT JOIN custom16_investigator_details
		    ON (custom16_investigator_details.user_id=custom16_in_sites. user_id)
            WHERE custom16_in_sites.user_id =".$inarray;
                
            
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
}


function csd_site_list($inarray){
			//exit;
            $sql="SELECT * FROM `custom16_csd_in_sites` 
            LEFT JOIN custom16_csd_details
		    ON (custom16_csd_details.user_id=custom16_csd_in_sites. user_id)
            WHERE custom16_csd_in_sites.user_id =".$inarray;
                
            
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
}
function csd_invest_list($inarray){
			//exit;
            $sql="SELECT * FROM `custom16_csd_in_details` 
            LEFT JOIN custom16_csd_details
		    ON (custom16_csd_details.user_id=custom16_csd_in_details. user_id)
            WHERE custom16_csd_in_details.user_id =".$inarray;
                
            
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
}

function project_list(){
		$sql="SELECT custom16_clinical_projects.*,
		therapeutic_areas.name as therapeutic_name,
		group_concat(countries.country_name) as country
		FROM 
		custom16_clinical_projects
		LEFT JOIN therapeutic_areas
			ON (therapeutic_areas.id=custom16_clinical_projects. therapeutic_area)
		JOIN countries
		ON find_in_set(countries.id ,custom16_clinical_projects. regions)
		WHERE custom16_clinical_projects. Post = 1
        GROUP BY custom16_clinical_projects. project_id";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}

//Project Invitation List
function project_invitation_list(){
    $sql = "SELECT resp.project_id as proj_id,proj.title,
            COUNT(resp.project_id) as countProj,
            (SELECT reference.email FROM reference WHERE reference.email = resp.email LIMIT 1) as comment_email, (SELECT COUNT( DISTINCT reference.email) FROM reference WHERE reference.project_id = resp.project_id AND reference.comment_question <> '') as suplied,
            (SELECT COUNT( DISTINCT reference.email) FROM reference WHERE reference.project_id = resp.project_id AND reference.reference_text <> '') as refs,
            (SELECT COUNT(CASE r.user_action WHEN 'Yes' THEN 1 ELSE NULL END) FROM response as r
            LEFT JOIN custom16_email_subscription as sub ON sub.email=r.email
            WHERE r.created_at = (SELECT MAX(re.created_at) FROM response AS re WHERE re.email=sub.email AND re.project_id=resp.project_id)) as countYes,
            (SELECT COUNT(CASE r.user_action WHEN 'No' THEN 1 ELSE NULL END) FROM response as r
            LEFT JOIN custom16_email_subscription as sub ON sub.email=r.email
            WHERE r.created_at = (SELECT MAX(re.created_at) FROM response AS re WHERE re.email=sub.email AND re.project_id=resp.project_id)) as countNo
            FROM response as resp LEFT JOIN custom16_clinical_projects as proj ON resp.project_id=proj.project_id GROUP BY proj.project_id ORDER BY created_at ASC";
        $dbCon=$this->connectDB();
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();
        if($row=$stmt->rowCount()){
            $uns = $stmt->fetchAll();
            $this->closeDB($dbCon);
            return $uns;
        }else{
            $this->closeDB($dbCon);
            return false;
        }
}

//Project Invitation List
function project_invitation_responses_list($project,$status){
    $sql = "SELECT DISTINCT sub.email,us.profile_status
                FROM custom16_email_subscription as sub 
                LEFT JOIN reference as ref ON sub.email=ref.email 
                LEFT JOIN `custom16_signup_users` as us ON sub.email=us.email 
                LEFT JOIN response as resp ON resp.email=sub.email
                WHERE sub.unsubscribe=0 AND (ref.project_id='".$project."' OR  resp.project_id='".$project."')";
    if($status == 'Yes' || $status == 'No'){
        $sql .= " AND resp.user_action = '".$status."' AND resp.created_at=(SELECT MAX(created_at) FROM response AS r WHERE r.email = resp.email )";
    }
    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();
    if($row=$stmt->rowCount()){
        $result=$stmt->fetchAll();
        $this->closeDB($dbCon);
        return $result;
    }else{
        $this->closeDB($dbCon);
        return false;
    }
}

function getResponses($email,$project,$resp=false){
    $sql = "SELECT DISTINCT resp.ip,resp.created_at,resp.user_action
            FROM response as resp WHERE resp.email='".$email."' AND resp.project_id='".$project."' ";
    if($resp == 'Yes' || $resp == 'No'){
        $sql.="AND resp.user_action='".$resp."' ";
    }
    $sql.="ORDER BY created_at DESC";
    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();
    if($row=$stmt->rowCount()){
        $result=$stmt->fetchAll();

        $this->closeDB($dbCon);
        return $result;
    }else{
        $this->closeDB($dbCon);
        return false;
    }
}

function getCommentsReferences($email,$project,$comment=false,$refer=false){
    $sql = "SELECT DISTINCT ref.comment_question,ref.ip,ref.reference_text,ref.created_at
                FROM reference as ref 
                WHERE ref.email='".$email."' AND ref.project_id='".$project."' ";
//    if($comment){
//        $sql.="AND ref.comment_question <> '' ";
//    }
//    if($refer){
//        $sql.="AND ref.reference_text <> '' ";
//    }
    $sql.="ORDER BY created_at DESC";
    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();
    if($row=$stmt->rowCount()){
        $result=$stmt->fetchAll();

        $this->closeDB($dbCon);
        return $result;
    }else{
        $this->closeDB($dbCon);
        return false;
    }
}

function invitations($start_from, $finish_at){
		$sql="SELECT * FROM unsubscribed ORDER BY sid ASC LIMIT $start_from, $finish_at";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();			
			if($row=$stmt->rowCount()){				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $uns[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $uns;
			}else{
				$this->closeDB($dbCon);
				return false;				
				//echo "Not found"; exit;
			}
}

function unsubscribed($start_from, $finish_at){
		$sql="SELECT * FROM unsubscribed WHERE unsubscribe = 1 ORDER BY sid ASC LIMIT $start_from, $finish_at";
		//exit;
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();			
			if($row=$stmt->rowCount()){				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $uns[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $uns;
			}else{
				$this->closeDB($dbCon);
				return false;				
				//echo "Not found"; exit;
			}
}

function subscribed($start_from, $finish_at){
		$sql="SELECT * FROM unsubscribed WHERE unsubscribe = 0 ORDER BY sid ASC LIMIT $start_from, $finish_at";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();			
			if($row=$stmt->rowCount()){				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $uns[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $uns;
			}else{
				$this->closeDB($dbCon);
				return false;				
				//echo "Not found"; exit;
			}
}

function newsletter($start_from, $finish_at){
		$sql="SELECT * FROM custom16_email_subscription ORDER BY sid ASC LIMIT $start_from, $finish_at";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $uns[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $uns;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}
    function project_response_list(){
		$sql="SELECT project_response.*,custom16_clinical_projects.title,custom16_clinical_projects.summary,s.prefix,s.name,s.last_name,s.role as role, i.prefix as inv_pre, i.name as inv_name, i.last_name as inv_surname
		FROM 
		project_response
        LEFT JOIN custom16_clinical_projects
			ON (custom16_clinical_projects.project_id=project_response. project_id)
            
        LEFT JOIN custom16_signup_users s
			ON (s.user_id=project_response. user_id)
        
        LEFT JOIN custom16_signup_users i
			ON (i.user_id=project_response. investigator)
        GROUP BY project_response. id";
        // echo $sql; exit;
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}
    
    function get_therapeutic_areas(){
	
	$sql="SELECT * FROM therapeutic_areas";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			//$row = $stmt->fetch();
			while ($row = $stmt->fetch())
			  	{
			  
					  $res[]=$row;
			  	} 
			$this->closeDB($dbCon);
			return $res;
			
	
	
}

function get_country_list(){
	
	$sql="SELECT * FROM countries ORDER BY country_name ASC";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			//$row = $stmt->fetch();
			while ($row = $stmt->fetch())
			  	{
			  
					  $res[]=$row;
			  	} 
			$this->closeDB($dbCon);
			return $res;
	
}
 function save_project($inArr){
	
		/*$this->printr($inArr);
		exit;*/
		$therapeutic_areas=implode(",",$inArr['therapeutic_area']);
		//$geographic_reasons=implode(",",$inArr['geographic_reasons']);
		$projectid=$this->MaxId("custom16_clinical_projects","project_id");
        
        $country_of_project=implode(",",$inArr['country_of_project']);
		
		/*$sql = "INSERT INTO custom16_trialmanagers_details (`det_id`, `user_id`, `name_of_organization`, 
		`address_of_organization`, `country_of_work`, `therapeutic_area`, `geographic_reasons`, `accept_terms`)
		  VALUES(:detid,:userid,:orgname,:orgaddress,:country,:tarea,:greason,:terms)";*/
		  
		$sql="INSERT INTO `custom16_clinical_projects` (`project_id`, `tm_proj_id`, `tm_id`, `title`, `therapeutic_area`,`summary` ,`regions`, `number_of_sites_needed`, `status`, `ab`, `cb`, `cbd`) VALUES (:projectid, :tmprojid, :tmid, :title, :tarea, :summary,:regions, :sites, :status, :ab, :cb, :cbd)" ;
				
				// $ab=0;
				$cb=0;
				$dbCon=$this->connectDB();
				$q = $dbCon->prepare($sql);
			// print_r($sql)	;exit;
				
				$cbd="NOW()";
				$sql="INSERT INTO `custom16_clinical_projects` (`project_id`, `tm_proj_id`, `tm_id`, `title`, `therapeutic_area`,`summary` ,`regions`, `number_of_sites_needed`, `status`, `ab`, `cb`, `cbd`) VALUES (:projectid, :tmprojid, :tmid, :title, :tarea, :summary,:regions, :sites, :status, :ab, :cb, :cbd)" ;
		
					$q->bindParam(":projectid",$projectid,PDO::PARAM_INT);
					$q->bindParam(":tmprojid",$inArr['tm_proj_id'],PDO::PARAM_INT);
					$q->bindParam(":tmid",$inArr['tm_id'],PDO::PARAM_INT);
					$q->bindParam(":title",$inArr['project_title'],PDO::PARAM_STR);
					$q->bindParam(":tarea",$therapeutic_areas,PDO::PARAM_STR);
                    $q->bindParam(":summary",$inArr['summary'],PDO::PARAM_STR);
                    $q->bindParam(":regions",$country_of_project,PDO::PARAM_STR);
                    $q->bindParam(":sites",$inArr['number_of_sites'],PDO::PARAM_INT);
					$q->bindParam(":status",$inArr['status'],PDO::PARAM_INT);
					// $q->bindParam(":post",$inArr['post'],PDO::PARAM_INT);
					
					$q->bindParam(":ab",$inArr['period'],PDO::PARAM_INT);
					
					
					$q->bindParam(":cb",$cb,PDO::PARAM_INT);
					$q->bindParam(":cbd",$cbd,PDO::PARAM_STR);
		
					if($q->execute()){
    					$this->closeDB($dbCon);
    				    return $projectid;
					}else{
						$this->printr(var_dump($q->errorInfo()));
					$this->printr(var_dump($q->errorCode()));
					exit;
    				
    				    return false;
    				}
	}  
 
function approved_project($id){
		$sql="SELECT custom16_clinical_projects.*,
		therapeutic_areas.name as therapeutic_name,
		group_concat(countries.country_name) as country
		FROM 
		custom16_clinical_projects
		LEFT JOIN therapeutic_areas
			ON (therapeutic_areas.id=custom16_clinical_projects. therapeutic_area)
		JOIN countries
		ON find_in_set(countries.id ,custom16_clinical_projects. regions)
		WHERE custom16_clinical_projects.tm_proj_id =".$id."
		GROUP BY custom16_clinical_projects. project_id";
		//print_r($sql);exit;
		
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $posted[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $posted;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}	

	function modified_project($id){
		$sql="SELECT modify_trialmanager_projects.*,
		therapeutic_areas.name as therapeutic_name,
		group_concat(countries.country_name) as country
		FROM 
		modify_trialmanager_projects
		LEFT JOIN therapeutic_areas
			ON (therapeutic_areas.id=modify_trialmanager_projects. therapeutic_area)
		JOIN countries
		ON find_in_set(countries.id ,modify_trialmanager_projects. regions)
		WHERE modify_trialmanager_projects. project_id = ".$id."
		GROUP BY modify_trialmanager_projects. mod_id";
		//print_r($sql);exit;
		
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $mod[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $mod;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}
	
function modify_project($inArr){
	
		/*$this->printr($inArr);
		exit;*/
		$therapeutic_areas=implode(",",$inArr['therapeutic_area']);
		//$geographic_reasons=implode(",",$inArr['geographic_reasons']);
		//$projectid=$this->MaxId("custom16_trialmanager_projects","project_id");
        $modid=$this->MaxId("modify_trialmanager_projects","mod_id");
		// print_r($modid);exit;
        $country_of_project=implode(",",$inArr['country_of_project']);
		
		/*$sql = "INSERT INTO custom16_trialmanagers_details (`det_id`, `user_id`, `name_of_organization`, 
		`address_of_organization`, `country_of_work`, `therapeutic_area`, `geographic_reasons`, `accept_terms`)
		  VALUES(:detid,:userid,:orgname,:orgaddress,:country,:tarea,:greason,:terms)";*/
		  
		$sql="INSERT INTO `modify_trialmanager_projects` (`mod_id`, `comments`, `project_id`, `tm_id`, `title`, `therapeutic_area`,`summary` ,`regions`, `number_of_sites_needed`, `period`) VALUES (:modid, :comments, :projectid, :tmid, :title, :tarea, :summary,:regions, :sites, :period)" ;
				//print_r($sql);exit;
				
				$dbCon=$this->connectDB();
				$q = $dbCon->prepare($sql);
				
					$q->bindParam(":modid",$modid,PDO::PARAM_INT);
					$q->bindParam(":comments",$inArr['comments'],PDO::PARAM_INT);
					$q->bindParam(":projectid",$inArr['tm_proj_id'],PDO::PARAM_INT);
					$q->bindParam(":tmid",$inArr['tm_id'],PDO::PARAM_INT);
					$q->bindParam(":title",$inArr['project_title'],PDO::PARAM_STR);
					$q->bindParam(":tarea",$therapeutic_areas,PDO::PARAM_STR);
                    $q->bindParam(":summary",$inArr['summary'],PDO::PARAM_STR);
                    $q->bindParam(":regions",$country_of_project,PDO::PARAM_STR);
                    $q->bindParam(":sites",$inArr['number_of_sites'],PDO::PARAM_INT);
					$q->bindParam(":period",$inArr['period'],PDO::PARAM_INT);
		
					if($q->execute()){
    					$this->closeDB($dbCon);
						//echo "done"; exit;
    				    return $modid;
					/*$this->printr(var_dump($q->errorInfo()));
					$this->printr(var_dump($q->errorCode()));
					exit;*/
    				}else{
    				    return false;
    				}
	}  
    
 

function trialmanager_project_list(){
		$sql="SELECT custom16_trialmanager_projects.*,
		therapeutic_areas.name as therapeutic_name,
		group_concat(countries.country_name) as country
		FROM 
		custom16_trialmanager_projects
		LEFT JOIN therapeutic_areas
			ON (therapeutic_areas.id=custom16_trialmanager_projects. therapeutic_area)
		JOIN countries
		ON find_in_set(countries.id ,custom16_trialmanager_projects. regions)
		GROUP BY custom16_trialmanager_projects. project_id";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	} 
	
function search_investigators($therapeutic_area,$countries, $start_from){
	$areas = explode(",",$therapeutic_area);
	$counter = 1;
	$qery_area = "SELECT user_id, country_of_work, ques1, ques2, qualifications, other_qual, therpeutic_area_of_work, sub_therapeutic_area_of_work, start_working, medica_practice_exp, number_of_trials, name, country_name 
	FROM custom16_investigator_details a
	LEFT JOIN therapeutic_areas
		ON (therapeutic_areas.id=a. therpeutic_area_of_work)
	JOIN countries
	ON find_in_set(countries.id ,a. country_of_work)
	WHERE (";
	foreach($areas as $area){
		$qery_area .= " FIND_IN_SET($area,a.therpeutic_area_of_work)>0 ";
		if($counter < count($areas)){
			$qery_area .= "OR ";
		}
		$counter++;
	}
	$qery_area .= ") AND (";
	$countr = explode(",",$countries);
	$counter = 1;
	foreach($countr as $country){
		$qery_area .= " a.country_of_work = ".$country." ";
		if($counter < count($countr)){
			$qery_area .= "OR ";
		}
		$counter++;
	}
	$qery_area .= ") ORDER BY name ASC LIMIT $start_from, 10";
	
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($qery_area);
			$stmt->execute();
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
			  //echo $row['0'];exit;
					  $res[]=$row;
					  
			  	}
				 //echo "found"; exit;
					
					return $res;
			
			}else{
				return false;
				
				//echo "Not found"; exit;
			}
	
	
	
}//END FUNCTION

function mailbatch_upload($inArr){ 
	$dbCon=$this->connectDB();
	//$this->printr($inArr);
	//echo "Hello";
	// print_r($_FILES['uploaded_file']);exit;
	$filename = basename($_FILES['uploaded_file']['name']);
	$openfilename = $_FILES['uploaded_file']['tmp_name'];
	// echo $filename; echo $openfilename; exit;
	$ext = substr($filename, strrpos($filename, '.') + 1);
    $size = $_FILES['uploaded_file']['size'];
	// echo $ext;echo '<br />'; echo $size; exit;
	if($ext!= 'csv'){
		echo 'ext not ok'; exit;
		return false;
	} else if(!$size > 100000){
		echo "size not ok"; exit;
		return false;
	} else {
		// echo 'size and ext ok'; exit;
		$file = fopen($openfilename, "r");
        // print_r($file); exit;
        while(! feof($file))
		{
			while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
			{
				//print_r($emapData);
				//exit();
				// $sql = "INSERT into unsubscribed(sid,p_name,p_price,p_reason) values ('$emapData[0]','$emapData[1]','$emapData[2]','$emapData[3]')";
				$sql="INSERT INTO `unsubscribed` (`sid`, `mail_batch`, `unsubscribe`,`date` ) VALUES (:sid, :batch, :unsub, :dt)" ;
				$q = $dbCon->prepare($sql);
					$q->bindParam(":sid",$emapData[0],PDO::PARAM_INT);
					$q->bindParam(":batch",$emapData[1],PDO::PARAM_STR);
					$q->bindParam(":unsub",$emapData[2],PDO::PARAM_INT);
					$q->bindParam(":dt",$emapData[3],PDO::PARAM_INT);
					if (!$q->execute()){
					$this->printr(var_dump($q->errorInfo()));
					$this->printr(var_dump($q->errorCode()));
					exit;
					}
			}
		}
        fclose($file);
		$this->closeDB($dbCon);
		// echo 'CSV File has been successfully Imported';
		// exit;
        return true;
    }
}
function emaillist_upload($inArr){ 
	$dbCon=$this->connectDB();
	//$this->printr($inArr);
	//echo "Hello";
	// print_r($_FILES['uploaded_file']);exit;
	$filename = basename($_FILES['uploaded_file']['name']);
	$openfilename = $_FILES['uploaded_file']['tmp_name'];
	// echo $filename; echo $openfilename; exit;
	$ext = substr($filename, strrpos($filename, '.') + 1);
    $size = $_FILES['uploaded_file']['size'];
	// echo $ext;echo '<br />'; echo $size; exit;
	if($ext!= 'csv'){
		echo 'ext not ok'; exit;
		return false;
	} else if(!$size > 100000){
		echo "size not ok"; exit;
		return false;
	} else {
		// echo 'size and ext ok'; exit;
		$file = fopen($openfilename, "r");
        // print_r($file); exit;
        while(! feof($file))
		{
			while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
			{
				//print_r($emapData);
				//exit();
				// $sql = "INSERT into unsubscribed(sid,p_name,p_price,p_reason) values ('$emapData[0]','$emapData[1]','$emapData[2]','$emapData[3]')";
				$sql="INSERT INTO `custom16_email_subscription` (`sid`, `email`, `date`, `email_status`, `unsubscribe`,`unsub_date`, `gift1`, `feedback1` ) VALUES (:sid, :batch, :dt, :ver, :unsub, :unsubdt, :gift1, :feed1)" ;
				$q = $dbCon->prepare($sql);
					$q->bindParam(":sid",$emapData[0],PDO::PARAM_INT);
					$q->bindParam(":batch",$emapData[1],PDO::PARAM_STR);
					$q->bindParam(":dt",$emapData[2],PDO::PARAM_INT);
					$q->bindParam(":ver",$emapData[3],PDO::PARAM_STR);
					$q->bindParam(":unsub",$emapData[4],PDO::PARAM_INT);
					$q->bindParam(":unsubdt",$emapData[5],PDO::PARAM_INT);
					$q->bindParam(":gift1",$emapData[6],PDO::PARAM_INT);
					$q->bindParam(":feed1",$emapData[7],PDO::PARAM_INT);
					if (!$q->execute()){
					$this->printr(var_dump($q->errorInfo()));
					$this->printr(var_dump($q->errorCode()));
					exit;
					}
			}
		}
        fclose($file);
		$this->closeDB($dbCon);
		// echo 'CSV File has been successfully Imported';
		// exit;
        return true;
    }
}

function search_investigators_count($therapeutic_area,$countries){
	$areas = explode(",",$therapeutic_area);
	$counter = 1;
	$qery_count = "SELECT COUNT(user_id) FROM custom16_investigator_details a WHERE (";
	foreach($areas as $area){
		$qery_count .= " FIND_IN_SET($area,a.therpeutic_area_of_work)>0 ";
		if($counter < count($areas)){
			$qery_count .= "OR ";
		}
		$counter++;
	}
	$qery_count .= ") AND (";
	$countr = explode(",",$countries);
	$counter = 1;
	foreach($countr as $country){
		$qery_count .= " a.country_of_work = ".$country." ";
		if($counter < count($countr)){
			$qery_count .= "OR ";
		}
		$counter++;
	}
	$qery_count .= ")";
	//print_r ($qery_count);exit;
	//echo
			$dbCon=$this->connectDB();
			$q_count = $dbCon->prepare($qery_count);
			$q_count->execute();
			
			if($que_count=$q_count->rowCount()){
				
				//echo "found";exit;
				while ($que_count = $q_count->fetch())
			  	{
			  //echo $row['0'];exit;
					  $que=$que_count;
					  
			  	}
				//print_r($que); exit;
					return $que;
					
			} else {
				return false;
			}
	
	
	
}//END FUNCTION

	
 function project_details($project_id){
            //exit;
			/*$sql="SELECT * FROM `custom16_clinical_projects`";exit;*/
            
            /*$sql="select
            custom16_clinical_projects.*,
            group_concat(therapeutic_areas.name) as therapeutic_name,
            group_concat(countries.country_name) as country
            from
            custom16_clinical_projects
            join therapeutic_areas on find_in_set(therapeutic_areas.id, custom16_clinical_projects.therapeutic_area)
            join countries on find_in_set(countries.id, custom16_clinical_projects.regions)
            WHERE project_id = ".$project_id."
            group by
            custom16_clinical_projects.project_id";*/
			
			$sql="SELECT custom16_clinical_projects.*,
			therapeutic_areas.name as therapeutic_name,
			group_concat(countries.country_name) as country
			FROM 
			custom16_clinical_projects
			LEFT JOIN therapeutic_areas
				ON (therapeutic_areas.id=custom16_clinical_projects. therapeutic_area)
			JOIN countries
			ON find_in_set(countries.id ,custom16_clinical_projects. regions)
			WHERE project_id = ".$project_id."
			GROUP BY custom16_clinical_projects. project_id";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				//echo "Not found"; exit;
			}
}

function trial_project_details($project_id){
            
			
			$sql="SELECT custom16_trialmanager_projects.*,
			therapeutic_areas.name as therapeutic_name,
			group_concat(countries.country_name) as country
			FROM 
			custom16_trialmanager_projects
			LEFT JOIN therapeutic_areas
				ON (therapeutic_areas.id=custom16_trialmanager_projects. therapeutic_area)
			JOIN countries
			ON find_in_set(countries.id ,custom16_trialmanager_projects. regions)
			WHERE project_id = ".$project_id."
			GROUP BY custom16_trialmanager_projects. project_id";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				//echo "Not found"; exit;
			}
}

function change_status($inarray){
    $sql = "UPDATE `custom16_clinical_projects` SET status='".($_POST['status_change'])."' WHERE project_id=".$_POST['project_id']; 
	$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res=$row;
			  	}
				 $this->closeDB($dbCon);
				return true;
			}else{
				$this->closeDB($dbCon);
				return false;
				//echo "Not found"; exit;
			}		
}
function sendModificationEmail($post, $mid)
    {
        $tmid = $post['tm_id'];
        $p = $post['tm_proj_id'];
        $t               = time(); //echo '<pre>';print_r($countAns);echo '</pre>';
        $projectID       = $p*267;
        $ti              = $t.$projectID;
        $projectid            = base64_encode($ti);
        $link = SITE_URL."myzone/view_project_mod.php?p=".$projectid;
        $sql="SELECT * from `custom16_signup_users` WHERE user_id = ".$tmid;
        $dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		
		if($row=$stmt->rowCount()){
			
			// echo "found";exit;
			while ($row = $stmt->fetch())
		  	{
				  $res=$row;
		  	}
			 $this->closeDB($dbCon);
			$tmname = $res['name'];
			$to = trim($res['email']);
	        $subject = 'Credevo: Modify Your Posted Project';	        
	        $body_html = $this->LoadEmailTemplate('mail_modifyProject.html');
	        $body_html = str_replace('%tmName%', $tmname, $body_html);
			$body_html = str_replace('%title%', $post['project_title'], $body_html);
			$body_html = str_replace('%comments%', $post['comments'], $body_html);
			$body_html = str_replace('%link%', $link, $body_html);
	        $body_html = str_replace('%tmProjectId%', $p, $body_html);
	        $body_html = str_replace('%mid%', $mid, $body_html);
			$body_html = str_replace('%email%', $to, $body_html);

	        $body = "Dear ".$tmname."! \r\n\r\nYour posted project was reviewed and there are some modifications being recommended. Here are the comments from review:'".$post['comments']."'. Please click here to view and post modified project: ".$link.". - Team Credevo!";

	        # Make the call to the client.
	        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
	            // 'from'    => 'Gracey C. <gracey.c@'.MAILER_DOMAIN.'>',
	            'from'    => 'Gracey C. <gracey.c@credevo.com>',
	            'to'      => $to,
	            'bcc'     => 'gm@clinicalstudydesign.com',
	            'subject' => $subject,
	            'text'    => $body,
	            'html'    => $body_html
	        ));

	        $id = $result->http_response_body->id;
	        if ($id) {
				return true;
			}else{
				return false;
			}
		}else{
			$this->closeDB($dbCon);
			return false;
			//echo "Not found"; exit;
		}

        
    }

function sendApprovalEmail($post, $pid)
    {
        // echo '<pre>';print_r($post);print_r($pid);echo '</pre>';exit;
        $tmid = $post['tm_id'];
        $p = $post['tm_proj_id'];
        $t               = time(); //echo '<pre>';print_r($countAns);echo '</pre>';
        $projectID       = $p*267;
        $ti              = $t.$projectID;
        $projectid            = base64_encode($ti);
        $link = SITE_URL."myzone/my_project?p=".$projectid;
        $sql="SELECT * from `custom16_signup_users` WHERE user_id = ".$tmid;
        $dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		
		if($row=$stmt->rowCount()){
			
			// echo "found";exit;
			while ($row = $stmt->fetch())
		  	{
				  $res=$row;
		  	}
			 $this->closeDB($dbCon);
			$tmname = $res['name'];
			$to = trim($res['email']);
	        $subject = 'Credevo: Your Project Has Been Approved And Posted';	        
	        $body_html = $this->LoadEmailTemplate('mail_approvePostedProject.html');
	        $body_html = str_replace('%title%', $post['project_title'], $body_html);
			$body_html = str_replace('%tmName%', $tmname, $body_html);
			$body_html = str_replace('%link%', $link, $body_html);
	        $body_html = str_replace('%tmProjectId%', $p, $body_html);
	        $body_html = str_replace('%pid%', $pid, $body_html);
			$body_html = str_replace('%email%', $to, $body_html);
	        
	        $body = "Dear ".$tmname."! \r\n\r\nYour project was reviewed and approved to be posted on Credevo. Please click here to view your project: ".$link.". - Team Credevo!";

	        # Make the call to the client.
	        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
	            // 'from'    => 'Gracey C. <gracey.c@'.MAILER_DOMAIN.'>',
	            'from'    => 'Gracey C. <gracey.c@credevo.com>',
	            'to'      => $to,
	            'bcc'     => 'gm@clinicalstudydesign.com',
	            'subject' => $subject,
	            'text'    => $body,
	            'html'    => $body_html
	        ));

	        $id = $result->http_response_body->id;
	        if ($id) {
				return true;
			}else{
				return false;
			}
		}else{
			$this->closeDB($dbCon);
			return false;
			//echo "Not found"; exit;
		}

        
    }
   
   function invitationList($start_from, $finish_at,$therArea,$roleDetails,$countryDetails,$companyDetails,$user_type, $failed_type){
       ini_set('max_execution_time', 3000);
   		$fields = [[$therArea,"therArea"],[$roleDetails,"role"],[$countryDetails,"country"],[$companyDetails,"company"]];
   		$types = [[$user_type,"unsubscribe"],[$failed_type,"failStatus"]];
       $sql="SELECT i.sid, iu.id, s.user_id, s.role AS signed_role, s.profile_status, s.name, s.last_name, s.prefix, iu.email, iu.role, i.mail_batch, i.therArea, i.lastName, i.firstName, i.country, iu.failStatus, iu.dataType, i.company, u.unsubscribe as unsubscribe1, ur.unsubscribe as unsubscribe2, u.date as date1, ur.date as date2
			FROM `invited_user_email` iu
			LEFT JOIN invited i
			ON 
			 i.sid = 
			(
			    CASE 
			        WHEN iu.refer <> '0' THEN iu.refer
			        ELSE iu.sid
			    END
			)
			LEFT JOIN unsubscribed u
			ON u.sid = iu.sid
			LEFT JOIN unsubscribed ur
			ON ur.sid = iu.refer
			LEFT JOIN signup_users s
			ON iu.email = s.email
			WHERE ";
			foreach($fields as $fieldArea){
				$field = $fieldArea[0];
				$column = $fieldArea[1];
				$counter = 1;
				if ($field != "All") {
					$i = "i";
					if ($column == "role"){
						$i = "iu";
					}
                /* Old version */

//					$sql .= "(";
//					foreach($field as $area){
//						$sql .= "($i.$column = '".$area."')";
//						if($counter < count($field)){
//
//							$sql .= " OR ";
//							$counter++;
//						} else {
//							$sql .= ") AND ";
//						}
//					}

                /* New Version */
                    $sql .= $i.'.'.$column." IN ('".implode("', '",$field)."') AND ";
                }
			}
//			echo "<pre>";var_dump($types);die;
			foreach($types as $typeArea){
				$type = $typeArea[0];
				$column = $typeArea[1];
				$counter = 1;
				if ($type != 2) {
					if ($column == "failStatus"){
						$sql .= "(iu.$column = '".$type."') AND ";
					} else {
					$sql .= "((
							    CASE 
							        WHEN u.date > ur.date THEN u.$column
							        ELSE ur.$column
							    END
							) = '".$type."') AND ";
					}
					
				}
			}
			$sql .= "iu.sid > $start_from
				ORDER BY iu.sid ASC LIMIT 0, $finish_at";
			 // echo $sql; exit;
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			if($row=$stmt->rowCount()){
				//echo "found";exit;
                /* Old version */
//				while ($row = $stmt->fetch())
//			  	{
//					  $uns[]=$row;
//			  	}
                /* New Version */
                $uns=$stmt->fetchAll();
				 $this->closeDB($dbCon);
				return $uns;
			}else{
				$this->closeDB($dbCon);
				return false;				
				//echo "Not found"; exit;
			}
	}

	function invitationListP($start,$end,$therArea,$roleDetails,$countryDetails,$userType=[],$user_type, $failed_type){
        $fields = [[$therArea,"therArea"],[$roleDetails,"role"],[$countryDetails,"country"],[$companyDetails,"company"]];
   		$types = [[$user_type,"unsubscribe"],[$failed_type,"failStatus"]];
       $sql="SELECT i.sid, iu.id, s.user_id, s.role AS signed_role, s.profile_status, s.name, s.last_name, s.prefix, iu.email, iu.role, i.mail_batch, i.therArea, i.lastName, i.firstName, i.country, iu.failStatus, iu.dataType, i.company, u.unsubscribe as unsubscribe1, ur.unsubscribe as unsubscribe2, u.date as date1, ur.date as date2
			FROM `invited_user_email` iu
			LEFT JOIN invited i
			ON 
			 i.sid = 
			(
			    CASE 
			        WHEN iu.refer <> '0' THEN iu.refer
			        ELSE iu.sid
			    END
			)
			LEFT JOIN unsubscribed u
			ON u.sid = iu.sid
			LEFT JOIN unsubscribed ur
			ON ur.sid = iu.refer
			LEFT JOIN signup_users s
			ON iu.email = s.email
			WHERE ";
			foreach($fields as $fieldArea){
				$field = $fieldArea[0];
				$column = $fieldArea[1];
				$counter = 1;
				if ($field != "All") {
					$i = "i";
					if ($column == "role"){
						$i = "iu";
					}
                    $sql .= $i.'.'.$column." IN ('".implode("', '",$field)."') AND ";
                }
			}
//			echo "<pre>";var_dump($types);die;
			foreach($types as $typeArea){
				$type = $typeArea[0];
				$column = $typeArea[1];
				$counter = 1;
				if ($type != 2) {
					if ($column == "failStatus"){
						$sql .= "(iu.$column = '".$type."') AND ";
					} else {
					$sql .= "((
							    CASE 
							        WHEN u.date > ur.date THEN u.$column
							        ELSE ur.$column
							    END
							) = '".$type."') AND ";
					}
					
				}
			}
			$sql .= "iu.sid > $start_from
				ORDER BY iu.sid ASC LIMIT 0, $finish_at";
			 // echo $sql; exit;
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			if($row=$stmt->rowCount()){
				//echo "found";exit;
                /* Old version */
//				while ($row = $stmt->fetch())
//			  	{
//					  $uns[]=$row;
//			  	}
                /* New Version */
                $uns=$stmt->fetchAll();
				 $this->closeDB($dbCon);
				return $uns;
			}else{
				$this->closeDB($dbCon);
				return false;				
				//echo "Not found"; exit;
			}
    }


	function fieldList($field) {
		$sql = "SELECT DISTINCT ($field) FROM invited";
		// echo $sql; exit;
        $dbCon=$this->connectDB();
        // echo $dbCon; exit;
		$stmt = $dbCon->prepare($sql);
		// echo $stmt; exit;
		$stmt->execute();		
		if($row=$stmt->rowCount()){				
			// echo "found";exit;
			while ($row = $stmt->fetch())
		  	{
				  $uns[]=$row;
		  	}
			 $this->closeDB($dbCon);
			 // print_r($uns);exit;
			return $uns;
		}else{
			$this->closeDB($dbCon);
			return false;				
			//echo "Not found"; exit;
		}
	}

	function csd_list(){
			$sql="SELECT c.timestamp, c.email, c.name, c.surname, s.user_id, s.email_verify, s.profile_status, 'clinical_stud_coordinator' AS role FROM `mvp_clinical_stud_coordinator` c
			LEFT JOIN custom16_signup_users s
			ON c.email = s.email
			UNION
			SELECT i.timestamp, i.email, i.name, i.surname, s.user_id, s.email_verify, s.profile_status, 'investigator' AS role FROM `mvp_investigator` i
			LEFT JOIN custom16_signup_users s
			ON i.email = s.email
			UNION
			SELECT t.timestamp, t.email, t.name, t.surname, s.user_id, s.email_verify, s.profile_status, 'trial_manager' AS role FROM `mvp_trial_manager` t
			LEFT JOIN custom16_signup_users s
			ON t.email = s.email";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
	} 

	function invTaList(){
		$sql="SELECT 
			  SUBSTRING_INDEX(SUBSTRING_INDEX(all_tags, ',', num), ',', -1) AS one_tag,
			  COUNT(*) AS cnt, COUNT(*) * 100 / count_tags AS percent, count_tags as count_tags, ta.name AS therArea
			FROM (
			  SELECT
			    GROUP_CONCAT(therpeutic_area_of_work separator ',') AS all_tags,
			    LENGTH(GROUP_CONCAT(therpeutic_area_of_work SEPARATOR ',')) - LENGTH(REPLACE(GROUP_CONCAT(therpeutic_area_of_work SEPARATOR ','), ',', '')) + 1 AS count_tags
			  FROM custom16_investigator_details
			) t
			JOIN numbers n
			ON n.num <= t.count_tags
			LEFT JOIN therapeutic_areas ta
			ON ta.id = SUBSTRING_INDEX(SUBSTRING_INDEX(all_tags, ',', num), ',', -1)
			GROUP BY one_tag
			ORDER BY cnt DESC";
			// echo $sql; exit;
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
	}

function dataUpload($inArr){ 
	$dbCon=$this->connectDB();
	//$this->printr($inArr);
	//echo "Hello";
	// print_r($_FILES['uploaded_file']);exit;
	$filename = basename($_FILES['uploaded_file']['name']);
	$openfilename = $_FILES['uploaded_file']['tmp_name'];
	// echo $filename; echo $openfilename; exit;
	$ext = substr($filename, strrpos($filename, '.') + 1);
    $size = $_FILES['uploaded_file']['size'];
	// echo $ext;echo '<br />'; echo $size; exit;
	if($ext!= 'csv'){
		echo 'ext not ok'; exit;
		return false;
	} else if(!$size > 100000){
		echo "size not ok"; exit;
		return false;
	} else {
		// echo 'size and ext ok'; exit;
		$file = fopen($openfilename, "r");
        // print_r($file); exit;
        while(! feof($file))
		{
			while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
			{
				// print_r($emapData);
				// exit();
				// $sql = "INSERT into unsubscribed(sid,p_name,p_price,p_reason) values ('$emapData[0]','$emapData[1]','$emapData[2]','$emapData[3]')";
				$sql="INSERT INTO `invited` (`sid`, `mail_batch`, `therArea`,`lastName`, `firstName`, `middleName`, `suffix`, `company`, `country` ) VALUES (:sid, :batch, :ta, :ln, :fn, :mn, :sx, :company, :country)" ;
				$q = $dbCon->prepare($sql);
					$q->bindParam(":sid",$emapData[0],PDO::PARAM_INT);
					$q->bindParam(":batch",$emapData[1],PDO::PARAM_STR);
					$q->bindParam(":ta",$emapData[2],PDO::PARAM_INT);
					$q->bindParam(":ln",$emapData[4],PDO::PARAM_INT);
					$q->bindParam(":fn",$emapData[3],PDO::PARAM_INT);
					$q->bindParam(":mn",$emapData[5],PDO::PARAM_INT);
					$q->bindParam(":sx",$emapData[6],PDO::PARAM_INT);
					$q->bindParam(":company",$emapData[7],PDO::PARAM_INT);
					$q->bindParam(":country",$emapData[8],PDO::PARAM_INT);
					if (!$q->execute()){
					$this->printr(var_dump($q->errorInfo()));
					$this->printr(var_dump($q->errorCode()));
					exit;
					} else {
						$this->dataEmailUpload($emapData);
					}
			}
		}
        fclose($file);
		$this->closeDB($dbCon);
		// echo 'CSV File has been successfully Imported';
		// exit;
        return true;
    }
}

function dataEmailUpload($inArr){ 
	$dbCon=$this->connectDB();
	// print_r($inArr);
	// exit();
	// $sql = "INSERT into unsubscribed(sid,p_name,p_price,p_reason) values ('$emapData[0]','$emapData[1]','$emapData[2]','$emapData[3]')";
	$sql="INSERT INTO `invited_user_email` (`sid`, `email`, `dataType`,`role` ) VALUES (:sid, :email, :dt, :role)" ;
	$q = $dbCon->prepare($sql);
	$q->bindParam(":sid",$inArr[0],PDO::PARAM_INT);
	$q->bindParam(":email",$inArr[11],PDO::PARAM_STR);
	$q->bindParam(":dt",$inArr[13],PDO::PARAM_INT);
	$q->bindParam(":role",$inArr[14],PDO::PARAM_INT);
	if (!$q->execute()){
		$this->printr(var_dump($q->errorInfo()));
		$this->printr(var_dump($q->errorCode()));
		exit;
	}
        $this->closeDB($dbCon);
		// echo 'CSV File has been successfully Imported';
		// exit;
        return true;
    }

    function invcsclist(){
    	$sql="SELECT c.email as cscemail, c.prefix as cscpre, c.name as cscname, c.last_name as csclname, c.user_id as cscstatus, i.user_id as invstatus, l.*
			FROM custom16_csd_in_details l
			LEFT JOIN custom16_signup_users c
			ON 
			(
			    CASE 
			        WHEN l.user_id = '0' THEN l.coordinator_email = c. email
			        ELSE l.user_id  = c. user_id
			    END
			)
			-- l.user_id = c. user_id
			LEFT JOIN custom16_signup_users i
			ON 
			(
			    CASE 
			        WHEN l.in_userid = '0' THEN l.investigator_email = i.email
			        ELSE l.in_userid = i. user_id
			    END
			)
			-- l.in_userid = i. user_id
			ORDER BY l.det_id";
			// echo $sql; exit;
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
    }
}
?>