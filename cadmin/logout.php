<?php

				session_start();
				session_destroy();
				session_unset();
				$_SESSION = array();
				
				// If it's desired to kill the session, also delete the session cookie.
				// Note: This will destroy the session, and not just the session data!
				if (ini_get("session.use_cookies")) {
					$params = session_get_cookie_params();
					setcookie(session_name(), '', time() - (86400 * 30),$params["path"], $params["domain"],$params["secure"], $params["httponly"]
					);
				}
				header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
				header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
				header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
				header("Cache-Control: post-check=0, pre-check=0", false);
				header("Pragma: no-cache");
				
				unset($_SESSION['admin_email']);
				
				setcookie("PHPSESSID","",time()-3600,"/");
				

				header("location:./login.php?logout");
				
				  
				//$myzone->printr($_COOKIE);
				
				//exit;
				

?>