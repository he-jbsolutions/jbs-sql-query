<?php
//echo "hello";exit;
include("includes/constant.ini.php");

//$cstudy->test();
//echo $_SESSION['admin_email'];
//exit;
if(!$cstudy->admin_login()){
    //echo $_SESSION['admin_email'];
    //echo "hello";exit;
    header("location:login.php?logout");
}

if(isset($_POST['status_change'])){
    $cstudy->change_status($_POST);
}
/*echo'<pre>';
print_r($cstudy->user_list());exit;*/
if(isset($_POST["Import"])) {
    // print_r($_POST);
    // echo (count ($_POST)); exit;
    if ($cstudy->dataUpload($_POST,'invited')) {
        $upload_result = 'File has been uploaded';
        $cls= 'alert alert-success';
    } else {
        $upload_result = 'File could not be uploaded';
        $cls= 'alert alert-danger';
    }
}

if(isset($_GET["user_type"])) {
    $user_type= $_GET["user_type"];
} else {
    $user_type= 0;
}
if(isset($_GET["failed_type"])) {
    $failed_type= $_GET["failed_type"];
} else {
    $failed_type= 0;
}
$therArea = 'All';
$roleDetails = 'All';
$countryDetails = 'All';
$userType = [];
if(isset($_GET["therapeutic_area"])) {
    $therArea = $_GET['therapeutic_area'];
}
if(isset($_GET["role"])) {
    $roleDetails = $_GET['role'];
}
if(isset($_GET["countries"])) {
    $countryDetails = $_GET['countries'];
}
if(isset($_GET["user_types"])) {
    $userType = $_GET['user_types'];
}

if (isset($_POST['updateFailed'])){
    $cstudy->updateFailed();
}
if(isset($_GET['start'])){
    $start = $_GET['start'];
}else{
    $start = 0;
}
if(isset($_GET['end'])){
    $end = $_GET['end'];
}else{
    $end = 1000;
}
if(isset($_GET['page'])){
    $page=$_GET['page'];

    }else{
        $page=0;
}

    if ($page == 1 || $page == 0) {
//        $end = 1000;
        $start = 0;
    } else {
//        $end = $page * 1000;
//        $start = $end - 1000;
        $start=$page * 1000;
    }

//var_dump($start,$end)  ;die;
?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Clinical Study Network</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/chosen/chosen.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


    <!-- Data Tables -->

    <link href="css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">

    <link href="css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">

    <link href="css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">



    <link href="css/animate.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <style>
        #user_type,#failed_type{
            padding: 6px;
            width: 100%;
        }
        .table {
            table-layout: fixed;
            width: 100% !important;
        }
        .table td,
        .table th{
            width: auto !important;
            white-space: normal;
            text-overflow: ellipsis;
            overflow: hidden;
        }
    </style>
</head>

<body>
<div id="wrapper">
    <?php include("includes/left_side.php");?>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <?php include("includes/header.php");?>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-8">
                <h2>Database</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.php">Home</a>
                    </li>
                    <li class="active">
                        <strong>Invited users list</strong>
                    </li>
                </ol>
            </div>
            <!-- <div class="col-lg-4">
                <form method="post" role="form">
                <h4>&nbsp;</h4>
                    <div class="form-group col-lg-4 text-center">
                        <button type="submit" class="btn btn-primary" name="updateFailed" value="updateFailed">Click here to update after adding failed emails</button>
                    </div>
                </form>
            </div> -->
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <?php if (isset($upload_result)){?>
                                <div class=<?php echo '"col-lg-12 ';echo $cls;echo '"'; ?>>
                                    <?php echo $upload_result; ?>
                                </div>
                            <?php }?>
                            <form enctype="multipart/form-data" method="post" role="form">
                                <h4 class="col-lg-12">Upload more users in .csv file, before sending emails for ensuring successful unsubscribing</h4>
                                <div class="form-group col-lg-4">
                                    <input type="file" name="uploaded_file" id="uploaded_file" size="100000">
                                    <p class="help-block">Only CSV File Import.</p>
                                </div>
                                <div class="form-group col-lg-4">
                                    <button type="submit" class="btn btn-primary" name="Import" value="Import">Upload</button>
                                </div>
                            </form>
                            <div class="hr-line-dashed" style="clear:both;height: 10px;"></div>
                            <div class="form-group col-lg-12">
                                <form method="get" role="form">
                                    <div class="form-group col-lg-6">
                                        <select name="user_type"  id="user_type">
                                            <option class="tmp" value="0" <?php if($user_type =='0'){?> selected="selected" <?php }?>>None</option>
                                            <option class="tmp" value="2" <?php if($user_type =='2'){?> selected="selected" <?php }?>>All of them</option>
                                            <option class="tmp" value="1" <?php if($user_type =='1'){?> selected="selected" <?php }?>>Only them</option>
                                        </select>
                                        <p class="help-block">Unsubscribed?</p>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <select name="failed_type"  id="failed_type">
                                            <option class="tmp" value="0" <?php if($failed_type =='0'){?> selected="selected" <?php }?>>None</option>
                                            <option class="tmp" value="2" <?php if($failed_type =='2'){?> selected="selected" <?php }?>>All of them</option>
                                            <option class="tmp" value="1" <?php if($failed_type =='1'){?> selected="selected" <?php }?>>Only them</option>
                                        </select>
                                        <p class="help-block">Failed emails?</p>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <select name="role[]"  id="role" data-placeholder="Choose role..." class="chosen-select" multiple style="width:100%;" tabindex="4">
                                            <?php if(isset($_GET['role']) && is_array($_GET['role'])): ?>
                                                <option class="text-left" <?php echo in_array('investigator',$_GET['role']) ? 'selected="selected"' : '' ?> value="investigator">Investigator</option>
                                                <option class="text-left" <?php echo in_array('trial_manager',$_GET['role']) ? 'selected="selected"' : '' ?> value="trial_manager">Trial Manager</option>
                                                <option class="text-left" <?php echo in_array('clinical_stud_coordinator',$_GET['role']) ? 'selected="selected"' : '' ?> value="clinical_stud_coordinator">Clinical Study Coordinator</option>
                                                <option class="text-left" <?php echo in_array('service_provider',$_GET['role']) ? 'selected="selected"' : '' ?> value="service_provider">Service Provider</option>
                                            <?php else: ?>
                                                <option class="text-left" value="investigator">Investigator</option>
                                                <option class="text-left" value="trial_manager">Trial Manager</option>
                                                <option class="text-left" value="clinical_stud_coordinator">Clinical Study Coordinator</option>
                                                <option class="text-left" value="service_provider">Service Provider</option>
                                            <?php endif; ?>
                                        </select>
                                        <p class="help-block">Role</p>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <?php $res=$cstudy->fieldList("therArea");
                                        // print_r($res); exit; ?>
                                        <select id="ther_area" name="therapeutic_area[]" data-placeholder="Choose ther Area..." class="chosen-select" multiple style="width:100%;" tabindex="4">
                                            <?php foreach($res as $key=>$val){ ?>
                                                <?php if(isset($_GET['therapeutic_area']) && in_array($val['therArea'],$_GET['therapeutic_area'])): ?>
                                                    <option class="text-left" selected="selected" value="<?php echo $val['therArea'];?>"><?php echo $val['therArea'];?></option>
                                                <?php else: ?>
                                                    <option class="text-left" value="<?php echo $val['therArea'];?>"><?php echo $val['therArea'];?></option>
                                                <?php endif; ?>
                                            <?php } ?>
                                        </select>
                                        <p class="help-block">Therapeutic Area</p>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <?php $res1=$cstudy->fieldList("country"); ?>
                                        <select id="countries" name="countries[]" data-placeholder="Choose Country..." class="chosen-select" multiple style="width:100%;" tabindex="4">
                                            <?php foreach($res1 as $key=>$val1){ ?>
                                                <?php if(isset($_GET['countries']) && in_array($val1['country'],$_GET['countries'])): ?>
                                                    <option class="text-left" selected="selected" value="<?php echo $val1['country'];?>"><?php echo $val1['country'];?></option>
                                                <?php else: ?>
                                                    <option class="text-left" value="<?php echo $val1['country'];?>"><?php echo $val1['country'];?></option>
                                                <?php endif; ?>
                                            <?php } ?>
                                        </select>
                                        <p class="help-block">Countries</p>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <select id="user_types" name="user_types[]" data-placeholder=" User Types..." class="chosen-select" multiple style="width:100%;" tabindex="4">
                                                <?php if(isset($_GET['user_types']) && is_array($userType)): ?>
                                                    <option <?php echo in_array('1',$_GET['user_types']) ? 'selected="selected"' : '' ?> class="text-left" value="1">First</option>
                                                    <option <?php echo in_array('2',$_GET['user_types']) ? 'selected="selected"' : '' ?> class="text-left" value="2">Second</option>
                                                    <option <?php echo in_array('3',$_GET['user_types']) ? 'selected="selected"' : '' ?> class="text-left" value="3">Third</option>
                                                    <option <?php echo in_array('4',$_GET['user_types']) ? 'selected="selected"' : '' ?> class="text-left" value="4">Fourth</option>
                                                    <option <?php echo in_array('5',$_GET['user_types']) ? 'selected="selected"' : '' ?> class="text-left" value="5">Fifth</option>
                                                    <option <?php echo in_array('6',$_GET['user_types']) ? 'selected="selected"' : '' ?> class="text-left" value="6">Sixth</option>
                                                <?php else: ?>
                                                    <option class="text-left" value="1">First</option>
                                                    <option class="text-left" value="2">Second</option>
                                                    <option class="text-left" value="3">Third</option>
                                                    <option class="text-left" value="4">Fourth</option>
                                                    <option class="text-left" value="5">Fifth</option>
                                                    <option class="text-left" value="6">Sixth</option>
                                                <?php endif; ?>
                                        </select>
                                        <p class="help-block">User status</p>
                                    </div>
                                    <div class="form-group col-lg-2 text-center">
                                        <button type="submit" class="btn btn-primary" name="refresh" value="refresh">Refresh</button>
                                    </div>
                                </form>
                                <?php
                                    $subs_status = $cstudy->invitationListP($start,$end,$therArea,$roleDetails,$countryDetails,$userType,$user_type, $failed_type);
                                    $qount = floor($subs_status['total']/1000);
//                                    var_dump($qount);die;
                                ?>
                                <nav class="text-right my-4">
                                    <ul id="page_ination" class="pagination pagination-circle pg-blue mb-0">

                                    </ul>
                                </nav>

                            </div>
                            <table class="table table-striped table-bordered table-hover dataTables-example" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Batch</th>
                                    <th>Data Type</th>
                                    <th>TherArea</th>
                                    <th>Name</th>
                                    <th>Surname</th>
                                    <th>Country</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $u1 = $user_type;
                                $u2 = $user_type;
                                if ($user_type == 2) {
                                    $u1 = 0;
                                    $u2 = 1;
                                }
                                if ($subs_status['result'] >0 ){
                                    foreach($subs_status['result'] as $uns)
                                    {
//                                        var_dump($uns['sid']);die;
                                        //ID
                                        if(($uns['profile_status'] == 1 || $uns['profile_status'] === 0) && !$uns['csc_user_id']){
                                            $sid = "signed/".$uns['user_ID'];
                                        }elseif ($uns['subscription_id']){
                                            $sid = 'subs/'.$uns['subscription_id'];
                                        }elseif ($uns['csc_user_id']){
                                            $sid = 'inv/'.$uns['csc_det_id'];
                                        }else{
                                            $sid = 'sid/'.$uns['sid'];
                                        }

                                        //ROLE
                                        if($uns['role']){
                                            $role = $uns['role'];
                                        }else{
                                            $role = $uns['signed_role'];
                                        }

                                        //EMAIL
                                        if($uns['iu_email']){
                                            $email = $uns['iu_email'];
                                        }elseif ($uns['Email']){
                                            if($uns['coordinator_email'] && $uns['investigator_email']){
                                                if($uns['Email'] == $uns['coordinator_email'] && $uns['investigator_email'] !=$uns['Email']){
                                                    $email = $uns['investigator_email'];
                                                }elseif ($uns['Email'] != $uns['coordinator_email'] && $uns['investigator_email'] ==$uns['Email']){
                                                    $email = $uns['coordinator_email'];
                                                }else{
                                                    $email = '';
                                                }
                                                $role = 'investigator';
                                            }else{
                                                $email = $uns['Email'];
                                            }
                                        }

                                        //Batch
                                        $subsid=$uns['id'];
                                        if($uns['sid']){
                                            $batch=$uns['mail_batch'];
                                        }else{
                                            $batch='';
                                        }
                                        $ther=$cstudy->get_therapeutic_areas();
                                        //TherArea
                                        $terArr='';
                                        if($uns['signed_role'] == 'investigator' && $uns['invTar']){
                                            $ta=explode(',',$uns['invTar']);
                                            foreach ($ther as $t){
                                                if(in_array($t['id'],$ta)){
                                                    $terArr .= $t['name'].', ';
                                                }
                                            }
                                        }elseif ($uns['signed_role'] == 'trial_manager' && $uns['tmTar']){
                                            $ta=explode(',',$uns['tmTar']);
                                            foreach ($ther as $t){
                                                if(in_array($t['id'],$ta)){
                                                    $terArr .= $t['name'].', ';
                                                }
                                            }
                                        }elseif ($uns['therArea']){
                                            if(preg_match('#[0-9]#',$uns['therArea'])){
                                                $ta = explode(',',$uns['therArea']);
                                                foreach ($ther as $t){
                                                    if(in_array($t['id'],$ta)){
                                                        $terArr .= $t['name'].', ';
                                                    }
                                                }
                                            }else{

                                                $terArr = $uns['therArea'];
                                            }

                                        }

                                        //DataType
                                        if(($uns['profile_status'] == 1 || $uns['profile_status'] === 0) && !$uns['sid']){
                                            $dataType='self';
                                        }else{
                                            $dataType= $uns['dataType'];
                                        }

                                        //FName LName
                                        $fname=$uns['firstName'];
                                        $lname=$uns['lastName'];


                                        $emailCheck = $uns['failStatus'];

                                        //Country
                                        $countrys=$cstudy->get_country_list();
                                        if($uns['signed_role'] == 'investigator' && $uns['invCountry']){
                                            foreach ($countrys as $c){
                                                if($c['id'] == $uns['invCountry']){
                                                    $country = $c['country_name'];
                                                }
                                            }
                                        }elseif ($uns['signed_role'] == 'trial_manager' && $uns['tmCountry']){
                                            foreach ($countrys as $c){
                                                if($c['id'] == $uns['tmCountry']){
                                                    $country = $c['country_name'];
                                                }
                                            }
                                        }elseif ($uns['country']){
                                            if(is_numeric($uns['country'])){
                                                foreach ($countrys as $c){
                                                    if($c['id'] == $uns['country']){
                                                        $country = $c['country_name'];
                                                    }
                                                }
                                            }else{
                                                $country=$uns['country'];
                                            }
                                        }
                                        $profile = "";
                                        if($role=="investigator"){
                                            $profile = "profile_inv";
                                        }else if($role=="clinical_stud_coordinator"){
                                            $profile = "profile_csc";
                                        }else if($role=="trial_manager"){
                                            $profile = "profile_tm";
                                        }else if($role=="service_provider"){
                                            $profile = "profile_sp";
                                        }

                                        if ($uns['date1'] > $uns['date2']) {
                                            $unsub= $uns['unsubscribe1'];
                                            $ud=$uns['date1'];
                                        } else {
                                            $unsub= $uns['unsubscribe2'];
                                            $ud=$uns['date2'];
                                        }
                                        $user_id = $uns['user_id'];
                                        ?>
                                        <tr>
                                            <td class="text-nowrap">
                                                <?php echo $sid; ?>
                                            </td>
                                            <td class="text-nowrap">
                                                <?php if($emailCheck == 0){?>
                                                <span class="label">
										<?php } else { ?>
                                                    <span class="label label-danger">
										<?php 	}  echo $email ?>
										</span>
                                            </td>
                                            <td class="text-nowrap">
                                                <?php echo $role;?>
                                            </td>
                                            <td class="text-nowrap">
                                                <?php echo $batch; ?>
                                            </td>
                                            <td class="text-nowrap">
                                                <?php echo $dataType; ?>
                                            </td>
                                            <td class="text-nowrap">
                                                <?php echo $terArr; ?>
                                            </td>
                                            <td class="text-nowrap">
                                                <?php echo $fname;?>
                                            </td>
                                            <td class="text-nowrap">
                                                <?php echo $lname;?>
                                            </td>
                                            <td class="text-nowrap">
                                                <?php echo $country; ?>
                                            </td>
                                            <td class="text-nowrap">
                                                <?php if (($uns['user_ID'] && $uns['profile_status'] == 1) && !$uns['csc_user_id']){ ?>
                                                    <a target="_blank" href="<?php echo $profile;?>?user_id=<?php echo $uns['user_ID']?>&type=<?php echo $role;?>"><span>profiled</span></a>
                                               <?php } else if (($user_id !="") AND ($uns['profile_status'] == 0)){?>
                                                    registered
                                                <?php }
                                                else if ($uns['subscription_id']) { ?>
                                                    subscribed
                                                <?php }else if ($uns['csc_user_id'] && !$uns['iu_email']) { ?>
                                                    <a target="_blank" href="<?php echo $profile;?>?user_id=<?php echo $uns['csc_user_id']?>&type=<?php echo 'clinical_stud_coordinator';?>"><span>CSC inv</span></a>
                                                <?php }else if($uns['csc_user_id'] && $uns['iu_email']){ ?>
                                                    <a target="_blank" href="<?php echo $profile;?>?user_id=<?php echo $uns['csc_user_id']?>&type=<?php echo $role;?>"><span>invited/CSCinv</span></a>
                                                <?php }else{ ?>
                                                    invited
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php  }}?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include("includes/footer.php");?>
    </div>
</div>

<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="js/jquery.simplePagination.js"></script>
<!-- Flot -->
<script src="js/plugins/flot/jquery.flot.js"></script>
<script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/plugins/flot/jquery.flot.spline.js"></script>
<script src="js/plugins/flot/jquery.flot.resize.js"></script>
<script src="js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="js/plugins/peity/jquery.peity.min.js"></script>
<script src="js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- GITTER -->
<script src="js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="js/demo/sparkline-demo.js"></script>

<!-- ChartJS-->
<script src="js/plugins/chartJs/Chart.min.js"></script>

<!-- Toastr -->
<script src="js/plugins/toastr/toastr.min.js"></script>

<!-- Data Tables -->

<script src="js/plugins/dataTables/jquery.dataTables.js"></script>

<script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script src="js/plugins/dataTables/dataTables.responsive.js"></script>

<script src="js/plugins/dataTables/dataTables.tableTools.min.js"></script>
<!-- Page-Level Scripts -->

<script>

    $(document).ready(function() {

        $('.dataTables-example').dataTable({
            //"aaSorting": [[ 0, 'desc' ]],
            responsive: true,

            "dom": 'T<"clear">lfrtip',

            "tableTools": {

                "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"

            }

        });
        us_type='<?php echo is_array($user_type)?json_encode($user_type):$user_type; ?>';
        failed_type = '<?php echo  is_array($failed_type)?json_encode($failed_type):$failed_type; ?>';
        role='<?php echo is_array($roleDetails)?json_encode($roleDetails):$roleDetails; ?>';
        therArea='<?php echo is_array($therArea)? json_encode($therArea) :$therArea; ?>';
        country='<?php echo is_array($countryDetails)? json_encode($countryDetails) : $countryDetails ;?>';
        userTypes='<?php echo is_array($userType)?json_encode($userType):$userType; ?>';
        $(function() {
            $('#page_ination').pagination({
                items: '<?=floor($subs_status['total'])?>',
                itemsOnPage: 1000,
                pages: '<?=$qount;?>',
                currentPage : <?php echo $page;?>,
                hrefTextPrefix : "invitationlist.php?user_type="+us_type+"&failed_type="+failed_type+"&role="+role+"&therapeutic_area="+therArea+"&countries="+country+"&user_types="+userTypes+"&page="
            });
        });

        /* Init DataTables */

        var oTable = $('#editable').dataTable();

    });







</script>
<!-- Chosen -->
<script src="js/plugins/chosen/chosen.jquery.js"></script>
<script>
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</body>
</html>
