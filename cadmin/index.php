<?php

//echo "hello";exit;
include("includes/constant.ini.php");
//$cstudy->test();
//echo $_SESSION['admin_email'];
//exit;
if(!$cstudy->admin_login()){
	//echo $_SESSION['admin_email'];
	//echo "hello";exit;
	header("location:login.php?logout");
}
?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Clinical Study Network | Admin Dashboard</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>
	<div id="wrapper">
		<?php include("includes/left_side.php");?>
        <div id="page-wrapper" class="gray-bg dashbard-1">
			<?php include("includes/header.php");?>
			<div class="row  border-bottom white-bg dashboard-header">
				<div class="col-sm-3" style="width: 100%;">
					<h2>Welcome to Clinical Study Network</h2>
                </div>
            </div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-content">
								<h2 class="text-center">Problems reported</h2>
								<table class="table table-striped table-bordered table-hover " id="editable" >
									<thead>
										<tr>
											<th>Report No.</th>
											<th>Email</th>
											<th>User ID</th>
											<th>User Type</th>
											<th>Problem Detail</th>
											<th>Report Date</th>
										</tr>
									</thead>
									<tbody>
										<?php if($cstudy->problem_reported() > 0){
										// echo '<pre>';print_r($cstudy->problem_reported()); exit;
										foreach($cstudy->problem_reported() as $pro) {?>
										<tr>
											<td><?php echo $pro['project_id'];?></td>
											<td><?php echo $pro['email'];?></td>
											<td><?php echo $pro['user_id'];?></td>
											<td><?php echo $pro['user_type'];?></td>
											<td><?php echo $pro['summary'];?></td>
											<td><?php echo $pro['abd'];?></td>
										</tr>
										<?php }}?>
                                    </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer">
				<?php include("includes/footer.php");?>
			</div>
		</div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>
    <script src="js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="js/plugins/chartJs/Chart.min.js"></script>

    <!-- Toastr -->
    <script src="js/plugins/toastr/toastr.min.js"></script>


    
</body>
</html>
