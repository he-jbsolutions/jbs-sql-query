<?php

require(dirname(__FILE__)."/../../../includes/classes/general.class.php");

class myzone extends general{  

    var $UserId;
    var $UserEmail;
    var $profilePic;
    var $dateCreated;
    var $dateModified;

    function __construct() {
        parent::__construct();
        $this->dateCreated = date('Y-m-d H:i:s');
    }

function check_login()
{
    if (!isset($_SESSION['lemail']) and empty($_SESSION['lemail'])) {
    	header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
    	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    	header("Cache-Control: post-check=0, pre-check=0", false);
    	header("Pragma: no-cache");
    	
    	unset($_SESSION['lemail']);
    				
    	setcookie("PHPSESSID","",time()-3600,"/");
    	return false;
    	
    } else {
    	return true;
    }
}


function user_details($userid){
	
	//echo
	$sql="SELECT * FROM `custom16_signup_users` WHERE email='".$_SESSION['lemail']."' AND user_id=$userid";
		//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
			  
					  $res[]=$row;
			  	}
				 
				return $res;
				
			}else{
				return false;
				
				//echo "Not found"; exit;
			}
	
	
	
}//END FUNCTION

function profile_details($inarray,$type) {
    if ($type == 'service_provider') {
        $sql="SELECT * FROM `custom16_serviceproviders_details` 
        LEFT JOIN custom16_signup_users
        ON (custom16_signup_users.user_id=custom16_serviceproviders_details. user_id)
        WHERE custom16_serviceproviders_details.user_id='".$inarray."' AND deleted = '0'";
    } else if ($type == 'clinical_stud_coordinator') {
        $sql="SELECT custom16_csd_details.*, custom16_signup_users.*
        FROM `custom16_csd_details` 
        LEFT JOIN custom16_signup_users
        ON (custom16_signup_users.user_id=custom16_csd_details. user_id)
        WHERE custom16_csd_details.user_id='".$inarray."' AND deleted='0'";
    } else if ($type == 'trial_manager') {
        $sql="SELECT custom16_trialmanagers_details.*, custom16_signup_users.* 
        FROM `custom16_trialmanagers_details` 
        LEFT JOIN custom16_signup_users
        ON (custom16_signup_users.user_id=custom16_trialmanagers_details. user_id)
        WHERE custom16_trialmanagers_details.user_id='".$inarray."' AND deleted='0'";
    } else if ($type == 'investigator') {
        $sql="SELECT custom16_investigator_details.*, custom16_signup_users.*
        FROM `custom16_investigator_details` 
        LEFT JOIN custom16_signup_users
        ON (custom16_signup_users.user_id=custom16_investigator_details. user_id)
        WHERE custom16_investigator_details.user_id='".$inarray."' AND deleted='0'";
    }

    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();

    if($row=$stmt->rowCount()){

        //echo "found";exit;
        while ($row = $stmt->fetch())
        {
              $res[]=$row;
        }
         $this->closeDB($dbCon);
        return $res;
    }else{
        $this->closeDB($dbCon);
        return false;

        //echo "Not found"; exit;
    }
}

function change_role($new_role){
	$userid = $_SESSION['luser_id'];
	$current_role = $_SESSION['lrole'];
	$sql1 = "";
	$sql2 = "";
	$sql3 = "";
	$sql4 = "";
    $sqli = "";
	
		$dbCon=$this->connectDB();
		$sql1 = "UPDATE custom16_signup_users SET role = '".$new_role."', role_change_status = 1, profile_status = 0 WHERE user_id = '".$userid."'";
			// echo $sql1; exit;
		$stmt = $dbCon->prepare($sql1);
		if (!$stmt->execute()){
			return false;			
		} else {
            $plan=$this->find_plan_rol($new_role);
            if(is_array($plan) && !empty($plan)){
                if($current_role == 'service_provider'){
                    $sql2="UPDATE `custom16_serviceproviders_details` SET deleted = 1 WHERE user_id ='".$userid."'";
                    $sqli = "UPDATE plan_subscriptions SET campaign_id = '".$plan['id']."' WHERE user_id = '".$userid."'";
                }elseif($current_role == 'clinical_stud_coordinator'){
                    $sql2="UPDATE custom16_csd_details SET deleted = 1 WHERE user_id ='".$userid."'";
                    $sql3="UPDATE custom16_csd_in_details SET deleted = 1 WHERE user_id ='".$userid."'";
                    $sql4="UPDATE custom16_csd_in_sites SET deleted = 1 WHERE user_id ='".$userid."'";
                    if($new_role != 'investigator'){
                        $sqli = "UPDATE plan_subscriptions SET campaign_id = '".$plan['id']."' WHERE user_id = '".$userid."'";
                    }

                }elseif($current_role == 'trial_manager'){
                    $sql2="UPDATE custom16_trialmanagers_details SET deleted = 1 WHERE user_id ='".$userid."'";
                    $sqli = "UPDATE plan_subscriptions SET campaign_id = '".$plan['id']."' WHERE user_id = '".$userid."'";

                }else if($current_role == 'investigator'){
                    $sql2="UPDATE custom16_investigator_details SET deleted = 1 WHERE user_id ='".$userid."'";
                    $sql3="UPDATE custom16_in_sites SET deleted = 1 WHERE user_id ='".$userid."'";
                    $sql4="UPDATE  custom16_in_trails SET deleted = 1 WHERE user_id ='".$userid."'";
                    if($new_role != 'clinical_stud_coordinator'){
                        $sqli = "UPDATE plan_subscriptions SET campaign_id = '".$plan['id']."' WHERE user_id = '".$userid."'";
                    }
                }
                // echo $sql2; exit;
                if($sqli != ''){
                    $stmt = $dbCon->prepare($sqli);
                    if (!$stmt->execute()){
                        return false;
                    }
                }
                $stmt = $dbCon->prepare($sql2);

                if (!$stmt->execute()){
                    return false;
                } else {
                    if ($sql3 != ""){
                        $stmt = $dbCon->prepare($sql3);
                        if (!$stmt->execute()){
                            return false;
                        } else {
                            $stmt = $dbCon->prepare($sql4);
                            if (!$stmt->execute()){
                                return false;
                            } else {
								return true;
                            }
                        }
                    } else {
                        return true;
                    }
                } return true;
			}else{
            	return false;
			}

		}
	
}
function find_plan_rol($role){
    if($role=="trial_manager"){
        $plan="Trial Manager (Free)";
    }
    if($role=="investigator"){
        $plan='Investigator (Free)';
    }
    if($role=="clinical_stud_coordinator"){
        $plan='Investigator (Free)';
    }
    if($role=="service_provider"){
        $plan="Service Provider (Free)";
    }
    $sql="SELECT id FROM `plans` WHERE campaign_detail	='".$plan."'";
    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();

    if($row=$stmt->rowCount()) {
        //echo "test.php";
        return $stmt->fetch();
    }
}
function check_changed_role(){
	$sql="SELECT role_change_status FROM `custom16_signup_users` WHERE user_id='".$_SESSION['luser_id']."'";
		//exit;
		$dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		
		if($row=$stmt->rowCount()){
			//echo "test.php";
			$row = $stmt->fetch();
			
			if($row[0]==1){
				$this->closeDB($dbCon);
				return true;
			}else{
				$this->closeDB($dbCon);
				return false;
			}
		}
}

    
function getTrialByID($trialID, $ownerOnly = true) {
    $sql = "SELECT * FROM `custom16_in_trails` 
            WHERE deleted = '0' AND trail_id = '".$trialID."'";
    
    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();

    if($stmt->rowCount()){
        while ($row = $stmt->fetch()) {
            $res[] = $row;
        }
        $this->closeDB($dbCon);
        
        if ($ownerOnly && ($res[0]['user_id'] != $_SESSION['luser_id'])) {
            return false;
        } else {
            return $res;
        }
    }else{
        $this->closeDB($dbCon);
        return false;
    }
}

    function in_trails_list($inarray){
        $sql = "SELECT * FROM `custom16_in_trails` 
        WHERE deleted = '0' AND user_id ='".$inarray."'";

        return $this->executeQuery($sql, false);
    }

function getSiteByID($siteID, $ownerOnly = true) {
    $sql = "SELECT * FROM `custom16_in_sites` 
            WHERE deleted = '0' AND site_id = '".$siteID."'";
    
    $dbCon = $this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();

    if($stmt->rowCount()){
        while ($row = $stmt->fetch()) {
            $res[] = $row;
        }
        $this->closeDB($dbCon);
        
        if ($ownerOnly && ($res[0]['user_id'] != $_SESSION['luser_id'])) {
            return false;
        } else {
            return $res;
        }
    }else{
        $this->closeDB($dbCon);
        return false;
    }
}
    
function getCoordinatorSiteByID($siteID, $ownerOnly = true) {
    $sql = "SELECT * FROM `custom16_csd_in_sites` 
            WHERE deleted = '0' AND site_id = '".$siteID."'";
    
    $dbCon = $this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();

    if($stmt->rowCount()){
        while ($row = $stmt->fetch()) {
            $res[] = $row;
        }
        $this->closeDB($dbCon);
        
        if ($ownerOnly && ($res[0]['user_id'] != $_SESSION['luser_id'])) {
            return false;
        } else {
            return $res;
        }
    }else{
        $this->closeDB($dbCon);
        return false;
    }
}

    function in_site_list($inarray){
        $sql = "SELECT * FROM `custom16_in_sites` 
        WHERE deleted = '0' AND user_id = '".$inarray."'";

        return $this->executeQuery($sql, false);
    }

function csd_site_list($inarray){
			//exit;
            $sql="SELECT * FROM `custom16_csd_in_sites` 
            LEFT JOIN custom16_csd_details
		    ON (custom16_csd_details.user_id=custom16_csd_in_sites. user_id)
            WHERE custom16_csd_in_sites.user_id ='".$inarray."' AND custom16_csd_in_sites.deleted = '0'";
                
            
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
}
    function csd_invest_list($inarray)
    {
        $sql="SELECT custom16_csd_in_details.det_id AS csd_inv_rel_id, custom16_csd_in_details.*, custom16_csd_details.* FROM `custom16_csd_in_details` 
        LEFT JOIN custom16_csd_details
        ON custom16_csd_details.user_id=custom16_csd_in_details.user_id AND custom16_csd_details.deleted = '0' 
        WHERE custom16_csd_in_details.user_id ='".$inarray."' AND custom16_csd_in_details.deleted = '0'";
            
        return $this->executeQuery($sql, false);
    }

    function csd_list()
    {
        $sql = "SELECT * FROM `custom16_csd_in_details` 
        WHERE custom16_csd_in_details.investigator_email ='".$_SESSION['lemail']."' AND deleted='0'";
           
        return $this->executeQuery($sql, false);
    }

    function in_list()
    {
        $sql = "SELECT * FROM `custom16_csd_in_details` 
            WHERE (coordinator_email ='".$_SESSION['lemail']."' OR user_id='".$_SESSION['luser_id']."') AND deleted='0'";
        
        return $this->executeQuery($sql, false);
    }

function get_role($user){
	
	
	//echo $role;exit;
	$sql="SELECT role FROM `custom16_signup_users` WHERE email='$user'";
		//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				$row = $stmt->fetch();
				
				//echo $row[0];
				
			if($row[0]=="trial_manager"){
		
		
				  $role="Trial Manager";
				  echo $role;
				 // return;
			  }
			  if($row[0]=="investigator"){
				  
				  
				  $role="Investigator";
				  echo $role;
				  //return;
			  }
			  if($row[0]=="clinical_stud_coordinator"){
				  
				  
				  $role="Clinical Study Coordinator";
				  echo $role;
				  //return;
			  }
			  if($row[0]=="service_provider"){
				  
				  
				  $role="Service Provider";
				  echo $role;
				  //return;
			  }	
		  
		  
		   		$this->closeDB($dbCon);
				
	}
	 

}//END FUNCTION

function get_form_by_role(){
	
	
			//echo
			$sql="SELECT role FROM `custom16_signup_users` WHERE email='".$_SESSION['lemail']."'";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$form=NULL;
			
			if($row=$stmt->rowCount()){
				//echo "test.php";
				$row = $stmt->fetch();
				if($row[0]=="trial_manager"){
		
				  $form="trial-profile-form.php";
				 // return;
			  }
			  if($row[0]=="investigator"){
				  
				  
				 $form="in-profile-form-new-1.php";
			  }
			  
			  if($row[0]=="clinical_stud_coordinator"){
				  
				  
				 $form="csd-profile-form.php";
			  }
			  if($row[0]=="service_provider"){
				  
				 $form="service-provider-profile-form.php";
				  //return;
			  }	
			  
			  
			  
			}
			
			return $form;
			$this->closeDB($dbCon);
	
	
}//END FUNCTION
function randomString($length = 5) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
}
function update_profile_by_role($section='general') {
    $sql = "SELECT role FROM `custom16_signup_users` WHERE email='".$_SESSION['lemail']."'";

    $dbCon = $this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();
    $this->closeDB($dbCon);
    $form = '';

    if ($stmt->rowCount()) {
        $row = $stmt->fetch();
        if ($row[0]=="trial_manager") {
            //$form = "tm-profile-modify.php";
            $form = "tm-profile-edit-".$section.".php";
        }
        if ($row[0] == "investigator") {
            $form = "inv-profile-edit-".$section.".php";
        }
        if ($row[0] == "clinical_stud_coordinator") {
            //$form = "csd-profile-modify.php";
            $form = "coordinator-profile-edit-".$section.".php";
        }
        if ($row[0]=="service_provider") {
            //$form = "sp-profile-modify.php";
            $form = "sp-profile-edit-".$section.".php";
        }

    }
    
    if (file_exists('edit-profile/'.$form))
        return 'edit-profile/'.$form;
    else
        return false;
    
}//END FUNCTION

function check_profile_status(){
			$sql="SELECT profile_status FROM `custom16_signup_users` WHERE email='".$_SESSION['lemail']."'";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				//echo "test.php";
				$row = $stmt->fetch();
				
				if($row[0]==1){
					$this->closeDB($dbCon);
					return true;
				}else{
					$this->closeDB($dbCon);
					return false;
				}
			}
	
	
}

function get_therapeutic_areas(){
	
	$sql="SELECT * FROM therapeutic_areas";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			//$row = $stmt->fetch();
			while ($row = $stmt->fetch())
			  	{
			  
					  $res[]=$row;
			  	} 
			$this->closeDB($dbCon);
			return $res;
			
	
	
}//END of function

function insertOffer($offer_value,$reffering_string)
{
    $sql = "INSERT INTO `offers` (`reffering_string`, `offer_value`, `user_ref_str`, `user_id`) VALUES (:reffering_string, :offer_value, :user_ref_str, :user_id);";
    // print_r($sql); exit;
    $Con = $this->connectDB();
    $q = $Con->prepare($sql);
    $str=$this->checkForUniq();
    if ($q->execute(
        [
            ':reffering_string' => $reffering_string,
            ':offer_value' => $offer_value,
            ':user_ref_str' => $str,
			':user_id'=>$_SESSION['luser_id']
        ]
    )) {
        $this->closeDB($Con);
        return true;
    } else {
        $this->closeDB($Con);

        return false;
    }
}

function checkForUniq(){
	$str=$this->randomString();
	$sql = "SELECT user_ref_str FROM `offers` WHERE user_ref_str='".$str."'";
	$dbCon=$this->connectDB();
	$stmt = $dbCon->prepare($sql);
	$stmt->execute();

	$res=$stmt->rowCount();
	$this->closeDB($dbCon);
	if($res > 0){
		$str= $this->checkForUniq();
	}
	return $str;
}

function getOfferKey($user_id){
    $sql="SELECT * FROM offers WHERE user_id = ".$user_id;
    //exit;
    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();

    $res=$stmt->fetch();
    $this->closeDB($dbCon);
    return $res;
}

function getOfferCount($ref){
    $sql="SELECT * FROM offers WHERE `reffering_string` = '".$ref."'";
    //exit;
    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();

    $res=$stmt->rowCount();
    $this->closeDB($dbCon);
    return $res;
}
function get_country_list(){
	
	$sql="SELECT * FROM countries ORDER BY country_name ASC";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			//$row = $stmt->fetch();
			while ($row = $stmt->fetch())
			  	{
			  
					  $res[]=$row;
			  	} 
			$this->closeDB($dbCon);
			return $res;
	
}

/**
 * Get country name by ID
 * @countryID = ID
 * 
 * @return $row['country_name'] = string
 */
/*function getCountryByID($countryID) {
    $sql = "SELECT country_name FROM countries WHERE id='".$countryID."'";

    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetch();
    $this->closeDB($dbCon);
    
    return $row['country_name'];
}*/
function GetPremmiumStatus($user_id){
    $sql="SELECT * FROM `premium_connect_subscriptions` WHERE `user_id`=".$user_id." AND `valid_till` > NOW()";
    //exit;
    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();
    $row=$stmt->fetch();
    $this->closeDB($dbCon);
    if(date('Y-m-d',strtotime($row['date_modified'].'+1 Year')) == date('Y-m-d',strtotime($row['valid_till']))){
        return true;
    }else{
        return false;
    }

}
function get_investigator_list(){
	
	$sql="SELECT * FROM custom16_csd_in_details WHERE user_id='".$_SESSION['luser_id']."' AND in_confirm=1 AND deleted='0'";
	
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$inv = array();
			//$row = $stmt->fetch();
			while ($row = $stmt->fetch())
			  	{
			  
					  $inv[]=$row;
			  	} 
			$this->closeDB($dbCon);
			return $inv;
	
}

function get_geographic_reasons(){
	
	$sql="SELECT * FROM geographic_reasons ORDER BY name ASC";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			//$row = $stmt->fetch();
			while ($row = $stmt->fetch())
			  	{
			  
					  $res[]=$row;
			  	} 
			$this->closeDB($dbCon);
			return $res;
	
}
function getUserid(){
	
	
	//secho
	$sql="SELECT user_id FROM custom16_signup_users WHERE email='".$_SESSION['lemail']."'";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			$row = $stmt->fetch();
			
			if(!empty($row[0])){
				
				
				$this->closeDB($dbCon);
				return $row[0];
			}else{
				
				$this->closeDB($dbCon);
				return false;
				//echo "ERROR, User not specified !".
				
				//exit;
			}
			
			
}


function serviceproviders_details(){
	//secho
    $sql="SELECT * FROM custom16_serviceproviders_details WHERE user_id='".$_SESSION['luser_id']."'";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$row = $stmt->fetch();
			if(!empty($row[0])){
				$this->closeDB($dbCon);
				return $row;
			}else{
				$this->closeDB($dbCon);
				return false;
				//echo "ERROR, User not specified !".
				//exit;
			}
}

function create_profile($inArr,$is_ajax){

    if($is_ajax){
        $profile_status=0;
    }
    else{
        $profile_status=1;
    }
	//$this->printr($inArr);
		if($inArr['User']=="trial_manager"){
			
			if(!$this->create_profile_for_trail_manager($inArr,$profile_status)){
				return false;
			}
			return true;
			
		}
	
	if($inArr['User']=="service_provider"){
			
			if(!$this->create_profile_for_service_provider($inArr,$profile_status)){
				return false;
			}
			return true;
			
		}
		if($inArr['User']=="investigator"){
			
			if(!$this->create_profile_for_investigator($inArr,$profile_status)){
				return false;
			}
			return true;
			
		}
		
		if($inArr['User']=="clinical_stud_coordinator"){
			
			if(!$this->create_profile_for_csd($inArr,$profile_status)){
				
				return false;
			}
			return true;
			
		}
	
	//exit;
	
	
}
function create_response($inArr){
    if(isset($inArr['service_types']) && $inArr['service_types'] !=""){
        $service_types = implode(",",$inArr['service_types']);
    }else{
        $service_types ="";
    }
    
    if(isset($inArr['number_of_patients']) && $inArr['number_of_patients'] !=""){
        $number_of_patients = $inArr['number_of_patients'];
    }else{
        $number_of_patients ="";
    }
    
    if(isset($inArr['investigator']) && $inArr['investigator'] !=""){
        $investigator = $inArr['investigator'];
    }else{
        $investigator = $_SESSION['luser_id']; //user self is an investigator
    }
    //print_r($inArr); exit;
    $sql="INSERT INTO `project_response` (`user_id`, `project_id`, `email`, `role`, `number_patients`, `investigator`, `service_types`, `promoted`, `date_created`) VALUES (:userid, :project_id, :email, :role, :number_patients,:investigator,:service_types, :promoted, :date_created);";
	// print_r($sql); exit;
    $Con=$this->connectDB();
	$q = $Con->prepare($sql);
	if ($q->execute(
        array(
            ':userid'          => $_SESSION['luser_id'],
            ':project_id'      => $inArr['project_ID'],
            ':email'           => $_SESSION['lemail'],
            ':role'            => $_SESSION['lrole'],
            ':number_patients' => $number_of_patients,
            ':investigator'    => $investigator,
            ':service_types'   => $service_types,
            'promoted'         => $inArr['promote'],
            'date_created'     => $this->dateCreated
        )
    )) {
		$this->closeDB($Con);
		return true;
	} else {
		$this->closeDB($Con);
		
		return false;
	}
}

    function create_profile_for_csd($inArr, $profile_status=0)
    {
        $therapeutic_areas = isset($inArr['therapeutic_area_of_work']) ? implode(",",$inArr['therapeutic_area_of_work']) : '';
        $country           = isset($inArr['country_of_work']) ? $inArr['country_of_work'] : '';
        $acceptTerms       = isset($acceptTerms)?$acceptTerms:0;
    	$dateSome          = date("Y-m-d",strtotime($inArr['start_working']));
		$detid             = (isset($_SESSION['csc_detail_id']) && !empty($_SESSION['csc_detail_id'])) ? $_SESSION['csc_detail_id'] : $this->MaxId("custom16_csd_details","det_id");
		$dbCon             = $this->connectDB();

        $sql = "INSERT INTO `custom16_csd_details`
		 (`det_id`, `user_id`, `phone_no`, `mobile_no`,`country_of_work`,`qualifications`, 
		 `ques1`, `smo_name`, `smo_details`,`therpeutic_area_of_work`, 
		 `sub_therapeutic_area_of_work`,  `start_working`,`no_of_years_of_clinical_trails`,`number_of_trials`, `publications`, `affliations`,`accreditations`, `patents`, `accept_terms`) 
		           VALUES(:detid,:userid,:phone,:mobile,:country,:qualifications,:ques1,:smo,:smodet,:tarea,
			         :starea,:start_working,:years,:no_of_trials,:publications,:affliations,:accreditations,
				:patents,:terms) ON DUPLICATE KEY UPDATE phone_no = :phone,mobile_no = :mobile, country_of_work = :country, qualifications = :qualifications, ques1 = :ques1, smo_name = :smo, smo_details = :smodet, therpeutic_area_of_work = :tarea , sub_therapeutic_area_of_work = :starea , start_working = :start_working, no_of_years_of_clinical_trails=:years, number_of_trials = :no_of_trials, publications= :publications , affliations=:affliations,accreditations = :accreditations,patents = :patents,accept_terms = :terms";
			$q = $dbCon->prepare($sql);
			
			$user=$this->getUserid();

			$q->bindParam(":detid",$detid,PDO::PARAM_INT);
			$q->bindParam(":userid",$user,PDO::PARAM_INT);
			$q->bindParam(":phone",$inArr['phone_no'],PDO::PARAM_STR);
			$q->bindParam(":mobile",$inArr['mobile_no'],PDO::PARAM_STR);
			$q->bindParam(":country",$country,PDO::PARAM_INT);
			$q->bindParam(":qualifications",$inArr['qualifications'],PDO::PARAM_INT);//qualifications

			$q->bindParam(":ques1", $inArr['a'],PDO::PARAM_STR);//Available and willing to participate in new clinical trial opportunities?
			$q->bindParam(":smo",$inArr['org_name'],PDO::PARAM_STR);//Available and willing to participate in new consulting opportunities?

			$q->bindParam(":smodet",$inArr['address'],PDO::PARAM_STR);//Available and willing to participate in new consulting opportunities?
			$q->bindParam(":tarea",$therapeutic_areas,PDO::PARAM_INT);//therapeutic area of work

			$q->bindParam(":starea",$inArr['sub-therapeutic_area_of_work'],PDO::PARAM_STR);//Sub therapeutic area of work
			$q->bindParam(":start_working",$dateSome,PDO::PARAM_STR);//start_working

			$q->bindParam(":years",$inArr['no_of_years'],PDO::PARAM_INT);//Number of years of clinical trails

			$q->bindParam(":no_of_trials",$inArr['number_of_trials'],PDO::PARAM_INT); //Number of trails


			$q->bindParam(":publications",$inArr['publications'],PDO::PARAM_STR); //Publications
			$q->bindParam(":affliations",$inArr['affiliations'],PDO::PARAM_STR); //Affliations

			$q->bindParam(":accreditations",$inArr['accreditations'],PDO::PARAM_STR); //Accrediations

			$q->bindParam(":patents",$inArr['patents'],PDO::PARAM_STR); //Patents

			$q->bindParam(":terms",$acceptTerms,PDO::PARAM_INT); //Terms

			if(!$q->execute()){
				$this->closeDB($dbCon);
				return false;
			}


			$incount=sizeof($inArr['name_of_investigator']);
			
			for($i=0;$i<=$incount;$i++){
				
				if(!empty($inArr['name_of_investigator'][$i]) and !empty($inArr['email_investigator'][$i])){
					
					
					$data['name']      = $inArr['name_of_investigator'][$i];
                    $data['det_id']    = $inArr['det_id'][$i];
					$data['email']     = $inArr['email_investigator'][$i];
					$data['csd_name']  = $inArr['firstName'];
					$data['csd_surname'] = $inArr['familyName'];
					$data['csd_email'] = $inArr['email'];
					
					$this->add_csd_in_details($user,$data);
				}
				
			}
			
			
			//$inArr['name_of_site']=array_filter($inArr['name_of_site']);
			$sites=count($inArr['name_of_site']);
			//echo "sites---->".$sites;
			//exit;
			
			
			for($i=0;$i<=$sites;$i++){
				
			if(!empty($inArr['name_of_site'][$i]) and !empty($inArr['address1'][$i]) and !empty($inArr['address2'][$i]) and !empty($inArr['city'][$i]) and !empty($inArr['state_province'][$i]) and !empty($inArr['site_country'][$i]) and !empty($inArr['site_phone'][$i])){
				
				
				$data['name_of_site']=$inArr['name_of_site'][$i];
				$data['address1']=$inArr['address1'][$i];
				$data['address2']=$inArr['address2'][$i];
				$data['site_id']=$inArr['site_id'][$i];
				$data['city']=$inArr['city'][$i];
				
				$data['state_province']=$inArr['state_province'][$i];
				$data['site_country']=$inArr['site_country'][$i];
				$data['site_phone']=$inArr['site_phone'][$i];
    
					//$data
				
				$this->add_in_sites($user,$data);
			}
				
				
		}

		$sql="UPDATE `custom16_signup_users` SET prefix='".$inArr['prefix']."',alt_email='".$inArr['alt-email']."', name='".$inArr['firstName']."', last_name='".$inArr['familyName']."', profile_status=".$profile_status." WHERE user_id=".$user;
		//exit;
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		$this->closeDB($dbCon);
		
		return true;    	
    }

function add_in_sites($user,$det){ 
	
	if($det['site_id'] == ''){
        $sid=$this->MaxId("custom16_csd_in_sites","site_id");


        $sql="INSERT INTO `custom16_csd_in_sites` (`site_id`, `user_id`, `site_name`, `address1`, `address2`, `city`, `state`, `country`, `site_phone`) VALUES (:siteid, :userid, :sitename, :address1, :address2, :city,:state,:country, :sphone);";

        $Con=$this->connectDB();
        $q = $Con->prepare($sql);

        if($q->execute(array(':siteid'=>$sid,':userid'=>$user,':sitename'=>$det['name_of_site'],':address1'=>$det['address1'],':address2'=>$det['address2'],':city'=>$det['city'],':state'=>$det['state_province'],':country'=>$det['site_country'],':sphone'=>$det['site_phone']))){

            $this->closeDB($Con);
            return true;

        }else {

            /*$this->printr(var_dump($q->errorInfo()));
            $this->printr(var_dump($q->errorCode()));
            //exit;*/
            //$this->closeDB($Con);
            return false;
        }
	}
	else{
        $sql="UPDATE `custom16_csd_in_sites` SET site_name = '".$det['name_of_site']."' , address1 = '".$det['address1']."' , address2 = '".$det['address2']."' , city = '".$det['city']."' , state = '".$det['state_province']."' , country = '".$det['site_country']."' , site_phone = '".$det['site_phone']."' WHERE `site_id` = ".$det['site_id'];
        $Con=$this->connectDB();
        $q = $Con->prepare($sql);
        if($q->execute()){
            $this->closeDB($Con);
            return true;
        }else{
            return false;
        }
		
	}
	
	
}//END function
function add_csd_in_details($user,$data){
    if($data['email']){
        $sql="SELECT * FROM `custom16_csd_in_details` WHERE coordinator_email = '".$data['csd_email']."' AND investigator_email = '".$data['email']."' AND deleted='0'";
        $Con=$this->connectDB();
        $q = $Con->prepare($sql);
        if ($q->execute() && $upd=$q->fetch()) {
            $sub_sql = '';
            if (isset($data['email']) && !empty($data['email']) ) {
                $sub_sql .= " investigator_email = '".$data['email']."', ";
            }
            if (isset($data['name']) && !empty($data['name']) ) {
                $sub_sql .= " investigator_name = '".$data['name']."', ";
            }


            $sql1 = "UPDATE `custom16_csd_in_details` SET ".$sub_sql." user_id = '".$user."' , coordinator_name = '".$data['csd_name']."' , coordinator_surname = '".$data['csd_surname']."' WHERE det_id = '".$upd['det_id']."'";
            $q = $Con->prepare($sql1);
            if($q->execute()){
                $this->closeDB($Con);
                return true;
            }else{
                $this->closeDB($Con);
                return false;
            }
        }else{
            $this->closeDB($Con);
            if($data['det_id'] == ''){
                $detid=$this->MaxId("custom16_csd_in_details","det_id");

                $sql="INSERT INTO `custom16_csd_in_details` (`det_id`, `user_id`, `coordinator_name`, `coordinator_surname`, `coordinator_email`, `investigator_name`, `investigator_email`) VALUES (:detid, :user, :csdname, :csdsurname, :csdemail, :name, :email)";


                $Con=$this->connectDB();
                $q = $Con->prepare($sql);

                //echo "Hello1";
                if($q->execute(array(':detid'=>$detid,':user'=>$user,':csdname'=>$data['csd_name'],':csdsurname'=>$data['csd_surname'],':csdemail'=>$data['csd_email'],':name'=>$data['name'],':email'=>$data['email']))){

                    $this->closeDB($Con);
                    return true;

                }else{


                    //$this->printr(var_dump($q->errorInfo()));
                    //$this->printr(var_dump($q->errorCode()));
                    $this->closeDB($Con);

                    return false;
                }
            }else{
                $sql="UPDATE `custom16_csd_in_details` SET coordinator_name = '".$data['csd_name']."' , coordinator_surname = '".$data['csd_surname']."' , coordinator_email = '".$data['csd_email']."' , investigator_name = '".$data['name']."' , investigator_email = '".$data['email']."' WHERE `det_id` =".$data['det_id'];
                $Con=$this->connectDB();
                $q = $Con->prepare($sql);
                if($q->execute()){
                    $this->closeDB($Con);
                    return true;
                }else{
                    $this->closeDB($Con);
                    return false;
                }
            }
        }
    }
	
}
function create_profile_for_investigator($inArr,$profile_status=0)
{

    $therapeutic_areas = (isset($inArr['therapeutic_area_of_work'])) ? implode(",", $inArr['therapeutic_area_of_work']) : '';
    $qualifications    = (isset($inArr['qualifications'])) ? implode(",", $inArr['qualifications']) : '';
    $start_working     = (isset($inArr['start_working'])) ? date("Y-m-d", strtotime($inArr['start_working'])) : '';
    $country=isset($inArr['country_of_work'])?$inArr['country_of_work']:'';
    $acceptTerms=isset($acceptTerms)?$acceptTerms:0;
    $detid=$this->MaxId("custom16_investigator_details","det_id");
	
		$dbCon=$this->connectDB();

    $sql="INSERT INTO `custom16_investigator_details` 
							(`det_id`, `user_id`, `phone_no`, `mobile_no`, `country_of_work`, 
							`ques1`, `ques2`,`qualifications`,`other_qual`, `therpeutic_area_of_work`, `sub_therapeutic_area_of_work`,`start_working`,
							 `no_of_years_of_clinical_trails`, `medica_practice_exp`, `number_of_trials`, 
							 `publications`, `affliations`, `Accreditations`, `Patents`, `accept_terms`) 
												  VALUES (:detid,:userid,:phone,:mobile,:country,:ques1,:ques2,:qualifications,:other_qual,
												  :tarea,:starea,:start_working,:years,:exp,:trails,:publications,:affliations,:accreditations,
										  :patents,:terms)  ON DUPLICATE KEY UPDATE Patents = :patents,Accreditations = :accreditations,affliations =:affliations ,publications = :publications ,number_of_trials = :trails,medica_practice_exp = :exp ,no_of_years_of_clinical_trails =:years ,start_working = :start_working,other_qual = :other_qual,ques2 = :ques2,ques1 = :ques1,country_of_work = :country,qualifications = :qualifications,phone_no = :phone ,mobile_no = :mobile,therpeutic_area_of_work =:tarea ,sub_therapeutic_area_of_work = :starea,accept_terms = :terms ";
					
				//Trace the query below.						  
										  
				 /*$sql="INSERT INTO `custom16_investigator_details` 
							(`det_id`, `user_id`, `phone_no`, `mobile_no`,`country_of_work`,`ques1`, `ques2`,`therpeutic_area_of_work`, `sub_therapeutic_area_of_work`) 
												  VALUES (:detid,:userid,:phone,:mobile,:country,:ques1,:ques2)";*/						 
					$q = $dbCon->prepare($sql);
					
					$user = (isset($_SESSION['lemail'])) ? $this->getUserid() : $inArr['user_id'];
					$q->bindParam(":detid",$detid,PDO::PARAM_INT);
					$q->bindParam(":userid",$user,PDO::PARAM_INT);
					$q->bindParam(":phone",$inArr['phone_no'],PDO::PARAM_STR);
					$q->bindParam(":mobile",$inArr['mobile_no'],PDO::PARAM_STR);
					$q->bindParam(":country",$country,PDO::PARAM_INT);
					//echo $inArr['ques1'];
					//echo $inArr['ques2'];exit;

					//$test1="hello";
					//$test2="hello";
					$q->bindParam(":ques1", $inArr['ques1'],PDO::PARAM_STR);//Available and willing to participate in new clinical trial opportunities?
					$q->bindParam(":ques2",$inArr['ques2'],PDO::PARAM_STR);//Available and willing to participate in new consulting opportunities?
					$q->bindParam(":qualifications",$qualifications,PDO::PARAM_STR);//qualifications
					$q->bindParam(":other_qual",$inArr['other_qual'],PDO::PARAM_STR);//other_qual


					$q->bindParam(":tarea",$therapeutic_areas,PDO::PARAM_INT);//therapeutic area of work

					$q->bindParam(":starea",$inArr['sub-therapeutic_area_of_work'],PDO::PARAM_STR);//Sub therapeutic area of work
					$q->bindParam(":start_working", $start_working, PDO::PARAM_STR);//start_working
					$q->bindParam(":years",$inArr['no_of_years'],PDO::PARAM_INT);//Number of years of clinical trails
					$q->bindParam(":exp",$inArr['practice_of_years'],PDO::PARAM_INT);//Total medical practice experience
					$q->bindParam(":trails",$inArr['number_of_trials'],PDO::PARAM_INT); //Number of trails


					$q->bindParam(":publications",$inArr['publications'],PDO::PARAM_STR); //Publications
					$q->bindParam(":affliations",$inArr['affiliations'],PDO::PARAM_STR); //Affliations
					$q->bindParam(":accreditations",$inArr['accreditations'],PDO::PARAM_STR); //Accrediations

					$q->bindParam(":patents",$inArr['patents'],PDO::PARAM_STR); //Patents

					$q->bindParam(":terms",$acceptTerms,PDO::PARAM_INT); //Terms

					if(!$q->execute()){
						
						$this->closeDB($dbCon);
						
							/*$this->printr(var_dump($q->errorInfo()));
							$this->printr(var_dump($q->errorCode()));
							exit;*/
						return false;
					}
					//$this->printr($inArr);
					
					//exit;
					//echo "Trails--->".$inArr['number_of_trials'];
					
					//Start checking and adding trails
					//$inArr=array_filter($inArr);
					if($inArr['number_of_trials']>0){ 
							
							for($i=0;$i<=$inArr['number_of_trials'];$i++){
								
								if(!empty($inArr['title'][$i]) and !empty($inArr['population'][$i]) and !empty($inArr['cur_status'][$i])){
									//++i; 
									$data['trail_id']=$inArr['trail_id'][$i];
									$data['title']=$inArr['title'][$i];//Title of trial
									$data['population']=$inArr['population'][$i];//Patient population details
									$data['patient_rand_num']=$inArr['patient_rand_num'][$i];//Number of patients to be randomized
									$data['patient_rec_num']=$inArr['patient_rec_num'][$i];//Number of patietns recruited
									$data['rec_time']=$inArr['rec_time'][$i];//Period of recruiments (in months)
									$data['cur_status']=$inArr['cur_status'][$i];//Current status of trial
									
									$this->add_trials($user,$data);
							}
									
									/*if($this->add_trials($user,$data)){ 
								
											//echo "Added !";
									}*/
							}
							
					}//END checking and adding trails
			
					//Start checking and adding sitess
					$inArr['name_of_site']=array_filter($inArr['name_of_site']);
					$sites=count($inArr['name_of_site']);
					//echo $sites;
					//exit;
					for($i=0;$i<=$sites;$i++){
						
					if(!empty($inArr['name_of_site'][$i]) and !empty($inArr['address1'][$i]) and !empty($inArr['address2'][$i]) and !empty($inArr['city'][$i]) and !empty($inArr['state_province'][$i]) and !empty($inArr['site_phone'][$i])){
                        $data['site_id']=$inArr['site_id'][$i];
						$data['name_of_site']=$inArr['name_of_site'][$i];
						$data['address1']=$inArr['address1'][$i];
						$data['address2']=$inArr['address2'][$i];
						$data['city']=$inArr['city'][$i];
						
						$data['state_province']=$inArr['state_province'][$i];
						$data['site_country']=$inArr['site_country'][$i];
						$data['site_phone']=$inArr['site_phone'][$i];
							//$data
						
						$this->add_sites($user,$data);
						
					}
				}
				//END checking and adding sitess
				//exit;
    		$sql="UPDATE `custom16_signup_users` SET prefix='".$inArr['prefix']."',alt_email='".$inArr['alt-email']."',name='".$inArr['firstName']."', last_name='".$inArr['familyName']."', profile_status=".$profile_status." WHERE user_id=".$user;
			//exit;
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$this->closeDB($dbCon);
			
			
			
			return true;
			
			
				
	
}//END function

function add_sites($user, $siteData){
	if($siteData['site_id'] == ''){
        $sid = $this->MaxId("custom16_in_sites","site_id");
        $sql = "INSERT INTO `custom16_in_sites` (`site_id`, `user_id`, `site_name`, `address1`, `address2`, `city`, `state`, `country`, `site_phone`) VALUES (:siteid, :userid, :sitename, :address1, :address2, :city,:state,:country, :sphone);";
        $Con = $this->connectDB();
        $q = $Con->prepare($sql);

        if($q->execute(array(':siteid'=>$sid,':userid'=>$user,':sitename'=>$siteData['name_of_site'],':address1'=>$siteData['address1'],':address2'=>$siteData['address2'],':city'=>$siteData['city'],':state'=>$siteData['state_province'],':country'=>$siteData['site_country'],':sphone'=>$siteData['site_phone']))){
            $this->closeDB($Con);
            return true;
        }else{
            $this->closeDB($Con);
            return false;
        }
	}
	else{
        $sql="UPDATE `custom16_in_sites` SET site_name = '".$siteData['name_of_site']."' , address1 = '".$siteData['address1']."' , address2 = '".$siteData['address2']."' , city = '".$siteData['city']."' , state = '".$siteData['state_province']."' ,country = '".$siteData['site_country']."' , site_phone = '".$siteData['site_phone']."' WHERE `site_id` = ".$siteData['site_id'];
        $Con=$this->connectDB();
        $q = $Con->prepare($sql);
        if($q->execute()){
            $this->closeDB($Con);
            return true;
        }else{
            return false;
        }
	}
}
    
function add_trials($user,$trails){
    $status=strtolower($trails['cur_status']);
	if($trails['trail_id'] == ''){
        $tid=$this->MaxId("custom16_in_trails","trail_id");
        $sql="INSERT INTO `custom16_in_trails` (`trail_id`, `user_id`, `title`, `patient_population_details`, `numberof_patients_to_be_randomized`, `numberof_patients_to_be_recruited`, `period_of_recruitments`, `status`) VALUES (:trailid,:userid,:title,:pdetails,:number1,:number2,:number3,:status1);";
        $Con=$this->connectDB();
        $q = $Con->prepare($sql);
        //if($q->execute(array(':trailid'=>$tid,':userid'=>$user,':title'=>$trails['title']))){



        //echo $status;exit;
        if($q->execute(array(':trailid'=>$tid,':userid'=>$user,':title'=>$trails['title'],':pdetails'=>$trails['population'],':number1'=>$trails['patient_rand_num'],':number2'=>$trails['patient_rec_num'],':number3'=>$trails['rec_time'],':status1'=>$status))){


            $this->closeDB($Con);
            return true;
        }else{

            $this->closeDB($Con);
            /*$this->printr(var_dump($q->errorInfo()));
            $this->printr(var_dump($q->errorCode()));
            var_dump($q->debugDumpParams());
            exit;*/
            return false;
        }
	}
	else{
        $sql="UPDATE `custom16_in_trails` SET title = '".$trails['title']."' , patient_population_details = '".$trails['population']."' , numberof_patients_to_be_randomized = '".$trails['patient_rand_num']."' , numberof_patients_to_be_recruited = '".$trails['patient_rec_num']."' , period_of_recruitments = '".$trails['rec_time']."' ,status = '".$status."' WHERE `trail_id` = ".$trails['trail_id'];
        $Con=$this->connectDB();
        $q = $Con->prepare($sql);
        if($q->execute()){
            $this->closeDB($Con);
            return true;
        }else{
            return false;
        }
	}
	
}

function file_upload($inArr){ 
	
	//$this->printr($inArr);
	//echo "Hello";
	$filename = basename($_FILES['uploaded_file']['name']);
  	$ext = substr($filename, strrpos($filename, '.') + 1);
    $size = $inArr['size'];
    //echo $ext;exit;
	if(!$this->extension($ext)){
		return false;
	}
	if(!$this->size($size)){
			
		return false;
	}
	//$dir="/home/csn/public_html/myzone/uploads/";/*server path*/
  //  $dir="/opt/lampp/htdocs/csn/myzone/uploads/";/*localhost for pc*/
  $dir="/Xampp/htdocs/csn3/myzone/uploads/";/*localhost for pc*/ 
	$newname = $dir.$this->getUserid()."-".$filename;
	//echo $newname;exit;
	 if(file_exists($newname)){
		 
		 return false;
	 }
	//echo $_FILES['uploaded_file']['tmp_name'];
	//echo $newname."<br>";
	  if (move_uploaded_file($_FILES['uploaded_file']['tmp_name'],$newname)) {
	      //echo "uplodaded";exit;
		  $res['filename']=$filename;
		  $res['path']=$newname;
		  
		  return $res;
		  
	  }else{
		 return false; 
		// echo $_FILES['uploaded_file']['error'];
		// exit;
	  }
	 
	 
	 
	
}
function create_profile_for_service_provider($inArr,$profile_status=0){
	$dbCon=$this->connectDB();
		$file='';
		/*if(!$file=$this->file_upload($_FILES['uploaded_file'])){ 
			return false;
		}*/
		$other_services="";
		$terms=0;
    	$acceptTerms=isset($inArr['acceptTerms']) ? $inArr['acceptTerms'] : 0;
    	$country=isset($inArr['country_of_work'])?$inArr['country_of_work']:'';
    if($this->serviceproviders_details() > 0){
		//$sql = "INSERT INTO `custom16_serviceproviders_details` (`detid`, `user_id`, `qualifications`, `country_of_work`,`service_types`, `other_services`, `name_of_organization`, `address_of_organization`, `phone_no`, `mobile_no`, `file_name`, `file_path`, `user_type`, `accept_terms`) VALUES (:detid,:userid,:qualifications,:country,:services,:other_service,:orgname,:orgaddress,:phone,:mobile,:filename,:filepath,:usertype,:term)";
		$sql = "UPDATE `custom16_serviceproviders_details` SET qualifications='".($inArr['qualifications'])."', country_of_work='".($country)."', service_types='".($inArr['services'])."', other_services='".($other_services)."', name_of_organization='".($inArr['org_name'])."', address_of_organization='".($inArr['address_of_organization'])."', phone_no='".($inArr['phone_no'])."', mobile_no='".($inArr['mobile_no'])."',file_name='".($inArr['links'])."', file_path='".($inArr['links'])."', user_type='".($inArr['service_provider'])."',	accept_terms='".($acceptTerms)."' WHERE user_id='".$_SESSION['luser_id']."'";
		// print_r($sql); exit;
		$q = $dbCon->prepare($sql);
		if(!$q->execute()){
				
				$this->closeDB($dbCon);
				return false;
				/*$this->printr(var_dump($q->errorInfo()));
				$this->printr(var_dump($q->errorCode()));
				exit;*/
			}
		} else {
			$services=isset($inArr['services']) ? implode(",",$inArr['services']) : '';
        	$country=isset($inArr['country_of_work'])?$inArr['country_of_work']:'';
        	$detid=$this->MaxId("custom16_serviceproviders_details","detid");
			$user_id=$_SESSION['luser_id'];
			$sql = "INSERT INTO `custom16_serviceproviders_details` (`detid`, `user_id`, `qualifications`, `country_of_work`,`service_types`, `other_services`, `name_of_organization`, `address_of_organization`, `phone_no`, `mobile_no`, `file_name`, `file_path`, `user_type`, `accept_terms`) VALUES (:detid,:userid,:qualifications,:country,:services,:other_service,:orgname,:orgaddress,:phone,:mobile,:filename,:filepath,:usertype,:term)";
			// print_r($sql); exit;
			$q = $dbCon->prepare($sql);
			if(!$q->execute(array(':detid'=>$detid,':userid'=>$user_id,':qualifications'=>$inArr['qualifications'],':country'=>$country,':services'=>$services,':other_service'=>'',':orgname'=>$inArr['org_name'],':orgaddress'=>$inArr['address_of_organization'],':phone'=>$inArr['phone_no'],':mobile'=>$inArr['mobile_no'],':filename'=>$inArr['links'],':filepath'=>$inArr['links'],':usertype'=>$inArr['service_provider'],':term'=>$acceptTerms))){
				$this->closeDB($dbCon);
				return false;
				/*$this->printr(var_dump($q->errorInfo()));
				$this->printr(var_dump($q->errorCode()));
				exit;*/
			}
			
		}
		
			//$dbCon=$this->connectDB();
			//$q = $dbCon->prepare($sql);
			
			//$userid=$this->getUserid();
		  	/*$other_services="";
			$file_path="";
			$terms=0; */
			/*$stmt = $dbCon->prepare($sql);
					$stmt->execute();
					$this->closeDB($dbCon);
					
					return true;*/
			
			
		  			/*$q->bindParam(":detid",$detid,PDO::PARAM_INT);
					$q->bindParam(":userid",$this->getUserid(),PDO::PARAM_INT);
                    $q->bindParam(":qualifications",$inArr['qualifications'],PDO::PARAM_STR);
                    $q->bindParam(":country",$inArr['country_of_work'],PDO::PARAM_STR);
                    
                    $q->bindParam(":services",$inArr['services'] ,PDO::PARAM_STR);
					$q->bindParam(":other_service",$other_services,PDO::PARAM_STR);
                    $q->bindParam(":orgname",$inArr['org_name'],PDO::PARAM_STR);
					$q->bindParam(":orgaddress",$inArr['address_of_organization'],PDO::PARAM_STR);
					$q->bindParam(":phone",$inArr['phone_no'],PDO::PARAM_STR);
					$q->bindParam(":mobile",$inArr['mobile_no'],PDO::PARAM_STR);
					$q->bindParam(":filename",$inArr['links'],PDO::PARAM_STR);
					$q->bindParam(":filepath",$inArr['links'],PDO::PARAM_STR);
					$q->bindParam(":usertype",$inArr['service_provider'],PDO::PARAM_INT);
					$q->bindParam(":term",$inArr['acceptTerms'],PDO::PARAM_INT);
          
                    if(!$q->execute()){
					
							$this->closeDB($dbCon);
							return false;
							/*$this->printr(var_dump($q->errorInfo()));
							$this->printr(var_dump($q->errorCode()));
							exit;*/

    			$sql="UPDATE `custom16_signup_users` SET prefix='".$inArr['prefix']."',name='".$inArr['firstName']."', last_name='".$inArr['familyName']."',alt_email='".$inArr['alt-email']."', profile_status=".$profile_status." WHERE user_id=".$this->getUserid();
			//exit;
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$this->closeDB($dbCon);
			return true;
			//echo "hello";
			//exit;
			//return true;
}

function create_profile_for_trail_manager($inArr,$profile_status=0){
		$therapeutic_areas=isset($inArr['therapeutic_area'])?implode(",",$inArr['therapeutic_area']):'';
		$geographic_reasons=isset($inArr['geographic_reasons'])?implode(",",$inArr['geographic_reasons']):'';
		$country=isset($inArr['country_of_work'])?$inArr['country_of_work']:'';
		$acceptTerms=isset($acceptTerms)?$acceptTerms:0;
		$detid=$this->MaxId("custom16_trialmanagers_details","det_id");
		//$userid=$this->getUserid();
		//echo $therapeutic_areas."-".$detid."-".$geographic_reasons;
		//echo
		$sql = "INSERT INTO custom16_trialmanagers_details (`det_id`, `user_id`, `name_of_organization`,`position_in_organization`, 
		`address_of_organization`, `country_of_work`, `qualifications`, `phone_no`, `mobile_no`,`therapeutic_area`,`sub_therapeutic_area_of_work`, `geographic_reasons`, `accept_terms`)
		  VALUES(:detid,:userid,:orgname,:position_in_organization,:orgaddress,:country,:qualifications,:phone,:mobile,:tarea, :starea, :greason,:terms) ON DUPLICATE KEY UPDATE name_of_organization= :orgname ,position_in_organization=:position_in_organization,address_of_organization = :orgaddress,country_of_work = :country,qualifications = :qualifications,phone_no = :phone ,mobile_no = :mobile,therapeutic_area =:tarea ,sub_therapeutic_area_of_work = :starea, geographic_reasons = :greason ,accept_terms = :terms ";
		  
		  //exit;
		  	$dbCon=$this->connectDB();
			$q = $dbCon->prepare($sql);
			$userid=$this->getUserid();
			//echo "hello";



			$q->bindParam(":detid",$detid,PDO::PARAM_INT);
			$q->bindParam(":userid",$userid,PDO::PARAM_INT);
			$q->bindParam(":orgname",$inArr['name_of_organization'],PDO::PARAM_STR);
			$q->bindParam(":position_in_organization",$inArr['position_in_organization'],PDO::PARAM_STR);
			$q->bindParam(":orgaddress",$inArr['address_of_organization'],PDO::PARAM_STR);
			$q->bindParam(":country",$country,PDO::PARAM_INT);
			$q->bindParam(":qualifications",$inArr['qualifications'],PDO::PARAM_INT);
			$q->bindParam(":phone",$inArr['phone_no'],PDO::PARAM_INT);
			$q->bindParam(":mobile",$inArr['mobile_no'],PDO::PARAM_INT);
			$q->bindParam(":tarea",$therapeutic_areas,PDO::PARAM_STR);
			$q->bindParam(":starea",$inArr['sub-therapeutic_area_of_work'],PDO::PARAM_STR);

			$q->bindParam(":greason",$geographic_reasons,PDO::PARAM_STR);
			$q->bindParam(":terms",$acceptTerms,PDO::PARAM_STR);
//			var_dump($geographic_reasons);die;
				if(!$q->execute()){
					
					$this->closeDB($dbCon);
					return false;
					/*$this->printr(var_dump($q->errorInfo()));
					$this->printr(var_dump($q->errorCode()));
					exit;*/
				}
				
			//echo	
    		$sql="UPDATE `custom16_signup_users` SET prefix='".$inArr['prefix']."',name='".$inArr['firstName']."', last_name='".$inArr['familyName']."', alt_email='".$inArr['alt-email']."',profile_status=".$profile_status." WHERE user_id=".$userid;
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$this->closeDB($dbCon);
			return true;
		
	
	}//END of functions


//To check the duplicate entries

function modify_profile($inArr){
	//for trial managers
	if($inArr['UserRole']=="trial_manager" && $inArr['section']=="general"){
		return $this->UpdateTrialManagerGeneralInfo($inArr);
	}
    if ($inArr['UserRole']=="trial_manager" && $inArr['section']=="works") {
		return $this->UpdateTrialManagerWorks($inArr);
	}
	
    //for service provider
	if($inArr['UserRole']=="service_provider" && $inArr['section']=="general"){
		return $this->UpdateServiceProviderGeneralInfo($inArr);
	}
    if($inArr['UserRole']=="service_provider" && $inArr['section']=="works"){
		return $this->UpdateServiceProviderWorks($inArr);
	}
    
    //for investigators
	if ($inArr['UserRole']=="investigator" && $inArr['section']=="general") {
		return $this->UpdateInvestigatorGeneralInfo($inArr);
	}
	if ($inArr['UserRole']=="investigator" && $inArr['section']=="works") {
		return $this->UpdateInvestigatorWorks($inArr);
	}
	if ($inArr['UserRole']=="investigator" && $inArr['section']=="trial") {
		return $this->UpdateInvestigatorTrial($inArr);
	}
	if ($inArr['UserRole']=="investigator" && $inArr['section']=="site") {
		return $this->UpdateInvestigatorSite($inArr);
	}
	if ($inArr['UserRole']=="investigator" && $inArr['section']=="credentials") {
		return $this->UpdateInvestigatorCredentials($inArr);
	}
    
    //for coordinator
	if ($inArr['UserRole']=="clinical_stud_coordinator" && $inArr['section']=="general") {
		return $this->UpdateCoordinatorGeneralInfo($inArr);
	}
	if ($inArr['UserRole']=="clinical_stud_coordinator" && $inArr['section']=="works") {
		return $this->UpdateCoordinatorWorks($inArr);
	}
	if ($inArr['UserRole']=="clinical_stud_coordinator" && $inArr['section']=="site") {
		return $this->UpdateCoordinatorSite($inArr);
	}
	if ($inArr['UserRole']=="clinical_stud_coordinator" && $inArr['section']=="credentials") {
		return $this->UpdateCoordinatorCredentials($inArr);
	}

	return true;
}
function modify_profile_for_csd($inArr){
	$therapeutic_areas=implode(",",$inArr['therapeutic_area_of_work']);
		
		//$this->printr($inArr);
		//exit;
		//$detid=$this->MaxId("custom16_csd_details","det_id");
		$dbCon=$this->connectDB();
		$sql="UPDATE `custom16_signup_users` SET prefix='".$inArr['prefix']."',name='".$inArr['firstName']."', last_name='".$inArr['familyName']."', alt_email='".$inArr['alt-email']."',profile_status=1 WHERE user_id=".$userid;
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$this->closeDB($dbCon);
			return true;
		
		$sql="UPDATE `custom16_csd_details` SET phone_no='".$inArr['phone_no']."', mobile_no='".$inArr['mobile_no']."', country_of_work='".$inArr['country_of_work']."', qualifications='".$inArr['qualifications']."', ques1='".$inArr['a']."', smo_name='".$inArr['org_name']."', smo_details='".$inArr['address']."', therpeutic_area_of_work='".$therapeutic_areas."', sub_therapeutic_area_of_work='".$inArr['sub-therapeutic_area_of_work']."', start_working='".date("Y-m-d",strtotime($inArr['start_working']))."', no_of_years_of_clinical_trails='".$inArr['no_of_years']."', number_of_trials='".$inArr['number_of_trials']."', publications='".$inArr['publications']."', affliations='".$inArr['affiliations']."', accreditations='".$inArr['accreditations']."', patents='".$inArr['patents']."', accept_terms='".$inArr['acceptTerms']."' WHERE user_id=".$userid;
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$this->closeDB($dbCon);
			return true;
		
		/*$sql="INSERT INTO `custom16_csd_details`
				 (`det_id`, `user_id`, `phone_no`, `mobile_no`,`country_of_work`,`qualifications`, 
				 `ques1`, `smo_name`, `smo_details`,`therpeutic_area_of_work`, 
				 `sub_therapeutic_area_of_work`,  `start_working`,`no_of_years_of_clinical_trails`,`number_of_trials`, `publications`, `affliations`,`accreditations`, `patents`, `accept_terms`) 
				           VALUES(:detid,:userid,:phone,:mobile,:country,:qualifications,:ques1,:smo,:smodet,:tarea,
					         :starea,:start_working,:years,:no_of_trials,:publications,:affliations,:accreditations,
						:patents,:terms)";
						
				$q = $dbCon->prepare($sql);
					
					$user=$this->getUserid();
						
					$q->bindParam(":detid",$detid,PDO::PARAM_INT);
					$q->bindParam(":userid",$user,PDO::PARAM_INT);
					$q->bindParam(":phone",$inArr['phone_no'],PDO::PARAM_STR);
					$q->bindParam(":mobile",$inArr['mobile_no'],PDO::PARAM_STR);
					$q->bindParam(":country",$inArr['country_of_work'],PDO::PARAM_INT);
                    $q->bindParam(":qualifications",$inArr['qualifications'],PDO::PARAM_INT);//qualifications
                    
					$q->bindParam(":ques1", $inArr['a'],PDO::PARAM_STR);//Available and willing to participate in new clinical trial opportunities?
					$q->bindParam(":smo",$inArr['org_name'],PDO::PARAM_STR);//Available and willing to participate in new consulting opportunities?
					
					$q->bindParam(":smodet",$inArr['address'],PDO::PARAM_STR);//Available and willing to participate in new consulting opportunities?
					$q->bindParam(":tarea",$therapeutic_areas,PDO::PARAM_INT);//therapeutic area of work
					
					$q->bindParam(":starea",$inArr['sub-therapeutic_area_of_work'],PDO::PARAM_STR);//Sub therapeutic area of work
                    $q->bindParam(":start_working",date("Y-m-d",strtotime($inArr['start_working'])),PDO::PARAM_STR);//start_working
                    
					$q->bindParam(":years",$inArr['no_of_years'],PDO::PARAM_INT);//Number of years of clinical trails
					
					$q->bindParam(":no_of_trials",$inArr['number_of_trials'],PDO::PARAM_INT); //Number of trails
					
					
					$q->bindParam(":publications",$inArr['publications'],PDO::PARAM_STR); //Publications
					$q->bindParam(":affliations",$inArr['affiliations'],PDO::PARAM_STR); //Affliations
					
					$q->bindParam(":accreditations",$inArr['accreditations'],PDO::PARAM_STR); //Accrediations
					
					$q->bindParam(":patents",$inArr['patents'],PDO::PARAM_STR); //Patents
					
					$q->bindParam(":terms",$inArr['acceptTerms'],PDO::PARAM_INT); //Terms
	
					
					
					if(!$q->execute()){
						
						$this->closeDB($dbCon);
						
							$this->printr(var_dump($q->errorInfo()));
							$this->printr(var_dump($q->errorCode()));
							exit;
						//return false;
					}*/
		
		
					$incount=sizeof($inArr['name_of_investigator']);
					//echo $incount;
					
					for($i=0;$i<=$incount;$i++){
						
						if(!empty($inArr['name_of_investigator'][$i]) and !empty($inArr['email_investigator'][$i])){
							
							
							$data['name']=$inArr['name_of_investigator'][$i];
							$data['email']=$inArr['email_investigator'][$i];
							$data['csd_name']=$inArr['firstName'];
							$data['csd_surname']=$inArr['familyName'];
							$data['csd_email']=$inArr['email'];
							
							$this->modify_csd_in_details($user,$data);
						}
						
					}
					
					
					//$inArr['name_of_site']=array_filter($inArr['name_of_site']);
					$sites=count($inArr['name_of_site']);
					//echo "sites---->".$sites;
					//exit;
					
					
					for($i=0;$i<=$sites;$i++){
						
					if(!empty($inArr['name_of_site'][$i]) and !empty($inArr['address1'][$i]) and !empty($inArr['address2'][$i]) and !empty($inArr['city'][$i]) and !empty($inArr['state_province'][$i]) and !empty($inArr['site_country'][$i]) and !empty($inArr['site_phone'][$i])){
						
						
						$data['name_of_site']=$inArr['name_of_site'][$i];
						$data['address1']=$inArr['address1'][$i];
						$data['address2']=$inArr['address2'][$i];
						$data['city']=$inArr['city'][$i];
						
						$data['state_province']=$inArr['state_province'][$i];
						$data['site_country']=$inArr['site_country'][$i];
						$data['site_phone']=$inArr['site_phone'][$i];
            
							//$data
						
						$this->modify_csd_sites($user,$data);
					}
						
						
				}
					//exit;
					
					/*$sql="UPDATE `custom16_signup_users` SET prefix='".$inArr['prefix']."',alt_email='".$inArr['alt-email']."', name='".$inArr['firstName']."', last_name='".$inArr['familyName']."', profile_status=1 WHERE user_id=".$user;
					//exit;
					$stmt = $dbCon->prepare($sql);
					$stmt->execute();
					$this->closeDB($dbCon);
					
					return true;*/
					//exit;
				
				
				
	
	
}//END function

function modify_csd_sites($user,$det){ 
	
	
	$sid=$this->MaxId("custom16_csd_in_sites","site_id");
	
	
	$sql="INSERT INTO `custom16_csd_in_sites` (`site_id`, `user_id`, `site_name`, `address1`, `address2`, `city`, `state`, `country`, `site_phone`) VALUES (:siteid, :userid, :sitename, :address1, :address2, :city,:state,:country, :sphone);";
	
	$Con=$this->connectDB();
	$q = $Con->prepare($sql);
	
	if($q->execute(array(':siteid'=>$sid,':userid'=>$user,':sitename'=>$det['name_of_site'],':address1'=>$det['address1'],':address2'=>$det['address2'],':city'=>$det['city'],':state'=>$det['state_province'],':country'=>$det['site_country'],':sphone'=>$det['site_phone']))){
		
		 $this->closeDB($Con);
		return true;
				
	}else{
		
		/*$this->printr(var_dump($q->errorInfo()));
		$this->printr(var_dump($q->errorCode()));
		//exit;*/
		//$this->closeDB($Con);
		return false;
		
}//END function
	
	
}//END function
function modify_csd_in_details($user,$data){
	
	$detid=$this->MaxId("custom16_csd_in_details","det_id");
	
	//$this->printr($data);
	$sql="INSERT INTO `custom16_csd_in_details` (`det_id`, `user_id`, `coordinator_name`, `coordinator_surname`, `coordinator_email`, `investigator_name`, `investigator_email`) VALUES (:detid, :user, :csdname, :csdsurname, :csdemail, :name, :email)";

	
	$Con=$this->connectDB();
	$q = $Con->prepare($sql);
	
	//echo "Hello1";
	if($q->execute(array(':detid'=>$detid,':user'=>$user,':csdname'=>$data['csd_name'],':csdsurname'=>$data['csd_surname'],':csdemail'=>$data['csd_email'],':name'=>$data['name'],':email'=>$data['email']))){
						
		 	$this->closeDB($Con);
			return true;
				
	}else{
		
		
		//$this->printr(var_dump($q->errorInfo()));
		//$this->printr(var_dump($q->errorCode()));
		$this->closeDB($Con);
		
		return false;
	}
	
}


    function UpdateInvestigatorGeneralInfo($inArr){
        $qualifications = implode(",",$inArr['qualifications']);
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_signup_users u, custom16_investigator_details d 
            SET 
                u.prefix = '".$inArr['prefix']."', u.name = '".$inArr['firstName']."', u.last_name = '".$inArr['familyName']."', u.alt_email = '".$inArr['alt_email']."', 
                d.phone_no = '".$inArr['phone_no']."', d.mobile_no = '".$inArr['mobile_no']."', d.country_of_work = '".$inArr['country_of_work']."', 
                d.ques1 = '".$inArr['ques1']."', d.ques2 = '".$inArr['ques2']."', d.qualifications = '".$qualifications."', d.other_qual = '".$inArr['other_qual']."', 
                d.updated_date = now()
            WHERE u.user_id = d.user_id AND d.deleted = '0' AND u.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }

    function UpdateInvestigatorWorks($inArr){
        $therapeutic_areas = implode(",", $inArr['therapeutic_area_of_work']);
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_investigator_details d 
            SET 
                d.therpeutic_area_of_work = '".$therapeutic_areas."', d.sub_therapeutic_area_of_work = '".$inArr['sub-therapeutic_area_of_work']."', d.start_working = '".date("Y-m-d", strtotime($inArr['start_working']))."', 
                d.no_of_years_of_clinical_trails = '".$inArr['no_of_years']."', d.medica_practice_exp = '".$inArr['practice_of_years']."', d.number_of_trials = '".$inArr['number_of_trials']."', d.updated_date = now() 
            WHERE d.deleted = '0' AND d.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }

    function UpdateInvestigatorTrial($inArr){
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_in_trails t 
            SET 
                t.title = '".$inArr['title']."', t.patient_population_details = '".$inArr['population']."', 
                t.numberof_patients_to_be_randomized = '".$inArr['patient_rand_num']."', 
                t.numberof_patients_to_be_recruited = '".$inArr['patient_rec_num']."', t.period_of_recruitments = '".$inArr['rec_time']."', 
                t.status = '".$inArr['cur_status']."', t.updated_date = now() 
            WHERE t.trail_id = '".$inArr['record']."' AND t.deleted = '0' AND t.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }
    
    function DeleteInvestigatorTrial($id) {
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_in_trails t 
            SET 
                t.deleted = '1', t.updated_date = now() 
            WHERE t.trail_id = '".$id."' AND t.deleted = '0' AND t.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }

    function UpdateInvestigatorSite($inArr){
        $user = $this->getUserid();
        $dbCon = $this->connectDB();
        //echo '<pre>';print_r($inArr);echo '</pre>';
        $sql = "UPDATE custom16_in_sites s 
            SET 
                s.site_name = '".$inArr['name_of_site']."', s.address1 = '".$inArr['address1']."', 
                s.address2 = '".$inArr['address2']."', 
                s.city = '".$inArr['city']."', s.state = '".$inArr['state_province']."', 
                s.country = '".$inArr['site_country']."', s.site_phone = '".$inArr['site_phone']."', s.updated_date = now() 
            WHERE s.site_id = '".$inArr['record']."' AND s.deleted = '0' AND s.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }
    
    function DeleteInvestigatorSite($id) {
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_csd_in_sites s 
            SET 
                s.deleted = '1', s.updated_date = now() 
            WHERE s.site_id = '".$id."' AND s.deleted = '0' AND s.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }

    function UpdateInvestigatorCredentials($inArr){
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_investigator_details d 
            SET 
                d.publications = '".$inArr['publications']."', d.affliations = '".$inArr['affiliations']."', 
                d.Accreditations = '".$inArr['accreditations']."', d.Patents = '".$inArr['patents']."', d.updated_date = now()
            WHERE d.deleted = '0' AND d.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }
    
    function UpdateCoordinatorGeneralInfo($inArr){
        $qualifications = implode(",",$inArr['qualifications']);
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_signup_users u, custom16_csd_details d 
            SET 
                u.prefix = '".$inArr['prefix']."', u.name = '".$inArr['firstName']."', u.last_name = '".$inArr['familyName']."', u.alt_email = '".$inArr['alt-email']."', 
                d.phone_no = '".$inArr['phone_no']."', d.mobile_no = '".$inArr['mobile_no']."', d.country_of_work = '".$inArr['country_of_work']."', 
                d.ques1 = '".$inArr['ques1']."', d.smo_name = '".$inArr['smo_name']."', d.smo_details = '".$inArr['smo_details']."', d.qualifications = '".$qualifications."', 
                d.updated_date = now()
            WHERE u.user_id = d.user_id AND d.deleted = '0' AND u.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }

    function UpdateCoordinatorWorks($inArr){
        $therapeutic_areas = implode(",", $inArr['therapeutic_area_of_work']);
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_csd_details d 
            SET 
                d.therpeutic_area_of_work = '".$therapeutic_areas."', d.sub_therapeutic_area_of_work = :sub_therapeutic_area_of_work, d.start_working = '".date("Y-m-d", strtotime($inArr['start_working']))."', 
                d.no_of_years_of_clinical_trails = '".$inArr['no_of_years']."', d.number_of_trials = '".$inArr['number_of_trials']."', d.updated_date = now() 
            WHERE d.deleted = '0' AND d.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->bindValue(':sub_therapeutic_area_of_work', $inArr['sub-therapeutic_area_of_work']);
        
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }

    function UpdateCoordinatorSite($inArr){
        $user = $this->getUserid();
        $dbCon = $this->connectDB();
        //echo '<pre>';print_r($inArr);echo '</pre>';
        $sql = "UPDATE custom16_csd_in_sites s 
            SET 
                s.site_name = :name_of_site, s.address1 = :address1, 
                s.address2 = :address2, 
                s.city = :city, s.state = :state_province, 
                s.country = :site_country, s.site_phone = :site_phone, s.updated_date = now() 
            WHERE s.site_id = '".$inArr['record']."' AND s.deleted = '0' AND s.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->bindValue(':name_of_site', $inArr['name_of_site']);
        $stmt->bindValue(':address1', $inArr['address1']);
        $stmt->bindValue(':address2', $inArr['address2']);
        $stmt->bindValue(':city', $inArr['city']);
        $stmt->bindValue(':state_province', $inArr['state_province']);
        $stmt->bindValue(':site_country', $inArr['site_country']);
        $stmt->bindValue(':site_phone', $inArr['site_phone']);
        //$stmt->bindValue(':record', $inArr['record']);
        
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }
    
    function DeleteCoordinatorSite($id) {
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_csd_in_sites s 
            SET 
                s.deleted = '1', s.updated_date = now() 
            WHERE s.site_id = '".$id."' AND s.deleted = '0' AND s.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }

    function UpdateCoordinatorCredentials($inArr){
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_csd_details d 
            SET 
                d.publications = :publications, d.affliations = :affiliations, 
                d.accreditations = :accreditations, d.patents = :patents, d.updated_date = now()
            WHERE d.deleted = '0' AND d.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->bindValue(':publications', $inArr['publications']);
        $stmt->bindValue(':affiliations', $inArr['affiliations']);
        $stmt->bindValue(':accreditations', $inArr['accreditations']);
        $stmt->bindValue(':patents', $inArr['patents']);
        
        $stmt->execute();

        $this->closeDB($dbCon);
        return true;
    }
    
    /*
     * Fuction to delete Investigator of a Coordinator
     */
    function DeleteInvestigatorByCoordinator($id) {
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_csd_in_details s 
            SET 
                s.deleted = '1', s.updated_date = now() 
            WHERE s.det_id = '".$id."' AND s.deleted = '0' AND s.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();
        $this->closeDB($dbCon);
        
        return true;
    }
    
    function UpdateTrialManagerGeneralInfo($inArr){
        $qualifications = implode(",", $inArr['qualifications']);
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_signup_users u, custom16_trialmanagers_details d 
            SET 
                u.prefix = '".$inArr['prefix']."', u.name = :firstName, u.last_name = :familyName, u.alt_email = :alt_email, 
                d.phone_no = :phone_no, d.mobile_no = :mobile_no, d.country_of_work = '".$inArr['country_of_work']."', 
                d.qualifications = '".$qualifications."', d.name_of_organization = :name_of_organization, 
                d.position_in_organization = :position_in_organization, d.address_of_organization = :address_of_organization 
            WHERE u.user_id = d.user_id AND d.deleted = '0' AND u.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->bindValue(':firstName', $inArr['firstName']);
        $stmt->bindValue(':familyName', $inArr['familyName']);
        $stmt->bindValue(':alt_email', $inArr['alt_email']);
        $stmt->bindValue(':phone_no', $inArr['phone_no']);
        $stmt->bindValue(':mobile_no', $inArr['mobile_no']);
        $stmt->bindValue(':name_of_organization', $inArr['name_of_organization']);
        $stmt->bindValue(':position_in_organization', $inArr['position_in_organization']);
        $stmt->bindValue(':address_of_organization', $inArr['address_of_organization']);
        $stmt->execute();
        $this->closeDB($dbCon);
        
        return true;
    }
    
    function UpdateTrialManagerWorks($inArr){
        $therapeutic_areas  = implode(",", $inArr['therapeutic_area_of_work']);
        $geographic_reasons = implode(",", $inArr['geographic_reasons']);
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_trialmanagers_details d 
            SET 
                d.therapeutic_area = '".$therapeutic_areas."', d.sub_therapeutic_area_of_work = :sub_therapeutic_area_of_work, 
                d.geographic_reasons = '".$geographic_reasons."' 
            WHERE d.deleted = '0' AND d.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->bindValue(':sub_therapeutic_area_of_work', $inArr['sub-therapeutic_area_of_work']);
        $stmt->execute();
        $this->closeDB($dbCon);
        
        return true;
    }
    
    function UpdateServiceProviderGeneralInfo($inArr) {
        $qualifications = implode(",", $inArr['qualifications']);
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_signup_users u, custom16_serviceproviders_details d 
            SET 
                u.prefix = '".$inArr['prefix']."', u.name = :firstName, u.last_name = :familyName, u.alt_email = :alt_email, 
                d.phone_no = :phone_no, d.mobile_no = :mobile_no, d.country_of_work = '".$inArr['country_of_work']."', 
                d.qualifications = '".$qualifications."'  
            WHERE u.user_id = d.user_id AND d.deleted = '0' AND u.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->bindValue(':firstName', $inArr['firstName']);
        $stmt->bindValue(':familyName', $inArr['familyName']);
        $stmt->bindValue(':alt_email', $inArr['alt_email']);
        $stmt->bindValue(':phone_no', $inArr['phone_no']);
        $stmt->bindValue(':mobile_no', $inArr['mobile_no']);
        $stmt->execute();
        $this->closeDB($dbCon);
        
        return true;
    }
    
    function UpdateServiceProviderWorks($inArr){
        $user = $this->getUserid();
        $dbCon = $this->connectDB();

        $sql = "UPDATE custom16_serviceproviders_details d 
            SET 
                d.user_type = '".$inArr['user_type']."', d.name_of_organization = :name_of_organization, 
                d.address_of_organization = :address_of_organization, d.file_path = :file_path 
            WHERE d.deleted = '0' AND d.user_id = '".$user."'";
        $stmt = $dbCon->prepare($sql);
        $stmt->bindValue(':name_of_organization', $inArr['name_of_organization']);
        $stmt->bindValue(':address_of_organization', $inArr['address_of_organization']);
        $stmt->bindValue(':file_path', $inArr['file_path']);
        $stmt->execute();
        $this->closeDB($dbCon);
        
        return true;
    }

function modify_profile_for_investigator($inArr){
	$therapeutic_areas=implode(",",$inArr['therapeutic_area_of_work']);
	$qualifications=implode(",",$inArr['qualifications']);
	$user=$this->getUserid();
	$dbCon=$this->connectDB();
	
	$sql = "UPDATE `custom16_investigator_details` SET 
		phone_no='".$inArr['phone_no']."',mobile_no='".$inArr['mobile_no']."',country_of_work='".$inArr['country_of_work']."', ques1='".$inArr['ques1']."', 
		ques2='".$inArr['ques2']."',qualifications='".$qualifications."',other_qual='".$inArr['other_qual']."', therpeutic_area_of_work='".$therapeutic_areas."', 
		sub_therapeutic_area_of_work='".$inArr['sub-therapeutic_area_of_work']."',start_working='".date("Y-m-d",strtotime($inArr['start_working']))."',no_of_years_of_clinical_trails='".$inArr['no_of_years']."', medica_practice_exp='".$inArr['practice_of_years']."', 
		number_of_trials='".$inArr['number_of_trials']."',publications='".$inArr['publications']."',affliations='".$inArr['affiliations']."', Accreditations='".$inArr['accreditations']."', 
		Patents='".$inArr['patents']."',accept_terms='".$inArr['acceptTerms']."', updated_date=now() WHERE user_id=".$user;
	$stmt = $dbCon->prepare($sql);
	$stmt->execute();
	$sql1="UPDATE `custom16_in_trails` SET deleted= 1 WHERE user_id=".$user;
	$stmt = $dbCon->prepare($sql1);
	$stmt->execute();
	if($inArr['number_of_trials']>0){ 
		for($i=0;$i<=$inArr['number_of_trials'];$i++){
			if(!empty($inArr['title'][$i]) and !empty($inArr['population'][$i]) and !empty($inArr['cur_status'][$i])){
				$data['title']=$inArr['title'][$i];//Title of trial
				$data['population']=$inArr['population'][$i];//Patient population details
				$data['patient_rand_num']=$inArr['patient_rand_num'][$i];//Number of patients to be randomized
				$data['patient_rec_num']=$inArr['patient_rec_num'][$i];//Number of patietns recruited
				$data['rec_time']=$inArr['rec_time'][$i];//Period of recruiments (in months)
				$data['cur_status']=$inArr['cur_status'][$i];//Current status of trial
				$this->add_trials($user,$data);
			}				
		}
	}
	$sql2="UPDATE `custom16_in_sites` SET deleted= 1 WHERE user_id=".$user;
	$stmt = $dbCon->prepare($sql2);
	$stmt->execute();
	$inArr['name_of_site']=array_filter($inArr['name_of_site']);
	$sites=count($inArr['name_of_site']);
	for($i=0;$i<=$sites;$i++){
		if(!empty($inArr['name_of_site'][$i]) and !empty($inArr['address1'][$i]) and !empty($inArr['address2'][$i]) and !empty($inArr['city'][$i]) and !empty($inArr['state_province'][$i]) and !empty($inArr['site_phone'][$i])){
			$data['name_of_site']=$inArr['name_of_site'][$i];
			$data['address1']=$inArr['address1'][$i];
			$data['address2']=$inArr['address2'][$i];
			$data['city']=$inArr['city'][$i];
			$data['state_province']=$inArr['state_province'][$i];
			$data['site_country']=$inArr['site_country'][$i];
			$data['site_phone']=$inArr['site_phone'][$i];
			
			$this->add_sites($user,$data);	
		}
	}
//END checking and adding sitess
	$sql="UPDATE `custom16_signup_users` SET prefix='".$inArr['prefix']."',alt_email='".$inArr['alt-email']."',name='".$inArr['firstName']."', last_name='".$inArr['familyName']."', profile_status=1 WHERE user_id=".$user;
	$stmt = $dbCon->prepare($sql);
	$stmt->execute();
	$this->closeDB($dbCon);
	return true;
}//END function

function modify_profile_for_service_provider($inArr){
	
		$file='';
		
		$other_services="";
		$terms=0;
	    //$sql = "INSERT INTO `custom16_serviceproviders_details` (`detid`, `user_id`, `qualifications`, `country_of_work`,`service_types`, `other_services`, `name_of_organization`, `address_of_organization`, `phone_no`, `mobile_no`, `file_name`, `file_path`, `user_type`, `accept_terms`) VALUES (:detid,:userid,:qualifications,:country,:services,:other_service,:orgname,:orgaddress,:phone,:mobile,:filename,:filepath,:usertype,:term)";
		$sql = "UPDATE `custom16_serviceproviders_details` SET qualifications='".($inArr['qualifications'])."', country_of_work='".($inArr['country_of_work'])."', service_types='".($inArr['services'])."', other_services='".($other_services)."', name_of_organization='".($inArr['org_name'])."', address_of_organization='".($inArr['address_of_organization'])."', phone_no='".($inArr['phone_no'])."', mobile_no='".($inArr['mobile_no'])."',file_name='".($inArr['links'])."', file_path='".($inArr['links'])."', user_type='".($inArr['service_provider'])."',	accept_terms='".($inArr['acceptTerms'])."' WHERE user_id='".$_SESSION['luser_id']."'"; 
		
			$dbCon=$this->connectDB();
			$q = $dbCon->prepare($sql);
			if(!$q->execute()){
					
					$this->closeDB($dbCon);
					return false;
					
				}
			
				    
					$sql="UPDATE `custom16_signup_users` SET prefix='".$inArr['prefix']."',name='".$inArr['firstName']."', last_name='".$inArr['familyName']."',alt_email='".$inArr['alt-email']."', profile_status=1 WHERE user_id=".$this->getUserid();
			//exit;
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$this->closeDB($dbCon);
			return true;
			
}
function modify_profile_for_trail_manager($inArr){
		$therapeutic_areas=implode(",",$inArr['therapeutic_area']);
		$geographic_reasons=implode(",",$inArr['geographic_reasons']);
		$detid=$this->MaxId("custom16_trialmanagers_details","det_id");
		
		$sql = "INSERT INTO custom16_trialmanagers_details (`det_id`, `user_id`, `name_of_organization`,`position_in_organization`, 
		`address_of_organization`, `country_of_work`, `qualifications`, `phone_no`, `mobile_no`,`therapeutic_area`,`sub_therapeutic_area_of_work`, `geographic_reasons`, `accept_terms`)
		  VALUES(:detid,:userid,:orgname,:position_in_organization,:orgaddress,:country,:qualifications,:phone,:mobile,:tarea, :starea, :greason,:terms)";
		  
		  //exit;
		  	$dbCon=$this->connectDB();
			$q = $dbCon->prepare($sql);
			$userid=$this->getUserid();
			//echo "hello";
			
			
			
					$q->bindParam(":detid",$detid,PDO::PARAM_INT);
					$q->bindParam(":userid",$userid,PDO::PARAM_INT);
					$q->bindParam(":orgname",$inArr['name_of_organization'],PDO::PARAM_STR);
                    $q->bindParam(":position_in_organization",$inArr['position_in_organization'],PDO::PARAM_STR);
                    $q->bindParam(":orgaddress",$inArr['address_of_organization'],PDO::PARAM_STR);
					$q->bindParam(":country",$inArr['country_of_work'],PDO::PARAM_INT);
                    $q->bindParam(":qualifications",$inArr['qualifications'],PDO::PARAM_INT);
                    $q->bindParam(":phone",$inArr['phone_no'],PDO::PARAM_INT);
                    $q->bindParam(":mobile",$inArr['mobile_no'],PDO::PARAM_INT);
                    $q->bindParam(":tarea",$therapeutic_areas,PDO::PARAM_STR);
                    $q->bindParam(":starea",$inArr['sub-therapeutic_area_of_work'],PDO::PARAM_STR);
                    
					$q->bindParam(":greason",$geographic_reasons,PDO::PARAM_STR);
					$q->bindParam(":terms",$inArr['acceptTerms'],PDO::PARAM_STR);
			
				if(!$q->execute()){
					
					$this->closeDB($dbCon);
					return false;
					/*$this->printr(var_dump($q->errorInfo()));
					$this->printr(var_dump($q->errorCode()));
					exit;*/
				}
				
			//echo	
			$sql="UPDATE `custom16_signup_users` SET prefix='".$inArr['prefix']."',name='".$inArr['firstName']."', last_name='".$inArr['familyName']."', alt_email='".$inArr['alt-email']."',profile_status=1 WHERE user_id=".$userid;
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$this->closeDB($dbCon);
			return true;
		
	
	}//END of functions
	
function check_row($tbl,$col,$where){
	
	
	$sql="SELECT * FROM $tbl WHERE $col='".$where."'";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			if($stmt->rowCount()){
				
				return false;
			}else{
				
				return true;
			}

	
}

function getTM($p_id){
    $sql="SELECT tm_id FROM `custom16_trialmanager_projects` WHERE project_id='".$p_id."'";
    $dbCon=$this->connectDB();
    $q = $dbCon->prepare($sql);

    if($tm=$q->execute()){
        $this->closeDB($dbCon);
        return $tm;
    }else{
        return false;
    }
}

function save_project($inArr){
	
		/*$this->printr($inArr);
		exit;*/
		$therapeutic_areas=implode(",",$inArr['therapeutic_area']);
		//$geographic_reasons=implode(",",$inArr['geographic_reasons']);
		$projectid=$this->MaxId("custom16_trialmanager_projects","project_id");
        
        $country_of_project=implode(",",$inArr['country_of_project']);
		
		/*$sql = "INSERT INTO custom16_trialmanagers_details (`det_id`, `user_id`, `name_of_organization`, 
		`address_of_organization`, `country_of_work`, `therapeutic_area`, `geographic_reasons`, `accept_terms`)
		  VALUES(:detid,:userid,:orgname,:orgaddress,:country,:tarea,:greason,:terms)";*/
		  
		$sql="INSERT INTO `custom16_trialmanager_projects` (`project_id`, `tm_id`, `title`, `therapeutic_area`,`summary` ,`regions`, `number_of_sites_needed`, `period`) VALUES (:projectid, :tmid, :title, :tarea, :summary,:regions, :sites, :period)" ;
				
				
				$dbCon=$this->connectDB();
				$q = $dbCon->prepare($sql);
				$userid=$this->getUserid();
				
				
					$q->bindParam(":projectid",$projectid,PDO::PARAM_INT);
					$q->bindParam(":tmid",$userid,PDO::PARAM_INT);
					$q->bindParam(":title",$inArr['project_title'],PDO::PARAM_STR);
					$q->bindParam(":tarea",$therapeutic_areas,PDO::PARAM_STR);
                    $q->bindParam(":summary",$inArr['summary'],PDO::PARAM_STR);
                    $q->bindParam(":regions",$country_of_project,PDO::PARAM_STR);
                    $q->bindParam(":sites",$inArr['number_of_sites'],PDO::PARAM_INT);
					$q->bindParam(":period",$inArr['period'],PDO::PARAM_INT);
		
					if($q->execute()){
    					$this->closeDB($dbCon);
    				    return true;
					/*$this->printr(var_dump($q->errorInfo()));
					$this->printr(var_dump($q->errorCode()));
					exit;*/
    				}else{
    				    return false;
    				}
	}  
	
	
	
	function report_problem($inArr)
    {
		$sql="INSERT INTO `report_problem_new` (`project_id`, `email`,`user_id`, `user_type`,`summary`) VALUES (:projectid, :email,:uid, :utype, :summary)" ;
        $projectid=$this->MaxId("report_problem_new","project_id");
		$userid=$this->getUserid();
		$ab=0;
		$cb=0;
		$dbCon=$this->connectDB();
		$q = $dbCon->prepare($sql);
		$q->bindParam(":projectid",$projectid,PDO::PARAM_INT);
		$q->bindParam(":email",$inArr['email'],PDO::PARAM_STR);
		$q->bindParam(":uid",$inArr['project_title'],PDO::PARAM_STR);
		$q->bindParam(":utype",$inArr['role'],PDO::PARAM_STR);
        $q->bindParam(":summary",$inArr['problem'],PDO::PARAM_STR);

		if (!$q->execute()) {
			$this->closeDB($dbCon);
			return false;
		} else {
            return true;
        }
	}
	
	function invite_colleagues($inArr){
		$projectid=$this->MaxId("invite_colleagues","project_id");
        $sql="INSERT INTO `invite_colleagues` (`project_id`, `email`,`user_id`, `user_type`, `colleagues_email`, `summary`) VALUES (:projectid, :email,:uid, :utype, :cemail, :summary)" ;	
		$userid=$this->getUserid();
		$ab=0;
		$cb=0;
		$dbCon=$this->connectDB();
		$q = $dbCon->prepare($sql);		
		$q->bindParam(":projectid",$projectid,PDO::PARAM_INT);
		$q->bindParam(":email",$inArr['email'],PDO::PARAM_STR);
		$q->bindParam(":uid",$inArr['project_title'],PDO::PARAM_STR);
		$q->bindParam(":utype",$inArr['role'],PDO::PARAM_STR);
		$q->bindParam(":cemail",$inArr['colleagues_email'],PDO::PARAM_STR);
		$q->bindParam(":summary",$inArr['invite'],PDO::PARAM_STR);
		if(!$q->execute()){
			$this->closeDB($dbCon);
			return false;
			// $this->printr(var_dump($q->errorInfo()));
			// $this->printr(var_dump($q->errorCode()));
			// exit;
		} else {
			$this->closeDB($dbCon);
			return true;
		}
	}//END function
	
	function project_list(){
		
			//exit;
			/*$sql="SELECT * FROM `custom16_clinical_projects`";exit;*/
            
            /*$sql="select
            custom16_clinical_projects.*,
            group_concat(therapeutic_areas.name) as therapeutic_name,
            group_concat(countries.country_name) as country
            from
            custom16_clinical_projects
            join therapeutic_areas on find_in_set(therapeutic_areas.id, custom16_clinical_projects.therapeutic_area)
            and join countries on find_in_set(countries.id, custom16_clinical_projects.regions)
            group by
            custom16_clinical_projects.project_id";*/
			
		$sql="SELECT custom16_clinical_projects.*,
		therapeutic_areas.name as therapeutic_name,
		group_concat(countries.country_name) as country
		FROM 
		custom16_clinical_projects
		LEFT JOIN therapeutic_areas
			ON (therapeutic_areas.id=custom16_clinical_projects. therapeutic_area)
		JOIN countries
		ON find_in_set(countries.id ,custom16_clinical_projects. Regions)
		GROUP BY custom16_clinical_projects. project_id
		ORDER BY custom16_clinical_projects. abd DESC";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}
    
    function project_details($project_id){
            //exit;
			/*$sql="SELECT * FROM `custom16_clinical_projects`";exit;*/
            
            /*$sql="select
            custom16_clinical_projects.*,
            group_concat(therapeutic_areas.name) as therapeutic_name,
            group_concat(countries.country_name) as country
            from
            custom16_clinical_projects
            join therapeutic_areas on find_in_set(therapeutic_areas.id, custom16_clinical_projects.therapeutic_area)
            join countries on find_in_set(countries.id, custom16_clinical_projects.regions)
            WHERE project_id = ".$project_id."
            group by
            custom16_clinical_projects.project_id";*/
			
			$sql="SELECT custom16_clinical_projects.*,
			therapeutic_areas.name as therapeutic_name,
			group_concat(countries.country_name) as country
			FROM 
			custom16_clinical_projects
			LEFT JOIN therapeutic_areas
				ON (therapeutic_areas.id=custom16_clinical_projects. therapeutic_area)
			JOIN countries
			ON find_in_set(countries.id ,custom16_clinical_projects. Regions)
			WHERE project_id = ".$project_id."
			GROUP BY custom16_clinical_projects. project_id";
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $res=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				//echo "Not found"; exit;
			}
}
	
	function project_response_tm($project){
		// echo '<pre>'; print_r($project); exit;
		$sql="SELECT pr.id, pr.user_id, pr.project_id, pr.role, pr.number_patients, pr.service_types,pr.promoted,
            custom16_clinical_projects.tm_id, custom16_csd_in_details.in_userid
		FROM project_response pr
        LEFT JOIN custom16_clinical_projects
			ON (custom16_clinical_projects.project_id=pr.project_id)
            
        LEFT JOIN custom16_signup_users
			ON (custom16_signup_users.user_id=pr.user_id)
        
        LEFT JOIN custom16_csd_in_details ON custom16_csd_in_details.in_userid=pr.investigator AND custom16_csd_in_details.deleted = '0'
		WHERE custom16_clinical_projects.tm_id ='".$_SESSION['luser_id']."' AND custom16_clinical_projects.tm_proj_id ='".$project."'
        GROUP BY pr.id";
        // print_r($sql); exit;
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
			  	{
					  $response[]=$row;
			  	}
				//echo '<pre>'; print_r($response);exit;
				 $this->closeDB($dbCon);
				return $response;
				return true;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}
    
    /**
     * Returns list of responses for a project
     * 
     * @param string $projectId [Trial Manager project ID]
     * 
     * @return array
     */
    function listProjectResponse($projectId)
    {
        $sql = "SELECT pr.id, pr.user_id, pr.project_id, pr.role, pr.number_patients, 
                pr.service_types, pr.promoted, cp.tm_id, csd.in_userid 
            FROM project_response pr
            LEFT JOIN custom16_clinical_projects cp ON cp.project_id = pr.project_id 
            LEFT JOIN custom16_signup_users u ON u.user_id = pr.user_id 
            LEFT JOIN custom16_csd_in_details csd ON csd.det_id = pr.investigator AND csd.deleted = '0'
            WHERE cp.tm_id = '".$_SESSION['luser_id']."' AND cp.tm_proj_id = '".$projectId."' 
            GROUP BY pr.id";
        
        return $this->executeQuery($sql, false);
	}
	function response_csc($userid){
        $qery_area = "SELECT a.user_id, country_of_work, ques1, ques2, qualifications, other_qual, therpeutic_area_of_work, sub_therapeutic_area_of_work, start_working, medica_practice_exp, number_of_trials, 
            ta.name, country_name, a.publications, a.affliations, a.Accreditations, a.Patents, 
            u.prefix, u.name, u.last_name, u.profile_pic 
        	FROM custom16_csd_details a
        	LEFT JOIN therapeutic_areas ta ON (ta.id=a.therpeutic_area_of_work) 
            LEFT JOIN custom16_signup_users u ON a.user_id = u.user_id 
        	JOIN countries ON find_in_set(countries.id ,a. country_of_work)
        	WHERE a.user_id=".$userid;

        $dbCon=$this->connectDB();
        $stmt = $dbCon->prepare($qery_area);
        $stmt->execute();
        if($row=$stmt->rowCount()){

            //echo "found";exit;
            while ($row = $stmt->fetch())
            {
                //echo $row['0'];exit;
                $res[]=$row;

            }
            //echo "found"; exit;

            return $res;

        }else{
            return false;

            //echo "Not found"; exit;
        }
    }
	function response_profile($userid){
    	$qery_area = "SELECT a.user_id, country_of_work, ques1, ques2, qualifications, other_qual, therpeutic_area_of_work, sub_therapeutic_area_of_work, start_working, medica_practice_exp, number_of_trials, 
            ta.name, country_name, a.publications, a.affliations, a.Accreditations, a.Patents, 
            u.prefix, u.name, u.last_name, u.profile_pic 
        	FROM custom16_investigator_details a
        	LEFT JOIN therapeutic_areas ta ON (ta.id=a.therpeutic_area_of_work) 
            LEFT JOIN custom16_signup_users u ON a.user_id = u.user_id 
        	JOIN countries ON find_in_set(countries.id ,a. country_of_work)
        	WHERE a.user_id=".$userid;
	
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($qery_area);
			$stmt->execute();
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
			  //echo $row['0'];exit;
					  $res[]=$row;
					  
			  	}
				 //echo "found"; exit;
					
					return $res;
			
			}else{
				return false;
				
				//echo "Not found"; exit;
			}
    }//END FUNCTION

    //Create user contacts relationship
    function createUserContact($userId, $contactId)
    {
        $sql = "INSERT INTO users_contacts (`id`, `user_id`, `contact_id`, `date_created`, `date_modified`, `deleted`) 
            VALUES (:id, :userId, :contactId, :dateCreated, :dateModified, :deleted)";
        
        $dbCon        = $this->connectDB();
        $q            = $dbCon->prepare($sql);
        $id           = $this->getRecordId();
        // $userId       = $_SESSION['luser_id'];
        // $contactId    = 0;
        $dateCreated  = gmdate('Y-m-d H:i:s');
        $dateModified = gmdate('Y-m-d H:i:s');
        $deleted      = 0;

        $q->bindParam(":id", $id, PDO::PARAM_STR);
        $q->bindParam(":userId", $userId, PDO::PARAM_STR);
        $q->bindParam(":contactId", $contactId, PDO::PARAM_STR);
        $q->bindParam(":dateCreated", $dateCreated);
        $q->bindParam(":dateModified", $dateModified);
        $q->bindParam(":deleted", $deleted);
        
        if ($q->execute()) {
            $this->closeDB($dbCon);
            return $id;
        } else {
            return '';
        }
    }

    function isUserContact($userId, $contactId,$cord_id=false)
    {
        $sql = "SELECT uc.* 
            FROM users_contacts uc 
            WHERE uc.user_id = '".$userId."' AND uc.deleted='0' AND (uc.contact_id = '".$contactId."'";
        if($cord_id){
            $sql.=" OR uc.contact_id='".$cord_id."')";
        }else{
            $sql.=")";
        }
        return $this->executeQuery($sql, false);
    }
	
function get_therapeutic_details($val){
	
	$val=explode(",",$val);
	$res=array();
	//$this->printr($val);
	$dbCon=$this->connectDB();
		foreach($val as $key){
			
			$sql="SELECT name FROM `therapeutic_areas` WHERE id=".$key;
			
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$row = $stmt->fetch();
			$res[]=$row[0];
			
		}
		$res=implode(",",$res);
		$this->closeDB($dbCon);
		echo $res;
		//$this->printr($res);
	
}
function get_regions($val){
	
	$val=explode(",",$val);
	$res=array();
	//$this->printr($val);
	$dbCon=$this->connectDB();
		foreach($val as $key){
			
			$sql="SELECT name FROM `geographic_reasons` WHERE id=".$key;
			
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			$row = $stmt->fetch();
			$res[]=$row[0];
			
		}
		$res=implode(",",$res);
		$this->closeDB($dbCon);
		echo $res;
		//$this->printr($res);
	
}
function participated_project(){
		$sql="SELECT project_response.*, custom16_clinical_projects.*,
		project_response.id,project_response.user_id,project_response.project_id,project_response.number_patients,project_response.investigator,project_response.service_types
		FROM project_response 
			JOIN custom16_clinical_projects
			ON custom16_clinical_projects.project_id = project_response. project_id
		WHERE user_id =".$_SESSION['luser_id'];
		
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $pro[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $pro;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}

	function posted_project(){
		$sql="SELECT custom16_trialmanager_projects.*,
		therapeutic_areas.name as therapeutic_name,
		group_concat(countries.country_name) as country
		FROM 
		custom16_trialmanager_projects
		LEFT JOIN therapeutic_areas
			ON (therapeutic_areas.id=custom16_trialmanager_projects. therapeutic_area)
		JOIN countries
		ON find_in_set(countries.id ,custom16_trialmanager_projects. Regions)
		WHERE custom16_trialmanager_projects.tm_id =".$_SESSION['luser_id']." 
		GROUP BY custom16_trialmanager_projects. project_id";
		// print_r($sql);exit;
		
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $pro[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $pro;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}

	function posted_project_detail($project){
		$sql="SELECT custom16_trialmanager_projects.*,
		therapeutic_areas.name as therapeutic_name,
		group_concat(countries.country_name) as country
		FROM 
		custom16_trialmanager_projects
		LEFT JOIN therapeutic_areas
			ON (therapeutic_areas.id=custom16_trialmanager_projects. therapeutic_area)
		JOIN countries
		ON find_in_set(countries.id ,custom16_trialmanager_projects. Regions)
		WHERE custom16_trialmanager_projects.tm_id =".$_SESSION['luser_id']." AND custom16_trialmanager_projects.project_id =".$project."
		GROUP BY custom16_trialmanager_projects. project_id";
		// print_r($sql);exit;
		
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $pro[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $pro;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}
	
	function modified_project($id){
		$sql="SELECT modify_trialmanager_projects.*,
		therapeutic_areas.name as therapeutic_name,
		group_concat(countries.country_name) as country
		FROM 
		modify_trialmanager_projects
		LEFT JOIN therapeutic_areas
			ON (therapeutic_areas.id=modify_trialmanager_projects. therapeutic_area)
		JOIN countries
		ON find_in_set(countries.id ,modify_trialmanager_projects.regions)
		WHERE modify_trialmanager_projects.tm_id =".$_SESSION['luser_id']." AND modify_trialmanager_projects. project_id = ".$id."
		GROUP BY modify_trialmanager_projects. mod_id";
		//print_r($sql);exit;
		
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $mod[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $mod;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}

	function modify_action($inArr){
	
		// $this->printr($inArr);
		// exit;
		$sql="UPDATE `modify_trialmanager_projects` SET action_taken=1 WHERE project_id=".$inArr;
		// print_r($sql);
					$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			
					if($stmt->execute()){
    					$this->closeDB($dbCon);
						// echo "done"; exit;
    				    return true;
					
    				}else{
    				    return false;
						// $this->printr(var_dump($q->errorInfo()));
					// $this->printr(var_dump($q->errorCode()));
					// exit;
    				}
	}
	
	function approved_project($id){
		$sql="SELECT custom16_clinical_projects.*,
		therapeutic_areas.name as therapeutic_name,
		group_concat(countries.country_name) as country
		FROM 
		custom16_clinical_projects
		LEFT JOIN therapeutic_areas
			ON (therapeutic_areas.id=custom16_clinical_projects. therapeutic_area)
		JOIN countries
		ON find_in_set(countries.id ,custom16_clinical_projects. Regions)
		WHERE custom16_clinical_projects.tm_id =".$_SESSION['luser_id']." AND custom16_clinical_projects.tm_proj_id =".$id."
		GROUP BY custom16_clinical_projects. project_id";
		//print_r($sql);exit;
		
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				while ($row = $stmt->fetch())
			  	{
					  $posted[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $posted;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
		
		
		
	}
	
    function change_csd_status($inarray)
    {
        $sql = "UPDATE `custom16_csd_in_details` SET in_confirm='".($inarray['status_change'])."', investigator_name='".($inarray['in_name'])."', in_userid='".($_SESSION['luser_id'])."' WHERE user_id='".$inarray['csd']."' AND investigator_email='".$inarray['inemail']."' AND deleted='0'";
        $dbCon = $this->connectDB();
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        if ($row = $stmt->rowCount()) {
            while ($row = $stmt->fetch())
            {
                 $res = $row;
            }
            $this->closeDB($dbCon);
            return true;
        } else {
            $this->closeDB($dbCon);
            return false;
        }       
    }

	function change_csd_status_DEPRECATED($inarray){
        $sql = "UPDATE `custom16_csd_in_details` SET in_confirm='".($_POST['status_change'])."', investigator_name='".($_POST['in_name'])."', in_userid='".($_SESSION['luser_id'])."' WHERE user_id='".$_POST['csd']."' AND investigator_email='".$_POST['inemail']."'"; 
    	$dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();

		if($row=$stmt->rowCount()){
			
			while ($row = $stmt->fetch())
		  	{
				  $res=$row;
		  	}
			 $this->closeDB($dbCon);
			return true;
		}else{
			$this->closeDB($dbCon);
			return false;
		}		
}
    
function add_csd($inArr){

    if($inArr['csdmail']){
        $sql="SELECT * FROM `custom16_csd_in_details` WHERE coordinator_email = '".$inArr['csdmail']."' AND investigator_email = '".$_SESSION['lemail']."' AND deleted = '0'";
        $Con=$this->connectDB();
        $q = $Con->prepare($sql);
        if($q->execute() && $upd = $q->fetch()){
            $sql1 = "UPDATE `custom16_csd_in_details` SET coordinator_surname = '".$inArr['csname']."',coordinator_name = '".$inArr['cname']."',in_userid = '".$_SESSION['luser_id']."' , in_confirm = '1' WHERE coordinator_email = '".$inArr['csdmail']."' AND investigator_email = '".$_SESSION['lemail']."' WHERE deleted='0'";
            $Con=$this->connectDB();
            $q = $Con->prepare($sql1);
            if($q->execute()){
                $this->closeDB($Con);
                return true;
            }else{
                $this->closeDB($Con);
                return false;
            }
        }
        else{
            $sql="INSERT INTO `custom16_csd_in_details` (`det_id`, `user_id`, `coordinator_name`, `coordinator_surname`, `coordinator_email`, `in_userid`, `investigator_name`, `investigator_email`, `in_confirm`) VALUES (:detid, :userid, :cname, :csname, :cemail, :inid, :inname,:inemail, :inconf);";
            //print_r($sql);exit;

            $detid=$this->MaxId("custom16_csd_in_details","det_id");

            $userid=0;
            $ab=0;
            $cb=0;
            $conf=1;
            $dbCon=$this->connectDB();
            $q = $dbCon->prepare($sql);

            $q->bindParam(":detid",$detid,PDO::PARAM_INT);
            $q->bindParam(":userid",$userid,PDO::PARAM_INT);
            $q->bindParam(":cname",$inArr['cname'],PDO::PARAM_STR);
            $q->bindParam(":csname",$inArr['csname'],PDO::PARAM_STR);
            $q->bindParam(":cemail",$inArr['csdmail'],PDO::PARAM_STR);
            $q->bindParam(":inid",$_SESSION['luser_id'],PDO::PARAM_STR);
            $q->bindParam(":inname",$inArr['inv'],PDO::PARAM_STR);
            $q->bindParam(":inemail",$_SESSION['lemail'],PDO::PARAM_STR);
            $q->bindParam(":inconf",$conf,PDO::PARAM_INT);

            if(!$q->execute()){

                $this->closeDB($dbCon);
                return false;
                /*$this->printr(var_dump($q->errorInfo()));
                $this->printr(var_dump($q->errorCode()));
                exit;*/
            }
            $this->closeDB($dbCon);
            return true;
        }
    }
}

    function add_in($inArr)
    {
        $sql = "UPDATE `custom16_csd_in_details` SET user_id='".($_SESSION['luser_id'])."' 
        WHERE investigator_email ='".$_POST['iemail']."' AND coordinator_email='".$_SESSION['lemail']."'";
    	
    	$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		
		if ($row=$stmt->rowCount()) {
			while ($row = $stmt->fetch())
		  	{
				$res = $row;
		  	}
			$this->closeDB($dbCon);
			return true;
		} else {
			$this->closeDB($dbCon);
			return false;
		}		
    }
	
    function project_response_status($proj_id=false){
		$sql = "SELECT project_response.id,project_response.user_id,project_response.project_id,project_response.number_patients,project_response.investigator,project_response.service_types,project_response.promoted
		  FROM project_response WHERE (investigator ='".$_SESSION['luser_id']."' OR user_id ='".$_SESSION['luser_id']."') ";
            if($proj_id){
                $sql.="AND project_id='".$proj_id."'";
            }
            $dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				while ($row = $stmt->fetch())
			  	{
					  $res[]=$row;
			  	}
				 $this->closeDB($dbCon);
				return $res;
			}else{
				$this->closeDB($dbCon);
				return false;
				
				//echo "Not found"; exit;
			}
	}

    function checkProjectResponseStatus($userId, $userRole, $projectId)
    {
        if ($userRole = 'clinical_stud_coordinator') {
            $subSql = " AND investigator ='".$userId."' ";
        } elseif ($userRole = 'service_provider') {
            $subSql = "";
        } else {
            $subSql = "";
        }
        $sql = "SELECT project_response.id,project_response.user_id,project_response.project_id,project_response.number_patients,project_response.investigator,project_response.service_types
          FROM project_response WHERE user_id ='".$_SESSION['luser_id']."' AND project_id='".$projectId."' ".$subSql." ORDER BY date_created ASC";
        return $this->executeQuery($sql, false);
    }

    function updateRespond($id){
        $sql = "UPDATE `project_response` SET promoted = '1' WHERE id = '".$id."'";
        $dbCon = $this->connectDB();
        $stmt = $dbCon->prepare($sql);
        if($stmt->execute()){
            $this->closeDB($dbCon);
            return true;
        }else{
            $this->closeDB($dbCon);
            return false;
        }
    }

function extension($val){
	
	//$ext=array();
	$ext=array("jpg","jpeg","doc","txt","docx","png","pdf");
	
	if(!in_array($val,$ext)){
		return false;
		
	}
	return true;
}


function size($val){
    $maxsize=5242880;
 	if($val > $maxsize){
		return false;
		
	}else{
	    return true;
	}
	
	
}

	function FeasibilityRequested($InvestigatorUserID, $ProjectID){
		//project_response, trialmanager_projects
		$sql = "SELECT `tm_proj_id`, `investigator_id` FROM share WHERE tm_proj_id='".$ProjectID."' AND investigator_id = '".$InvestigatorUserID."'";
		$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		
		$row = $stmt->fetch();
		
		if($row[0] != ''){
			$this->closeDB($dbCon);
			return true;
		}else{
			$this->closeDB($dbCon);
			return false;
		}
	}

	function SubmitFeasibilityRequest($ShareVal,$CscID=false){
		//project_response, trialmanager_projects
		$ShareID = $this->MaxId("share","share_id");
		if($CscID){
            $sql = "INSERT INTO `share` (`share_id`, `tm_proj_id`, `investigator_id`, `tm_accepted_terms`, `create_date`, `csc_id`) VALUES (:ShareID, :ProjectID, :InvestigatorUserID, :nda, :CreateDate, :CSCID)";
            $dbCon = $this->connectDB();
            $stmt = $dbCon->prepare($sql);
            if($stmt->execute(array(':ShareID'=>$ShareID, ':ProjectID'=>$ShareVal['ProjectID'], ':InvestigatorUserID'=>$ShareVal['InvestigatorUserID'], ':nda'=>$ShareVal['nda'], ':CreateDate'=>$ShareVal['CreateDate'], ':CSCID'=>$CscID))){
                $this->closeDB($dbCon);
                return true;
            }else{
                $this->closeDB($dbCon);
                return false;
            }
        }else{
            $sql = "INSERT INTO `share` (`share_id`, `tm_proj_id`, `investigator_id`, `tm_accepted_terms`, `create_date`) VALUES (:ShareID, :ProjectID, :InvestigatorUserID, :nda, :CreateDate)";
            $dbCon = $this->connectDB();
            $stmt = $dbCon->prepare($sql);
            if($stmt->execute(array(':ShareID'=>$ShareID, ':ProjectID'=>$ShareVal['ProjectID'], ':InvestigatorUserID'=>$ShareVal['InvestigatorUserID'], ':nda'=>$ShareVal['nda'], ':CreateDate'=>$ShareVal['CreateDate']))){
                $this->closeDB($dbCon);
                return true;
            }else{
                $this->closeDB($dbCon);
                return false;
            }
        }

	}

    /**
     * Nofify to Investigator after Trial Manager requests for feasibility
     * 
     * @param array $investigatorDetail [Details of investigator]
     * @param array $ProjectDetail [Details of project]
     * @param integer $ResponseID [Sequence number of the response]
     * 
     * @return boolean
     */
    function NofifyFeasibilityRequest($investigatorDetail, $ProjectDetail, $ResponseID)
    {
        $investigatorName = $investigatorDetail['prefix']." ".$investigatorDetail['last_name'];
        $to = trim($investigatorDetail['email']);
        $subject = 'Credevo: Request for feasibility received';
        $link = SITE_URL."myzone/list-requested-feasibilities";
        
        $body_html = $this->LoadEmailTemplate('mail_notification_request_for_feasibility.html');
        $body_html = str_replace('%investigatorName%', $investigatorName, $body_html);
        $body_html = str_replace('%link%', $link, $body_html);
		$body_html = str_replace('%title%', $ProjectDetail['title'], $body_html);
		$body_html = str_replace('%project_id%', $ProjectDetail['project_id'], $body_html);
		$body_html = str_replace('%email%', $to, $body_html);
        
        $body = "Dear ".$investigatorName."! \r\n\r\nYour preliminary feedback for this project ('".$ProjectDetail['title']."') was received well by the trial manager. Now, they've requested for your detailed feasibility feedback.\r\n\r\nPlease click here ('".$link."') to review the synopsis and respond to the feasibility questionnaire.\r\n\r\nKind Regards,\r\n\r\nGracey C.";

        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Gracey C. <gracey.c@credevo.com>',
            //'from'    => 'Credevo <confirmations@clinicalstudynetwork.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            'text'    => $body,
            'html'    => $body_html
        ));

        $id = $result->http_response_body->id;
        if ($id) {
			return true;
		}else{
			return false;
		}
    }

    /**
     * Get list of requests of feasibilities by user
     * 
     * @param string $this->UserId [description]
     */
    function listRequestedFeasibilities($csc=false) {
    	$sql = "SELECT 
                    tmp.project_id, tmp.title AS project_title, tmp.therapeutic_area AS project_therapeutic_area, tmp.summary, tmp.regions AS project_region, tmp.number_of_sites_needed, tmp.period, tmp.abd , tmp.tm_id
                FROM share s 
                LEFT JOIN custom16_trialmanager_projects tmp ON tmp.project_id = s.tm_proj_id 
                LEFT JOIN custom16_trialmanagers_details tmd ON tmd.user_id = tmp.tm_id AND tmd.deleted = '0' ";
    	if($csc && $csc !== ''){
    	    $sql.="WHERE s.csc_id='".$csc."'";
        }else{
            $sql.="WHERE s.investigator_id='".$this->UserId."'";
        }

		return $this->executeQuery($sql, false);
    }
    
    function getAcceptedTerms($projectId, $UserId, $role) {
        $sql = "SELECT 
                    s.tm_accepted_terms, s.invitee_accepted_terms 
                FROM share s 
                LEFT JOIN custom16_signup_users u ON u.user_id = s.investigator_id 
                WHERE (s.investigator_id='".$UserId."' OR s.csc_id='".$UserId."' )AND s.tm_proj_id = '".$projectId."'";
        return $this->executeQuery($sql);
    }
    
    function submitAcceptedTerms($projectId, $inviteeId, $terms) {
        $termToField = 'invitee_accepted_terms';
        if ($_SESSION['lrole'] == 'trial_manager') {
            $termToField = 'tm_accepted_terms';
        }
        
        $sql = "UPDATE `share` SET ".$termToField." = '".$terms."' WHERE tm_proj_id = '".$projectId."' AND (investigator_id = '".$inviteeId."' OR csc_id='".$inviteeId."')";
	    $dbCon = $this->connectDB();
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();
        $this->closeDB($dbCon);
        return true;
    }
    
    /**
     * Uploads profile picture of an user
     * 
     * @param string $this->UserId [description]
     * 
     * @return boolean
     */
    function updateProfilePicture()
    {
        $sql = "UPDATE custom16_signup_users SET profile_pic=? WHERE user_id=?";
		
        $sqlFields   = array();
        $sqlFields[] = $this->profilePic;
        $sqlFields[] = $this->UserId;
        
        return $this->executeQuery($sql, true, $sqlFields);
    }

    /**
     * Updates information of a table
     * 
     * @param string $table Name of the table
     * @param array $fieldsArr Array of field name in the table
     * @param array $valArr Array of the values to update, should match with $fieldsArr field sequence
     * @param string $primaryKeyField Name of the primary key field
     * @param string $primaryKeyVal Value of the primary field
     * 
     * @return boolean
     */
    function updateInfo($table, $fieldsArr, $valArr, $primaryKeyField, $primaryKeyVal)
    {
        $subSql = '';

        foreach ($fieldsArr as $fieldKey => $fieldName) {
            if ((count($fieldsArr) - 1) == $fieldKey) {
                $subSql .= $fieldName.'=? ';
            } else {
                $subSql .= $fieldName.'=?, ';
            }
        }

        $sql = "UPDATE ".$table." SET ".$subSql." WHERE ".$primaryKeyField."=?";
        
        $sqlFields   = $valArr;
        $sqlFields[] = $primaryKeyVal;
        
        return $this->executeQuery($sql, true, $sqlFields);
    }

    /**
     * Send invitation to colleagues that user wants
     * @return boolean
     */
    function sendInvitation($contents,$FromName)
    {
        $emails=trim($contents['colleagues_email']);
        $allTo = explode (",",$emails);
        $body = $contents['invite'];
		$body .= "\r\n \r\n Note: This message was sent by ";
		$body .= $contents['email'];
		$body .= " using Credevo.com";
		$altbody = "Hi, your colleague has invited you to Credevo. Here is the message that they sent. \r\n \"";
		$altbody .=$contents['invite'];
		$altbody .="\"";
		$cc = $contents['email'];
        $subject = "Did you try Credevo?";
        
        foreach ($allTo as $key => $to) {
	        # Make the call to the client.
	        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
	            'from'    => $FromName.' <confirmations@credevo.com>',
	            //'from'    => 'Credevo <confirmations@clinicalstudynetwork.com>',
	            'to'      => $to,
	            'cc'      => $cc,
	            'bcc'     => MAILER_BCC,
	            'subject' => $subject,
	            'text'    => $body
	        ));
	    }

        $id = $result->http_response_body->id;
        if ($id) {
			return true;
		}else{
			return false;
		}
    }
	/**
     * Send invitation to colleagues that user wants
     * parameter $projectId = tmProjectId after function save_project
     * @return boolean
     */
    function sendPostingMail($projectId,$title,$tmname)
    {
        $to = $_SESSION['lemail'];
        $subject = 'Credevo: Project has been submitted';
        $link = SITE_URL."myzone/view_project?project=".$projectId;

        $body_html = $this->LoadEmailTemplate('mail_postProject.html');
        $body_html = str_replace('%title%', $title, $body_html);
		$body_html = str_replace('%tmName%', $tmname, $body_html);
		$body_html = str_replace('%link%', $link, $body_html);
		$body_html = str_replace('%tmProjectId%', $projectId, $body_html);
		$body_html = str_replace('%email%', $to, $body_html);
        
        $body = "Dear ".$tmname."! \r\n\r\nYour project ('".$link."') was successfully submitted for review. You'll receive notification within 48 hours when project will be approved or any modifications will be required. Please do not hesitate to contact me, if you don't receive any notification within this time.\r\nKind Regards,\r\nGracey C.";

        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Gracey C. <gracey.c@credevo.com>',
            //'from'    => 'Credevo <confirmations@clinicalstudynetwork.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            'text'    => $body,
            'html'    => $body_html
        ));

        $id = $result->http_response_body->id;
        if ($id) {
			return true;
		}else{
			return false;
		}
    }

    /**
     * Send notification to TM after a response is submitted for the project
     * @return boolean
     */
    function notifyResponse($projectId)
    {
        // $to = TM email;
        $project = $this->project_details($projectId);
        $tmid = $project['tm_id'];
        $profile = $this->profile_details($tmid,'trial_manager');
        $p = $project['tm_proj_id'];
        $t               = time(); //echo '<pre>';print_r($countAns);echo '</pre>';
        $projectID       = $p*267;
        $ti              = $t.$projectID;
        $projectid            = base64_encode($ti);
        $link = SITE_URL."myzone/my_project?p=".$projectid;
        $to = $profile[0]['email'];
        $subject = 'Credevo: Your project has got a response';        
        $tmname = $profile[0]['name'];

        $body_html = $this->LoadEmailTemplate('projectRespNotific.html');
        $body_html = str_replace('%title%', $project['title'], $body_html);
		$body_html = str_replace('%tmName%', $tmname, $body_html);
		$body_html = str_replace('%link%', $link, $body_html);
		$body_html = str_replace('%project_id%', $projectId, $body_html);
		$body_html = str_replace('%email%', $to, $body_html);
        
        $body = "Dear ".$tmname."! \r\n\r\nAn investigator / site has expressed interest and submitted a response to your project ('".$link."') on Credevo.\r\nKind Regards,\r\nGracey C.";

        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Gracey C. <gracey.c@credevo.com>',
            //'from'    => 'Credevo <confirmations@clinicalstudynetwork.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            'text'    => $body,
            'html'    => $body_html
        ));

        $id = $result->http_response_body->id;
        if ($id) {
			return true;
		}else{
			return false;
		}
    }

    /**
     * Common Function:: Send email notification to anyone
     *
     * @param string $templateFileName  Name of the email template file with extension
     * @param string $mailSubject       Email Subject as a plain text
     * @param array $mailKeyToNameArray Array of keys => value to parse in email template
     * @param array $recipients         Array of recipients
     * @param array $from               From email address
     *
     * @return boolean
     */
    function notifyByEmail($templateFileName, $mailSubject, $mailKeyToNameArray, $recipients, $from)
    {
        $body_html = $this->LoadEmailTemplate($templateFileName);
        $body_html = $this->parseEmailTemplate($body_html, $mailKeyToNameArray);
        $body      = strip_tags($body_html);
        $from      = !empty($from) ? $from : 'Gracey C. <gracey.c@credevo.com>';

        // Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => $from,
            'to'      => $recipients[0],
            'bcc'     => MAILER_BCC,
            'subject' => $mailSubject,
            'text'    => $body,
            'html'    => $body_html
        ));

        $id = $result->http_response_body->id;

        if ($id) {
            return true;
        } else {
            return false;
        }
    }

}//END OF CLASS

?>
