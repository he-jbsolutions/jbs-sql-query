-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2018 at 01:20 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sqltest`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(7) NOT NULL,
  `continent_id` bigint(7) NOT NULL,
  `country_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `continent_id`, `country_name`) VALUES
(60, 3, 'India'),
(61, 3, 'Indonesia'),
(62, 3, 'Japan'),
(63, 3, 'Kazakhstan'),
(64, 3, 'Korea (north)'),
(65, 3, 'Korea (south)'),
(66, 3, 'Laos'),
(67, 3, 'Malaysia'),
(68, 3, 'Maldives'),
(69, 3, 'Mongolia'),
(70, 3, 'Nepal'),
(71, 3, 'Philippines');

-- --------------------------------------------------------

--
-- Table structure for table `csc_details`
--

CREATE TABLE `csc_details` (
  `det_id` bigint(7) NOT NULL,
  `user_id` bigint(7) NOT NULL,
  `phone_no` varchar(20) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `country_of_work` int(3) NOT NULL,
  `qualifications` varchar(255) NOT NULL,
  `ques1` varchar(5) NOT NULL DEFAULT 'no' COMMENT 'Are you associated with a site management organization (SMO)',
  `smo_name` text,
  `smo_details` mediumtext,
  `therpeutic_area_of_work` varchar(255) DEFAULT NULL,
  `sub_therapeutic_area_of_work` mediumtext NOT NULL,
  `start_working` date NOT NULL,
  `no_of_years_of_clinical_trails` int(3) NOT NULL,
  `number_of_trials` int(3) NOT NULL,
  `publications` longtext,
  `affliations` longtext,
  `accreditations` longtext,
  `patents` longtext,
  `accept_terms` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not accepted,1-accepted',
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Not deleted 1: Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `csc_details`
--

INSERT INTO `csc_details` (`det_id`, `user_id`, `phone_no`, `mobile_no`, `country_of_work`, `qualifications`, `ques1`, `smo_name`, `smo_details`, `therpeutic_area_of_work`, `sub_therapeutic_area_of_work`, `start_working`, `no_of_years_of_clinical_trails`, `number_of_trials`, `publications`, `affliations`, `accreditations`, `patents`, `accept_terms`, `added_date`, `updated_date`, `deleted`) VALUES
(2, 168, '1000000000', '1000000000', 71, 'fdsfsda', 'yes', 'fdsf', 'fdsf', '7,21,66', 'adadad', '2010-03-03', 2, 2, '1', '', '32', '4', 0, '2015-12-19 05:03:32', NULL, 0),
(8, 169, '12', '12', 60, 'ABC, PhD, M.B.A., Dr.,', 'yes', 'Org name', 'dsafds', '21', 'gfhgdfgf', '2015-12-03', 12, 12, 'pub', 'aff', 'acc', 'pat', 1, '2015-12-30 21:20:14', NULL, 0),
(55, 166, '', '', 67, 'bvcxvb', 'yes', '  cvv', ' vcbb', '21,7', '', '2018-01-05', 17, 500, '', '', '', '', 0, '2018-01-10 21:46:59', NULL, 0),
(56, 167, '', '', 67, 'bvcxb', 'yes', '', '', '7,66', '', '1970-01-01', 0, 0, '', '', '', '', 0, '2018-01-10 23:50:26', NULL, 0),
(57, 170, '', '', 67, 'fdgfd', 'no', '', '', '21,67', '', '1970-01-01', 0, 0, '', '', '', '', 0, '2018-01-18 21:37:13', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `csc_in_relation`
--

CREATE TABLE `csc_in_relation` (
  `det_id` bigint(7) NOT NULL,
  `csc_user_id` bigint(7) NOT NULL,
  `csc_name` varchar(255) NOT NULL,
  `csc_surname` varchar(255) NOT NULL,
  `csc_email` varchar(255) NOT NULL,
  `investigator_userid` bigint(7) NOT NULL,
  `investigator_name` text NOT NULL,
  `investigator_email` text NOT NULL,
  `in_confirm` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-investigator not confirmed yet,1-investigator confirmed',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: not deleted 1: deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='csc investigator relationship';

--
-- Dumping data for table `csc_in_relation`
--

INSERT INTO `csc_in_relation` (`det_id`, `csc_user_id`, `csc_name`, `csc_surname`, `csc_email`, `investigator_userid`, `investigator_name`, `investigator_email`, `in_confirm`, `deleted`) VALUES
(120, 170, 'fggfdg', 'gfdgf', 'csc1@free.com', 0, 'fdsfdg', 'test2Email3@test.com', 0, 0),
(128, 166, 'vbcxbv', 'bcvxb', 'testCSC1@free.com', 0, 'vcxbv', 'cxvv@fsdfads.com', 0, 0),
(129, 166, 'vbcxbv', 'bcvxb', 'testCSC1@free.com', 0, 'safadf', 'testfea12@inv.com', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `email_subscription`
--

CREATE TABLE `email_subscription` (
  `sid` bigint(7) NOT NULL COMMENT 'subscription id',
  `email` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email_status` tinyint(1) NOT NULL COMMENT '0-email not verified,1-Email verified',
  `unsubscribe` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Subscribed 1: Unsubscribed',
  `unsub_date` date DEFAULT NULL,
  `gift1` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: gift not claimed 1: gift claimed',
  `feedback1` text COMMENT 'Would you pay for premium services'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_subscription`
--

INSERT INTO `email_subscription` (`sid`, `email`, `date`, `email_status`, `unsubscribe`, `unsub_date`, `gift1`, `feedback1`) VALUES
(1, 'test2Email52@test.com', '2015-11-27 03:10:45', 1, 0, '2016-05-12', 1, 'y'),
(2, 'test2Email3@test.com', '2015-11-28 01:10:32', 1, 0, '0000-00-00', 0, ''),
(3, 'test2@test.com', '2015-11-30 07:30:41', 1, 0, '0000-00-00', 0, ''),
(4, 'test1@test.com', '2016-01-11 20:43:32', 1, 0, '0000-00-00', 0, ''),
(5, 'K2@112.com', '2016-01-11 20:45:45', 1, 0, '0000-00-00', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `investigator_details`
--

CREATE TABLE `investigator_details` (
  `det_id` bigint(7) NOT NULL,
  `user_id` bigint(7) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `mobile_no` varchar(20) DEFAULT NULL,
  `country_of_work` int(3) NOT NULL DEFAULT '0',
  `ques1` varchar(20) NOT NULL DEFAULT 'no' COMMENT 'Available and willing to participate in new clinical trial opportunities? 0-No,1-Yes',
  `ques2` varchar(20) NOT NULL DEFAULT 'no' COMMENT 'Available and willing to participate in new consulting opportunities.0-No,1-yes',
  `qualifications` varchar(255) NOT NULL,
  `other_qual` varchar(255) NOT NULL,
  `therpeutic_area_of_work` varchar(255) DEFAULT NULL,
  `sub_therapeutic_area_of_work` mediumtext,
  `start_working` date NOT NULL,
  `no_of_years_of_clinical_trails` smallint(3) NOT NULL DEFAULT '0' COMMENT 'number of years that investigator worked on clinical trials',
  `medica_practice_exp` smallint(3) NOT NULL DEFAULT '0' COMMENT 'medical practice experience (In years)',
  `number_of_trials` int(3) NOT NULL DEFAULT '0' COMMENT 'number of trials conducted by investigator',
  `publications` longtext,
  `affliations` longtext,
  `Accreditations` longtext,
  `Patents` longtext,
  `accept_terms` tinyint(1) DEFAULT '0' COMMENT '0-accepted,1-Not accepted',
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: not deleted 1: deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `investigator_details`
--

INSERT INTO `investigator_details` (`det_id`, `user_id`, `phone_no`, `mobile_no`, `country_of_work`, `ques1`, `ques2`, `qualifications`, `other_qual`, `therpeutic_area_of_work`, `sub_therapeutic_area_of_work`, `start_working`, `no_of_years_of_clinical_trails`, `medica_practice_exp`, `number_of_trials`, `publications`, `affliations`, `Accreditations`, `Patents`, `accept_terms`, `added_date`, `updated_date`, `deleted`) VALUES
(1, 156, '', '', 67, 'no', 'yes', 'MBBS', '1', '21', 'rwsasfd', '2015-12-22', 3, 3, 3, '', '', '', '', 0, '2015-12-21 03:20:15', NULL, 0),
(3, 157, '1000000000', '1000000000', 60, 'yes', 'yes', 'MD,MS,MBBS', 'FRCP', '21,7', 'fsdfdsf', '2010-02-14', 12, 12, 1, 'pub', 'aff', 'acc', 'pat', 1, '2015-12-30 20:47:52', '2016-06-13 03:31:54', 0),
(11, 158, '', '668989', 71, 'yes', 'yes', 'PhD', 'PMP', '7,66', 'Breast cancer, head and neck cancer', '2000-03-02', 2, 2, 5, 'p', 'af', 'ac', 'pa', 1, '2016-01-22 22:35:59', NULL, 0),
(15, 159, '1000000000', '1000000000', 60, 'No', 'No', 'MBBS,PhD,MD', 'Other1,Other2', '66,7,21', 'otherther2', '0000-00-00', 2, 8, 4, 'pub', 'aff', 'acc', 'pat', 1, '0000-00-00 00:00:00', NULL, 0),
(16, 166, '1000000000', '1000000000', 67, 'Yes', 'Yes', 'MD,FRCP', 'Other3', '21,66', 'otherther3', '0000-00-00', 3, 12, 6, 'pub', 'aff', 'acc', 'pat', 1, '0000-00-00 00:00:00', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invited`
--

CREATE TABLE `invited` (
  `sid` int(10) NOT NULL,
  `mail_batch` varchar(25) NOT NULL COMMENT ' "batch NO."/"first 3 letters of email""first two letters of last name"',
  `therArea` varchar(100) NOT NULL,
  `lastName` text NOT NULL,
  `firstName` text NOT NULL,
  `middleName` text NOT NULL,
  `suffix` text NOT NULL,
  `company` varchar(55) NOT NULL,
  `country` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invited`
--

INSERT INTO `invited` (`sid`, `mail_batch`, `therArea`, `lastName`, `firstName`, `middleName`, `suffix`, `company`, `country`) VALUES
(1, 'sth/a11a2', 'Oncology', 'Ivan', 'Nora', '', '', '', 'Malaysia'),
(2, 'sth/a22a2', 'Diabetes', 'Eric', 'N. P', '', '', '', 'Malaysia'),
(3, 'sth/a33a2', 'Psychiatry', 'Juli', 'Asre', '', '', '', 'Malaysia'),
(4, 'sth/a44a2', 'Oncology', 'Alma', 'Lorn', '', '', '', 'Philippines'),
(5, 'sth/a55a2', 'Psychiatry', 'Shan', 'Virg', '', '', '', 'Philippines'),
(6, 'sth/a61a2', 'Oncology', 'Iv', 'Nor', '', '', '', 'India'),
(7, 'sth/a72a2', 'Diabetes', 'Er', 'Nope', '', '', '', 'India'),
(8, 'sth/a83a2', 'Psychiatry', 'Jules', 'Asr', '', '', '', 'India'),
(9, 'sth/a94a2', 'Oncology', 'Alm', 'Lorner', '', '', '', 'India'),
(10, 'sth/a105a2', 'Oncology', 'Shanny', 'Viror', '', '', '', 'Malaysia');

-- --------------------------------------------------------

--
-- Table structure for table `invited_user_email`
--

CREATE TABLE `invited_user_email` (
  `id` int(100) NOT NULL,
  `sid` int(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `correctedEmail` varchar(50) NOT NULL,
  `dataType` varchar(20) NOT NULL,
  `role` varchar(30) NOT NULL,
  `refer` int(100) NOT NULL,
  `failStatus` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: not failed; 1: failed'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invited_user_email`
--

INSERT INTO `invited_user_email` (`id`, `sid`, `email`, `correctedEmail`, `dataType`, `role`, `refer`, `failStatus`) VALUES
(1, 1, 'test1CSC@free.com', '', 'self', 'investigator', 0, 1),
(2, 2, 'testfea1@inv.com', '', 'self', 'investigator', 0, 0),
(3, 3, 'testEmail3@test.com', '', 'self', 'investigator', 0, 1),
(4, 4, 'testEmail4@test.com', '', 'self', 'investigator', 0, 0),
(5, 5, 'testEmail5@test.com', '', 'self', 'csc', 0, 0),
(6, 1, 'test2CSC@free.com', '', 'self', 'investigator', 0, 0),
(7, 2, 'testfea2@inv.com', '', 'self', 'investigator', 0, 0),
(8, 3, 'test2Email3@test.com', '', 'self', 'investigator', 0, 0),
(9, 4, 'test2Email4@test.com', '', 'self', 'investigator', 0, 0),
(10, 5, 'test2Email5@test.com', '', 'self', 'investigator', 0, 0),
(11, 6, 'test1CSC2@free.com', '', 'self', 'investigator', 0, 1),
(12, 7, 'testfea12@inv.com', '', 'self', 'investigator', 0, 0),
(13, 8, 'testEmail32@test.com', '', 'self', 'investigator', 0, 0),
(14, 9, 'testEmail42@test.com', '', 'self', 'investigator', 0, 0),
(15, 10, 'testEmail52@test.com', '', 'self', 'investigator', 0, 0),
(16, 6, 'test2CSC2@free.com', '', 'self', 'investigator', 0, 0),
(17, 7, 'testfea22@inv.com', '', 'self', 'csc', 0, 0),
(18, 8, 'test2Email32@test.com', '', 'self', 'investigator', 0, 1),
(19, 9, 'test2Email42@test.com', '', 'self', 'investigator', 0, 0),
(20, 10, 'test2Email52@test.com', '', 'self', 'csc', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `signup_users`
--

CREATE TABLE `signup_users` (
  `user_id` bigint(10) NOT NULL,
  `prefix` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alt_email` varchar(255) NOT NULL,
  `role` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email_verify` tinyint(1) NOT NULL COMMENT '0-email not verified,1-Email verified',
  `password_reset_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Process not started and user can login,1-Process started and user is not able to login at this stage,2-Process completed and user can login',
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `profile_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Profile not completed,1-Profile completed',
  `role_change_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Role not changed 1: Role changed once',
  `profile_pic` varchar(100) NOT NULL,
  `stripe_customer_id` varchar(25) NOT NULL,
  `reminded` int(100) NOT NULL COMMENT 'timestamp for last reminder'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `signup_users`
--

INSERT INTO `signup_users` (`user_id`, `prefix`, `name`, `last_name`, `email`, `alt_email`, `role`, `password`, `email_verify`, `password_reset_status`, `added_date`, `profile_status`, `role_change_status`, `profile_pic`, `stripe_customer_id`, `reminded`) VALUES
(156, 'Dr.', 'jhjhj', 'jfgjhj', 'testfea1@inv.com', '', 'investigator', '202cb962ac59075b964b07152d234b70', 1, 0, '2017-12-18 13:21:06', 1, 0, '', 'cus_ByYmSsXDhTmGNp', 1513653666),
(157, 'Dr.', 'gdfgd', 'gfsdgds', 'testfea2@inv.com', '', 'investigator', '202cb962ac59075b964b07152d234b70', 1, 0, '2017-12-18 13:26:41', 1, 0, '', 'cus_ByYs04QxC4c2M6', 1513654001),
(158, 'Dr.', 'gfdgd', 'gfdgdfas', 'testfea3@inv.com', '', 'investigator', '202cb962ac59075b964b07152d234b70', 1, 0, '2017-12-18 14:48:06', 1, 0, '', 'cus_ByaBSrXAEeLqv3', 1513658886),
(159, 'Dr.', 'fffffff', 'rrrrr', 'test2Email52@test.com', '', 'investigator', '202cb962ac59075b964b07152d234b70', 1, 0, '2017-12-18 17:18:56', 1, 0, '', 'cus_ByccSfTgViIVtx', 1513667936),
(160, '', '', '', 'testEmail5@test.com', '', 'investigator', '202cb962ac59075b964b07152d234b70', 1, 0, '2017-12-22 00:40:44', 0, 0, '', 'cus_BzrQBInDJhkUTg', 1513953644),
(161, '', '', '', 'paymail2@inv.com', '', 'investigator', '202cb962ac59075b964b07152d234b70', 1, 0, '2017-12-22 16:54:16', 0, 0, '', 'cus_C077rDw3qII8kL', 1514012056),
(162, '', '', '', 'paymail3@inv.com', '', 'investigator', '202cb962ac59075b964b07152d234b70', 1, 0, '2017-12-22 20:43:31', 0, 0, '', 'cus_C0Ap4oPTqJHiN8', 1514025811),
(163, '', '', '', 'testEmail42@test.com', '', 'investigator', '202cb962ac59075b964b07152d234b70', 1, 0, '2017-12-22 20:53:21', 0, 0, '', 'cus_C0AzVjgnkDoynS', 1514026401),
(164, '', '', '', 'paymail5@inv.com', '', 'investigator', '202cb962ac59075b964b07152d234b70', 1, 0, '2017-12-22 20:56:13', 0, 0, '', 'cus_C0B1xG5NW9jX0a', 1514026573),
(165, '', '', '', 'paymail6@inv.com', '', 'investigator', '202cb962ac59075b964b07152d234b70', 1, 0, '2017-12-22 21:05:52', 0, 0, '', 'cus_C0BB8dyV3awvql', 1514027152),
(166, 'Dr.', 'vbcxbv', 'bcvxb', 'testCSC1@free.com', '', 'csc', '202cb962ac59075b964b07152d234b70', 1, 0, '2018-01-10 14:40:00', 1, 0, '', 'cus_C7CFgMO81ryqQb', 1515645600),
(167, 'Dr.', 'vcxbv', 'cbvcxb', 'test1CSC@free.com', '', 'csc', '202cb962ac59075b964b07152d234b70', 1, 0, '2018-01-10 16:45:13', 0, 0, '', 'cus_C7EGdAZ5A3WtCm', 1515653113),
(168, 'Dr.', 'hdgfhgf', 'hghf', 'testCSC2@free.com', '', 'csc', '202cb962ac59075b964b07152d234b70', 1, 0, '2018-01-10 21:38:06', 1, 0, '', '', 1515670686),
(169, 'Dr.', 'fdgdg', 'hfdh', 'testCSC3@free.com', '', 'csc', '202cb962ac59075b964b07152d234b70', 1, 0, '2018-01-10 21:39:20', 1, 0, '', 'cus_C7J1QJWBIjIah6', 1515670760),
(170, 'Dr.', 'bcvb', 'bvcb', 'csc1@free.com', '', 'csc', '202cb962ac59075b964b07152d234b70', 1, 0, '2018-01-18 14:36:32', 1, 0, '', 'cus_CAC03V9JXFZT6A', 1516336592),
(171, 'Dr.', 'dfg', 'hdhfg', 'testfea125435@inv.com', '', 'csc', '202cb962ac59075b964b07152d234b70', 1, 0, '2018-01-25 15:02:32', 1, 0, '', 'cus_CCp0PSaiH2H0zN', 1516942952);

-- --------------------------------------------------------

--
-- Table structure for table `therapeutic_areas`
--

CREATE TABLE `therapeutic_areas` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `therapeutic_areas`
--

INSERT INTO `therapeutic_areas` (`id`, `name`, `description`) VALUES
(1, 'Allergy and Immunology', 'Allergy/Immunology'),
(2, 'Anesthesiology', 'Anesthesiology'),
(3, 'Cardiology', 'Cardiology'),
(4, 'Cardiovascular Disease', 'Cardiovascular Disease'),
(5, 'Critical Care Medicine', 'Critical Care Medicine'),
(6, 'Dermatology', 'Dermatology'),
(7, 'Oncology', 'Oncology'),
(8, 'Child and Adolescent Psychiatry', 'Child and Adolescent Psychiatry'),
(9, 'Child Psychiatry', 'Child Psychiatry'),
(10, 'Clinical and Lab Immunology', 'Clinical and Lab Immunology'),
(11, 'Clinical Cardiology', 'Clinical Cardiology'),
(12, 'Clinical Genetics ', 'Clinical Genetics '),
(13, 'Clinical Neurophysiology', 'Clinical Neurophysiology'),
(14, 'Clinical Pathology', 'Clinical Pathology'),
(15, 'Clinical Pharmacology', 'Clinical Pharmacology'),
(16, 'Colon and Rectal Surgery', 'Colon and Rectal Surgery'),
(17, 'Dentistry', 'Dentistry'),
(18, 'Dermatological Immunology', 'Dermatological Immunology'),
(19, 'Diagnostic Radiology', 'Diagnostic Radiology'),
(20, 'Emergency Medicine', 'Emergency Medicine'),
(21, 'Diabetes', 'Diabetes'),
(35, 'Interventional Cardiology', 'Interventional Cardiology'),
(36, 'Lab Diagnostics Physician', 'Lab Diagnostics Physician'),
(37, 'Maternal and Fetal Medicine', 'Maternal and Fetal Medicine'),
(38, 'Medical Genetics', 'Medical Genetics'),
(39, 'Medical Oncology', 'Medical Oncology'),
(65, 'Preventive Medicine', 'Preventive Medicine'),
(66, 'Psychiatry', 'Psychiatry'),
(67, 'Public Health', 'Public Health');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `country_name` (`country_name`);

--
-- Indexes for table `csc_details`
--
ALTER TABLE `csc_details`
  ADD PRIMARY KEY (`det_id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `csc_in_relation`
--
ALTER TABLE `csc_in_relation`
  ADD PRIMARY KEY (`det_id`),
  ADD KEY `csc_user_id` (`csc_user_id`),
  ADD KEY `investigator_userid` (`investigator_userid`),
  ADD KEY `csc_email` (`csc_email`);

--
-- Indexes for table `email_subscription`
--
ALTER TABLE `email_subscription`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `investigator_details`
--
ALTER TABLE `investigator_details`
  ADD PRIMARY KEY (`det_id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `invited`
--
ALTER TABLE `invited`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `mail_batch` (`mail_batch`),
  ADD KEY `therArea` (`therArea`),
  ADD KEY `country` (`country`),
  ADD KEY `company` (`company`);

--
-- Indexes for table `invited_user_email`
--
ALTER TABLE `invited_user_email`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `correctedEmail` (`correctedEmail`),
  ADD KEY `role` (`role`),
  ADD KEY `sid` (`sid`,`failStatus`),
  ADD KEY `sid_2` (`sid`,`correctedEmail`,`role`,`failStatus`);

--
-- Indexes for table `signup_users`
--
ALTER TABLE `signup_users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `therapeutic_areas`
--
ALTER TABLE `therapeutic_areas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invited_user_email`
--
ALTER TABLE `invited_user_email`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
