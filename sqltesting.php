<?php 
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "sqltest";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

?>
<form method="get" role="form">
    <div class="form-group col-lg-6">
    <p class="help-block">Unsubscribed?</p>
        <select name="user_type"  id="user_type">
            <option class="tmp" value="0" >None</option>
            <option class="tmp" value="2" >All of them</option>
            <option class="tmp" value="1" >Only them</option>
        </select>
    </div>
    <div class="form-group col-lg-6">
    <p class="help-block">Failed emails?</p>
        <select name="failed_type"  id="failed_type">
            <option class="tmp" value="0" >None</option>
            <option class="tmp" value="2" >All of them</option>
            <option class="tmp" value="1" >Only them</option>
        </select>
    </div>
    <div class="form-group col-lg-6">
    <p class="help-block">Role</p>
        <select name="role"  id="role" data-placeholder="Choose role..." class="chosen-select" style="width:100%;" tabindex="4">
                <option class="text-left" value="investigator">Investigator</option>
                <option class="text-left" value="csc">CSC</option>
        </select>
    </div>
    <div class="form-group col-lg-6">
    <p class="help-block">Therapeutic Area</p>
        <select id="ther_area" name="therapeutic_area" data-placeholder="Choose ther Area..." class="chosen-select" style="width:100%;" tabindex="4">
        <?php
           echo  $sql = "SELECT DISTINCT ('therArea'), `therArea` FROM invited";
            $result = $conn->query($sql);
            while($row = $result->fetch_assoc()) { ?>
                <option class="text-left" value="<?php echo $row['therArea'];?>"><?php echo $row['therArea'];?></option>
            <?php }
            ?>
        </select>
    </div>
   
    <div class="form-group col-lg-6">
    <p class="help-block">Countries</p>
        <select id="countries" name="countries" data-placeholder="Choose Country..." class="chosen-select" style="width:100%;" tabindex="4">
        <?php
         $sql_countries = "SELECT * FROM countries";
         $result_countries = $conn->query($sql_countries);
            while($row = $result_countries->fetch_assoc()) { ?>
                <option class="text-left" value="<?php echo $row['country_name'];?>"><?php echo $row['country_name'];?></option>
            <?php }
            ?>
        </select>
    </div>

    <div class="form-group col-lg-6">
    <p class="help-block">User status</p>
        <select id="user_types" name="user_types" data-placeholder=" User Types..." class="chosen-select" style="width:100%;" tabindex="4">
                <option class="text-left" value="1">First</option>
                <option class="text-left" value="2">Second</option>
                <option class="text-left" value="3">Third</option>
                <option class="text-left" value="4">Fourth</option>
                <option class="text-left" value="5">Fifth</option>
                <option class="text-left" value="6">Sixth</option>
        </select>
    </div>
    </br>
    <div class="form-group col-lg-2 text-center">
        <button type="submit" class="btn btn-primary" name="refresh" value="refresh">Refresh</button>
    </div>
</form>
</br>

<?php

$sql_user_type = "SELECT DISTINCT (t1.email) as t1email, t1.sid AS t1sid, t1.unsubscribe as unsubscribe, 
t2.email AS t2email, t2.profile_status AS profile_status, t2.name AS name, t2.last_name AS t2lastName,
t3.sid AS t3sid, t3.email AS t3email, t3.failStatus AS failStatus, t3.role AS role,
t4.sid AS t4sid, t4.therArea AS therArea, t4.firstName AS firstName, t4.lastName AS lastName, t4.country AS country
FROM 
email_subscription t1, signup_users t2, invited_user_email t3, invited t4
WHERE ";

switch ((isset($_GET['user_types']) ? $_GET['user_types'] : '')) {
    /* Case 1 use for check the User status - 1   */
    case '1':
            if(isset($_GET['user_type'])){
                $sql_user_type .= " t1.unsubscribe LIKE '%" . $_GET['user_type'] . "%' AND t1.email = t2.email AND t1.sid = t3.sid";
            }
            if(isset($_GET['failed_type']))
            {
                $sql_user_type .= " AND t3.failStatus LIKE '%" . $_GET['failed_type'] . "%' ";
            }
            if(isset($_GET['role']))
            {
                $sql_user_type .= " AND t3.role LIKE '%" . $_GET['role'] . "%' ";
            }
            if(isset($_GET['therapeutic_area']))
            {
                $sql_user_type .= " AND t4.therArea LIKE '%" . $_GET['therapeutic_area'] . "%' AND t4.sid = t1.sid ";
            }
            if(isset($_GET['countries']))
            {
                $sql_user_type .= " AND t4.country LIKE '%" . $_GET['countries'] . "%'";
            }
            
           $sql_user_type .= " AND t2.profile_status = '1' ";

           $result_user_type = $conn->query($sql_user_type); 
            if ($result_user_type->num_rows > 0) {
                while($row = $result_user_type->fetch_assoc()) {
                    echo "Name: ".$row['firstName']." ".$row['lastName']."<br>";
                    echo "Name: ".$row['name']." ".$row['t2lastName']."<br>";
                }
            }
            else{
                echo "No Result Found";
            }
        break;

    case '2':
       /* Case 2 use for check the User status - 2   */
        if(isset($_GET['user_type'])){
            $sql_user_type .= " t1.unsubscribe LIKE '%" . $_GET['user_type'] . "%' AND t1.email = t2.email AND t1.sid = t3.sid";
        }
        if(isset($_GET['failed_type']))
        {
            $sql_user_type .= " AND t3.failStatus LIKE '%" . $_GET['failed_type'] . "%' ";
        }
        if(isset($_GET['role']))
        {
            $sql_user_type .= " AND t3.role LIKE '%" . $_GET['role'] . "%' ";
        }
        if(isset($_GET['therapeutic_area']))
        {
            $sql_user_type .= " AND t4.therArea LIKE '%" . $_GET['therapeutic_area'] . "%' AND t4.sid = t1.sid ";
        }
        if(isset($_GET['countries']))
        {
            $sql_user_type .= " AND t4.country LIKE '%" . $_GET['countries'] . "%'";
        }
        
       $sql_user_type .= " AND t2.profile_status = '0' ";
       $result_user_type = $conn->query($sql_user_type); 
            if ($result_user_type->num_rows > 0) {
                while($row = $result_user_type->fetch_assoc()) {
                    echo "Name: ".$row['firstName']." ".$row['lastName']."<br>";
                }
            }
            else{
                echo "No Result Found";
            }
        break;

    case '3':
        /* Case 3 use for check the User status - 3   */
        if(isset($_GET['user_type'])){
            $sql_user_type .= " t1.unsubscribe LIKE '%" . $_GET['user_type'] . "%' AND t1.sid = t3.sid";
        }
        if(isset($_GET['failed_type']))
        {
            $sql_user_type .= " AND t3.failStatus LIKE '%" . $_GET['failed_type'] . "%' ";
        }
        if(isset($_GET['role']))
        {
            $sql_user_type .= " AND t3.role LIKE '%" . $_GET['role'] . "%' ";
        }
        if(isset($_GET['therapeutic_area']))
        {
            $sql_user_type .= " AND t4.therArea LIKE '%" . $_GET['therapeutic_area'] . "%' AND t4.sid = t1.sid ";
        }
        if(isset($_GET['countries']))
        {
            $sql_user_type .= " AND t4.country LIKE '%" . $_GET['countries'] . "%'";
        }
        
        $sql_user_type .= " AND t1.unsubscribe = '0' AND t1.email != t2.email GROUP BY t2.email";

        $result_user_type = $conn->query($sql_user_type); 
        if ($result_user_type->num_rows > 0) {
            while($row = $result_user_type->fetch_assoc()) {
                echo "Name: ".$row['name']." ".$row['t2lastName']."<br>";
            }
        }
        else{
        echo "No Result Found";
        }
        //echo "3";
        break;

        /* Case 4 use for check the User status - 4   */
        case '4':
        $sql_user_type .= " NOT EXISTS (SELECT p1.email, p2.email FROM email_subscription p1, invited_user_email p2 WHERE p1.email = p2.email AND t2.email = p1.email AND t2.email = p2.email)";

        if(isset($_GET['user_type'])){
            $sql_user_type .= " AND t1.unsubscribe LIKE '%" . $_GET['user_type'] . "%' AND t1.sid = t3.sid";
        }
        if(isset($_GET['failed_type']))
        {
            $sql_user_type .= " AND t3.failStatus LIKE '%" . $_GET['failed_type'] . "%' ";
        }
        if(isset($_GET['role']))
        {
            $sql_user_type .= " AND t3.role LIKE '%" . $_GET['role'] . "%' ";
        }
        if(isset($_GET['therapeutic_area']))
        {
            $sql_user_type .= " AND t4.therArea LIKE '%" . $_GET['therapeutic_area'] . "%' AND t4.sid = t1.sid ";
        }
        if(isset($_GET['countries']))
        {
            $sql_user_type .= " AND t4.country LIKE '%" . $_GET['countries'] . "%'";
        }
        
       $sql_user_type .= " AND t1.unsubscribe = '1' GROUP BY t2.email";

        $result_user_type = $conn->query($sql_user_type); 
        if ($result_user_type->num_rows > 0) {
            while($row = $result_user_type->fetch_assoc()) {
                echo "Email Address: ".$row['t2email']."<br>";
            }
        }
        else{
        echo "No Result Found";
        }
        //echo "4";
        break;

        /* Case 5 use for check the User status - 5   */
        case '5':
        if(isset($_GET['user_type'])){
            $sql_user_type .= " t1.unsubscribe LIKE '%" . $_GET['user_type'] . "%' AND t1.sid = t3.sid";
        }
        if(isset($_GET['failed_type']))
        {
            $sql_user_type .= " AND t3.failStatus LIKE '%" . $_GET['failed_type'] . "%' ";
        }
        if(isset($_GET['role']))
        {
            $sql_user_type .= " AND t3.role LIKE '%" . $_GET['role'] . "%' ";
        }
        if(isset($_GET['therapeutic_area']))
        {
            $sql_user_type .= " AND t4.therArea LIKE '%" . $_GET['therapeutic_area'] . "%' AND t4.sid = t1.sid ";
        }
        if(isset($_GET['countries']))
        {
            $sql_user_type .= " AND t4.country LIKE '%" . $_GET['countries'] . "%'";
        }
        
       $sql_user_type .= " AND t1.unsubscribe = '1' GROUP BY t2.email";

        $result_user_type = $conn->query($sql_user_type); 
        if ($result_user_type->num_rows > 0) {
            while($row = $result_user_type->fetch_assoc()) {
                echo "Email Address: ".$row['t2email']."<br>";
            }
        }
        else{
        echo "No Result Found";
        }
        //echo "5";
        break;

        /* Case 6 use for check the User status - 6  */
        case '6':
        $sql_user_type .= " NOT EXISTS (SELECT p1.sid, p2.sid FROM email_subscription p1, invited_user_email p2 WHERE p1.sid = p2.sid AND t4.sid = p2.sid AND t4.sid = p1.email)";
        if(isset($_GET['user_type'])){
            $sql_user_type .= " AND t1.unsubscribe LIKE '%" . $_GET['user_type'] . "%'";
        }
        if(isset($_GET['failed_type']))
        {
            $sql_user_type .= " AND t3.failStatus LIKE '%" . $_GET['failed_type'] . "%' ";
        }
        if(isset($_GET['role']))
        {
            $sql_user_type .= " AND t3.role LIKE '%" . $_GET['role'] . "%' ";
        }
        if(isset($_GET['therapeutic_area']))
        {
            $sql_user_type .= " AND t4.therArea LIKE '%" . $_GET['therapeutic_area'] . "%' ";
        }
        if(isset($_GET['countries']))
        {
            $sql_user_type .= " AND t4.country LIKE '%" . $_GET['countries'] . "%'";
        }
        
       $sql_user_type .= " GROUP BY t4.sid";

        $result_user_type = $conn->query($sql_user_type); 
        if ($result_user_type->num_rows > 0) {
            while($row = $result_user_type->fetch_assoc()) {
                echo "Name: ".$row['firstName']." ".$row['lastName']."<br>";
            }
        }
        else{
        echo "No Result Found";
        }
        //echo "6";
        break;
    default:
        //echo "Null";
        break;
}
?>