<?php

//require("class.phpmailer.php");
include_once("condata.ini.php");

class general{
	public $key = '-@rrowk3y-';
    var $mailgunClient;
    var $mailgun;
    
    function __construct() {
        # Include the Autoloader (see "Libraries" for install instructions)
        if (file_exists('./vendor/autoload.php')) {
            require './vendor/autoload.php';
        } else {
            require '../vendor/autoload.php';
        }

        //init stripe
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

        // use Mailgun\Mailgun;

        # Instantiate the client.
        $this->mailgunClient = new \Http\Adapter\Guzzle6\Client();
        $this->mailgun = new \Mailgun\Mailgun(MAILGUN_API_KEY, $this->mailgunClient);
    }
    
    /**
	 * Retrieves the template file for sending email
     *
     * @param string $TemplateFileName [File name only, without path]
	 * 
	 * @return (string)
	 */
    function LoadEmailTemplate($TemplateFileName) {
        if (file_exists('./mail/'.$TemplateFileName)) {
            return file_get_contents('./mail/'.$TemplateFileName);
        } else {
            return file_get_contents('../mail/'.$TemplateFileName);
        }
    }

    /**
     * Parse email template with corresponded values
     *
     * @param string $body_html HTML email content
     * @param array $mailKeyToNameArray Array of key => value to replace in email template
     * 
     * @return string
     */
    function parseEmailTemplate($body_html, $mailKeyToNameArray) {
        preg_match_all('#\\{cTAG\\}(.*?)\\{/cTAG\\}#s', $body_html, $matches);

        foreach ($matches[1] as $seq => $match) {
            if (array_key_exists($match, $mailKeyToNameArray)) {
                $matches[1][$seq] = $mailKeyToNameArray[$match];
                $body_html = preg_replace('#\\{cTAG\\}'.$match.'\\{/cTAG\\}#s', $mailKeyToNameArray[$match], $body_html);
            }
        }
        
        return $body_html;
    }

    function connectDB(){
	
		//echo "Hello";exit;
		//echo "----->".$host;exit;
		$host = DB_HOST;
		$user = DB_USER;
		$pass = DB_PASS;
		$dbname = DB_NAME;
		
		//echo $user;exit;
		try {
		  # MS SQL Server and Sybase with PDO_DBLIB
		  //$DBH = new PDO("mssql:host=$host;dbname=$dbname, $user, $pass");
		  //$DBH = new PDO("sybase:host=$host;dbname=$dbname, $user, $pass");
		  # MySQL with PDO_MYSQL
		  $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
		 	//echo "Database connection established";
		  # SQLite Database
		  //$DBH = new PDO("sqlite:my/database/path/database.db");
		  return $DBH;
		}
		catch(PDOException $e) {
			echo $e->getMessage();
			exit;
		}
    }//END connectDB
	
    function closeDB($DB){
        $DB=NULL;
        return true;
    }
    
    /**
     * Executes query
     * 
     * @param string  $sql    [Query to be executed]
     * @param boolean $single [If there is only one record found, 'true' will return 1 dimentional array]
     * @param array   $arrVal [Array of values to use in update query. Values should follow same order defined in $sql]
     * 
     * @return array
     */
    function checkOffer($user_ref_str){
        $sql="SELECT * FROM offers WHERE `user_ref_str` = '".$user_ref_str."'";
        //exit;
        $dbCon=$this->connectDB();
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        $res=$stmt->fetch();
        $this->closeDB($dbCon);
        return $res;
    }
    
    function executeQuery($sql, $single=true, $arrVal=array()) {
        $rows  = array();
        $dbCon = $this->connectDB();
        $dbCon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt  = $dbCon->prepare($sql);
        
        if (!$stmt) {
            print_r($dbCon->errorInfo()); 
            return false;
        }
		
        try {
            if (empty($arrVal)) {
                $stmt->execute();
            } else {
                $stmt->execute($arrVal); //pass values for update query
            }
        } catch (PDOException $ex) {
            //echo '<pre>';print_r($ex->getCode().' = '.$ex->getMessage());echo '</pre>';
            return false;
        }
        
        if (empty($arrVal)) {
            if ($stmt->rowCount() == 1 && $single) {
                $rows = $stmt->fetch(PDO::FETCH_ASSOC);
            } else {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $rows[] = $row;
                }
            }
            
            return $rows;
        }
        
		$this->closeDB($dbCon);
		
		return true;
    }
    
    /**
     * Generates unique record id
     * 
     * @return id
     */

    function checkEmailSuppression($email){
        $this->mailgun->setApiVersion('v3');
        try{
            $complaints = $this->mailgun->get(MAILER_DOMAIN."/complaints/".$email);
            if($complaints->http_response_code == 200){
                return ['success'=>true,'goodEmail'=>false];
            }
        }catch(Exception $e){
            $complaintsError = $e->getMessage();
        }

        try{
            $bounces=$this->mailgun->get(MAILER_DOMAIN."/bounces/".$email);
            if($bounces->http_response_code == 200){
                return ['success'=>true,'goodEmail'=>false];
            }
        }catch(Exception $e){
            $bouncesError = $e->getMessage();
        }

        try{
            $unsubscribes=$this->mailgun->get(MAILER_DOMAIN."/unsubscribes/".$email);
            if($unsubscribes->http_response_code == 200){
                return ['success'=>true,'goodEmail'=>false];
            }
        }catch(Exception $e){
            $unsubscribesError = $e->getMessage();
        }

        if(
            (isset($bouncesError) && strpos($bouncesError,'Address not found in bounces table')) ||
            (isset($complaintsError) && strpos($complaintsError,'No spam complaints found for this address')) ||
            (isset($unsubscribesError) && strpos($unsubscribesError,'Address not found in unsubscribers table'))
        ){
            return ['success'=>true,'goodEmail'=>true];

        }else{
            return ['success'=>false,'goodEmail'=>false];
        }
    }

    function getRecordId()
    {
        $sql    = "SELECT UUID() AS id";
		$result = $this->executeQuery($sql);
        return $result['id'];
    }
	
	function printr($data){
		
		echo "<pre>";
				print_r($data);
		echo "</pre>";
	}
	
	function MaxId($tbl_name,$col_name){
		
			$dbCon=$this->connectDB();
			//echo
			$sql = "SELECT MAX(".$col_name.") as id FROM `".$tbl_name."` WHERE 1 ";
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			  while ($row = $stmt->fetch())
			  {
			  
			  	//echo "Hello";
				 // $row_res[]=$row;
				 // echo $row[0];
				  $res_id=$row[0];
			  } 
			  //$this->printr($row);
			  //$res_id=
			  ++$res_id;
			  $this->closeDB($dbCon);
			  return $res_id;
			  
	}
	
	
	//Function for checking duplicate values
	function re_entry($value1,$col_name1,$value2,$col_name2,$table,$where){ 
			
			
			
			  //			echo $value1."<br>";
			  //			echo $col_name1."<br>";
			  //			echo $value2."<br>";
			  //			echo $col_name2."<br>";
			
			//$inArr['signupemail'],'email',$inArr['role'],'role','custom16_signup_users',''
			//echo
			$sql = "SELECT * from `$table` WHERE $col_name1='$value1' AND $col_name2='$value2'";
			//exit;
			
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				//echo $row;
				return false;
				$this->closeDB($dbCon);
				//exit;
			}else{
				$this->closeDB($dbCon);
				return true;
			}
			
	}
    //Function for check if same email exists with any role
    function email_exists_verefied($email){

        //echo
        $sql="SELECT sid FROM `custom16_email_subscription` WHERE email='$email' AND email_status=1 AND unsubscribe=0";
        //exit;
        $dbCon=$this->connectDB();
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();
        if($row=$stmt->rowCount()){

            $row = $stmt->fetch();
            return $row;

        }else{
            return false;

            //echo "Not found"; exit;
        }





    }//END FUNCTION
	//Function for check if same email exists with any role
	function email_exists($email,$tbl_name){
		
		//echo
		$sql="SELECT email FROM `$tbl_name` WHERE email='$email'";
		//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "found";exit;
				return true;
				
			}else{
				return false;
				
				//echo "Not found"; exit;
			}
			
			
		
		
		
	}//END FUNCTION
	
	function common_signup($inArr){
		$user_id    = $this->MaxId("custom16_signup_users","user_id");
        $first_name = isset($inArr['name']) ? $inArr['name'] : '';
        $last_name  = isset($inArr['last_name']) ? $inArr['last_name'] : '';
        
		if (!filter_var($inArr['signupemail'], FILTER_VALIDATE_EMAIL) === false) {
			if($role_exists=$this->email_exists($inArr['signupemail'],'custom16_signup_users')){
				$token="error-email-exists";
			} else {
				$sql = "INSERT INTO custom16_signup_users (`user_id`, `name`, `last_name`, `email`, `role`, `password`, `email_verify`, `stripe_customer_id`, `reminded`) VALUES (:userid,:name,:last_name,:email,:role,:password,:email_verify, :stripe_customer_id, :reminded)";
				$dbCon=$this->connectDB();
				$q = $dbCon->prepare($sql);
				if($q->execute(array(':userid'=>$user_id,':name'=>$first_name,':last_name'=>$last_name,':email'=>$inArr['signupemail'],':role'=>$inArr['role'],':password'=>md5($inArr['signuppassword']),':email_verify'=>$inArr['email_verify'], ':stripe_customer_id'=>$inArr['stripe_customer_id'], ':reminded'=>strtotime('now')))){
					
					$this->closeDB($dbCon);

					/*
					 * Create initial subscription with '0' credit and 'Subscribed' status
					 */
					$usr_array['UserID'] = $user_id;
					$usr_array['CampaignID'] = $inArr['plan_id'];
					$usr_array['SubscriptionDate'] = date('Y-m-d');
					$usr_array['ValidTill'] = date('Y-m-d', strtotime($usr_array['SubscriptionDate']. ' +1 month'));
					$usr_array['RenewDate'] = date('Y-m-d', strtotime($usr_array['ValidTill']. ' +1 day'));
					$usr_array['FreePeriodUsed'] = '0';
					$usr_array['Credits'] = '0';
					$usr_array['Status'] = 'Subscribed';

					if ($this->SubscriptionIDExists($user_id, $usr_array['Status'])) {
						if($this->CreateSubscription($usr_array)){
							$token['SubscriptionID'] = $this->GetSubscriptionID($user_id, $usr_array['Status']);
							$token['UserID']    = $user_id;
							$token['UserEmail'] = $inArr['signupemail'];
							$token['ValidTill'] = $usr_array['ValidTill'];
							$token['RenewDate'] = $usr_array['RenewDate'];
						}else{
							$token = "error-creating-subscription";
						}
					}else{
						$token = "error-subscription-doesnt-exist";
					}
					// echo "<pre>";print_r($token);echo "</pre>";exit();
				} else {
					$token="error-1";
					$this->closeDB($dbCon);
				}
			}
		} else {
			$token="error-2";
		}
		return $token;
	}
	
	
	function decode($string){
		
		$res=base64_decode($string);
		$value=explode("-",$res);
		if($value[0]==TOKEN1 and $value[2]==TOKEN2){
			
			return $value[1];
			echo $value[1];exit;
		}else{
			
			return false;
		}
		
		
		
		
	}
	function email_verification($data,$email)
    {
		$sql1 = "SELECT email_verify from custom16_signup_users WHERE user_id=$data AND email='$email'";
			
		$dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql1);
		$stmt->execute();
			
			if($row=$stmt->rowCount()){
				$row = $stmt->fetch();
					if($row[0]==0){
						
						$sql1 = "UPDATE custom16_signup_users SET email_verify='1' WHERE user_id=$data AND email='$email'";
						$dbCon=$this->connectDB();
						$stmt = $dbCon->prepare($sql1);
						$stmt->execute();
						$this->closeDB($dbCon);
						return true;
						
					}else{
						
						$this->closeDB($dbCon);
						return false;
					}
					
			}else{
				//Email id and token does not match
				$this->closeDB($dbCon);
				return false;
			}
		//return true;
		
		
	}//END FUNCTION
function update_user_stripe_id($id,$email,$customer_id){
    $sql1 = "UPDATE custom16_signup_users SET stripe_customer_id='".$customer_id."' WHERE user_id=$id AND email='$email'";
    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql1);
    $stmt->execute();
    $this->closeDB($dbCon);
    return true;
}

function subscription_verification($data,$email){
		
		//echo  $data;
		//echo $email;exit;
	if($data){
        $sql1 = "SELECT email_status from custom16_email_subscription WHERE sid=$data AND email='$email'";
        $sql2 = "UPDATE custom16_email_subscription SET email_status=1 WHERE sid=$data AND email='$email'";
    }else{
        $sql1 = "SELECT email_status,sid from custom16_email_subscription WHERE email='$email'";
        $sql2 = "UPDATE custom16_email_subscription SET email_status=1 , unsubscribe=0 WHERE email='$email'";
        $upd_id=0;
	}
			//echo $sql1;exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql1);
			$stmt->execute();
			if($row=$stmt->rowCount()){
				$row = $stmt->fetch();
				//echo $row[0];
					if(isset($upd_id)){
                        $dbCon=$this->connectDB();
                        $stmt = $dbCon->prepare($sql2);
                        $stmt->execute();
						$upd_id = $row['sid'];
						$this->closeDB($dbCon);
						return $upd_id;
					}
					if($row['email_status']==0){
						//echo $sql1;exit;
						$dbCon=$this->connectDB();
						$stmt = $dbCon->prepare($sql2);
						$stmt->execute();
						$this->closeDB($dbCon);
						return true;
						
					}else{
						
						$this->closeDB($dbCon);
						return false;
					}
					//exit;
				
				
				//exit;
			}else{
				//Email id and token does not match
				$this->closeDB($dbCon);
				return false;
			}
		//return true;
		
		
	}//END FUNCTION

//Project details
function getProjectDetails($id){
    $sql = "SELECT title from custom16_clinical_projects WHERE project_id='$id'";
    $dbCon=$this->connectDB();
    $stmt = $dbCon->prepare($sql);
    $stmt->execute();
    if($row=$stmt->rowCount()){
        $row = $stmt->fetch();
        $this->closeDB($dbCon);
        if($row[0]==0){
            return $row[0];
        }else{
            //project id does not match
            $this->closeDB($dbCon);
            return false;
		}
        //exit;
    }else{
        //project id does not match
        $this->closeDB($dbCon);
        return false;
    }
}

// Store response
function addResponse($response){

    $id=$this->MaxId("response","response_id");
    $ip=$this->getRealIpAddr();
	$sql1="SELECT project_id,email,invitation_id,user_action FROM `response` WHERE project_id='".$response['projectID']."' AND invitation_id = '".$response['invitationID']."' AND email='".$response['email']."' ORDER BY created_at DESC LIMIT 1";
	$dbCon=$this->connectDB();
	$stmt = $dbCon->prepare($sql1);
	$stmt->execute();
	if($row=$stmt->rowCount()){
		$row=$stmt->fetch();
		if($row['user_action'] !== $response['action']){
            $sql2 = "INSERT INTO response (`response_id`, `project_id`, `email`, `ip`,`user_action`,`invitation_id`) VALUES (:response_id,:project_id,:email,:ip,:user_action,:invitation_id)";
            //print_r($sql); exit;
            $dbCon=$this->connectDB();
            $q = $dbCon->prepare($sql2);

            if(!$q->execute(array(':response_id'=>$id,':project_id'=>$response['projectID'],':email'=>$response['email'],':ip'=>$ip,':user_action'=>$response['action'],':invitation_id'=>$response['invitationID']))){
                //print_r($q); exit;
                $this->closeDB($dbCon);
                return false;
            }
            $this->closeDB($dbCon);
            return true;
		}
	}else{
        $sql2 = "INSERT INTO response (`response_id`, `project_id`, `email`, `ip`,`user_action`,`invitation_id`) VALUES (:response_id,:project_id,:email,:ip,:user_action,:invitation_id)";
        //print_r($sql); exit;
        $dbCon=$this->connectDB();
        $q = $dbCon->prepare($sql2);

        if(!$q->execute(array(':response_id'=>$id,':project_id'=>$response['projectID'],':email'=>$response['email'],':ip'=>$ip,':user_action'=>$response['action'],':invitation_id'=>$response['invitationID']))){
            //print_r($q); exit;
            $this->closeDB($dbCon);
            return false;
        }
        $this->closeDB($dbCon);
        return true;
	}
}
// Store comments questions
function addReference($post){
	if(isset($post['reference_text'])){
		$reference_text = $post['reference_text'];
		$comment_question = '';
		$success = 'Thank you for providing references. We appreciate your support.';
		$error = 'Sorry, your references were not submitted. Please contact us on helpdesk@credevo.com';
	}elseif(isset($post['comment_question'])){
        $reference_text = '';
        $comment_question = $post['comment_question'];
        $success = 'Thank you for sending your comments / questions. We’ll respond to you within 48 hours.';
        $error = 'Sorry, your comments / questions were not submitted. Please contact us on helpdesk@credevo.com';
	}else{
        $error = 'Sorry, your request were not submitted. Please contact us on helpdesk@credevo.com';
		return ['status'=>401,'error'=>$error];
	}
    $id=$this->MaxId("reference","comment_id");
    $ip=$this->getRealIpAddr();
	$sql2="INSERT INTO reference (`comment_id`,`project_id`,`email`,`reference_text`,`comment_question`,`ip`) VALUES (:comment_id,:project_id,:email,:reference_text,:comment_question,:ip)";
    $dbCon=$this->connectDB();
    $q = $dbCon->prepare($sql2);
    if(!$q->execute(array(':comment_id'=>$id,':project_id'=>$post['project_id'],':email'=>$post['email'],':reference_text'=>$reference_text,':comment_question'=>$comment_question,':ip'=>$ip))){
        //print_r($q); exit;
        $this->closeDB($dbCon);
        return ['status'=>401,'error'=>$error];
    }
    $this->closeDB($dbCon);
    return ['status'=>201,'success'=>$success];
}

//Get IP for user
function getRealIpAddr()
{
    if (isset($_SERVER['HTTP_X_SUCURI_CLIENTIP'])) {
    	$ipaddress = $_SERVER['HTTP_X_SUCURI_CLIENTIP'];
    } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } else if (isset($_SERVER['HTTP_FORWARDED'])) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } else if (isset($_SERVER['REMOTE_ADDR'])){
        $ipaddress = $_SERVER['REMOTE_ADDR'];
	}else {
        $ipaddress = 'UNKNOWN';
    }
    return $ipaddress;
}

	function unsubscribe($id,$batch){
		//print_r($id);exit;
		$sql1 = "SELECT unsubscribe FROM unsubscribed WHERE sid=$id AND mail_batch='$batch'";
		//print_r($sql1);exit;
		$dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql1);
		$stmt->execute();
		
		if($row=$stmt->rowCount()){
			$row = $stmt->fetch();
			//echo $row[0];
				if($row[0]==0){
					
					$sql1 = "UPDATE unsubscribed SET unsubscribe=1,date=CURRENT_TIMESTAMP WHERE sid=$id AND mail_batch='$batch'";
					//print_r($sql1);exit;
					$dbCon=$this->connectDB();
					$stmt = $dbCon->prepare($sql1);
					$stmt->execute();
					//print_r($sql1);exit;
					$this->closeDB($dbCon);
					return true;
					
				}else{
					
					$this->closeDB($dbCon);
					return false;
				}
				//exit;
			
			
			//exit;
		}else{
			//Email id and token does not match
			$this->closeDB($dbCon);
			return false;
		}
	}
	
	function resubscribe($id,$batch){
		//print_r($id);print_r($batch);exit;
		$sql1 = "SELECT unsubscribe FROM unsubscribed WHERE sid=$id AND mail_batch='$batch'";
		//print_r($sql1);exit;
		$dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql1);
		$stmt->execute();
		
		if($row=$stmt->rowCount()){
			$row = $stmt->fetch();
			//echo $row[0];
				if($row[0]==1){
					
					$sql1 = "UPDATE unsubscribed SET unsubscribe=0,date=CURRENT_TIMESTAMP WHERE sid=$id AND mail_batch='$batch'";
					//print_r($sql1);exit;
					$dbCon=$this->connectDB();
					$stmt = $dbCon->prepare($sql1);
					$stmt->execute();
					//print_r($sql1);exit;
					$this->closeDB($dbCon);
					return true;
					
				}else{
					
					$this->closeDB($dbCon);
					return false;
				}
				//exit;
			
			
			//exit;
		}else{
			//Email id and token does not match
			$this->closeDB($dbCon);
			return false;
		}
	}
	
	function unsub_news($id,$batch){
		//print_r($id);exit;
		$sql1 = "SELECT unsubscribe FROM custom16_email_subscription WHERE sid=$id AND email='$batch'";
		//print_r($sql1);exit;
		$dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql1);
		$stmt->execute();
		
		if($row=$stmt->rowCount()){
			$row = $stmt->fetch();
			//echo $row[0];
				if($row[0]==0){
					
					$sql1 = "UPDATE custom16_email_subscription SET unsubscribe=1,unsub_date=now() WHERE sid=$id AND email='$batch'";
					//print_r($sql1);exit;
					$dbCon=$this->connectDB();
					$stmt = $dbCon->prepare($sql1);
					$stmt->execute();
					//print_r($sql1);exit;
					$this->closeDB($dbCon);
					return true;
					
				}else{
					
					$this->closeDB($dbCon);
					return false;
				}
				//exit;
			
			
			//exit;
		}else{
			//Email id and token does not match
			$this->closeDB($dbCon);
			return false;
		}
	}
	
	function resub_news($id,$batch){
		//print_r($id);print_r($batch);exit;
		$sql1 = "SELECT unsubscribe FROM custom16_email_subscription WHERE sid=$id AND email='$batch'";
		//print_r($sql1);exit;
		$dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql1);
		$stmt->execute();
		
		if($row=$stmt->rowCount()){
			$row = $stmt->fetch();
			//echo $row[0];
				if($row[0]==1){
					
					$sql1 = "UPDATE custom16_email_subscription SET unsubscribe=0,unsub_date=now() WHERE sid=$id AND email='$batch'";
					//print_r($sql1);exit;
					$dbCon=$this->connectDB();
					$stmt = $dbCon->prepare($sql1);
					$stmt->execute();
					//print_r($sql1);exit;
					$this->closeDB($dbCon);
					return true;
					
				}else{
					
					$this->closeDB($dbCon);
					return false;
				}
				//exit;
			
			
			//exit;
		}else{
			//Email id and token does not match
			$this->closeDB($dbCon);
			return false;
		}
	}

	function gift1($id,$batch){
		//print_r($id);exit;
		$sql1 = "SELECT gift1 FROM custom16_email_subscription WHERE sid=$id AND email='$batch'";
		//print_r($sql1);exit;
		$dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql1);
		$stmt->execute();
		
		if($row=$stmt->rowCount()){
			$row = $stmt->fetch();
			//echo $row[0];
			if($row[0]==0){
				
				$sql1 = "UPDATE custom16_email_subscription SET gift1=1 WHERE sid=$id AND email='$batch'";
				//print_r($sql1);exit;
				$dbCon=$this->connectDB();
				$stmt = $dbCon->prepare($sql1);
				$stmt->execute();
				//print_r($sql1);exit;
				$this->closeDB($dbCon);
				$gift="done";
				return $gift;
				
			}else{
				
				$this->closeDB($dbCon);
				$gift="previously";
				return $gift;
			}
			//exit;
			
			
			//exit;
		}else{
			//Email id and token does not match
			$this->closeDB($dbCon);
			$gift="nope";
			return $gift;
		}
	}
	
	function feedback1($id,$batch,$r){
		// print_r($id);echo '<br/>';
		// print_r($batch);echo '<br/>';
		// print_r($r);echo '<br/>';
		// exit;
		$sql1 = "UPDATE custom16_email_subscription SET feedback1='$r' WHERE sid=$id AND email='$batch'";
		// print_r($sql1);exit;
		$dbCon=$this->connectDB();
		$stmt = $dbCon->prepare($sql1);
		if ($stmt->execute()){
			$this->closeDB($dbCon);
			return true;
		} else {
			$this->closeDB($dbCon);
			return false;
		}
	}
	
	function record($id,$batch,$page){
		//print_r($id);print_r($batch);exit;
	$aid=$this->MaxId("access_log","aid");
		//print_r($aid);exit;
	$sql = "INSERT INTO access_log (`aid`, `sid`, `mail_batch`, `page`) VALUES (:aid,:sid,:batch,:page)";
	//print_r($sql); exit;
				$dbCon=$this->connectDB();
				$q = $dbCon->prepare($sql);
		
				if(!$q->execute(array(':aid'=>$aid,':sid'=>$id,':batch'=>$batch,':page'=>$page))){
					//print_r($q); exit;
					return false;
				} 
	}
	
	function investigator_signup($inArr){
		
		//$this->printr($inArr);
		//exit;
		$user_id=$this->MaxId("custom16_signup_users","user_id");
		
		if (!filter_var($inArr['signupemail'], FILTER_VALIDATE_EMAIL) === false) {
			
				//echo("$email is a valid email address");
				//VALID EMAIL
			  } else {
				  
				//echo("$email is not a valid email address");
				//INVALID EMAIL
				$this->closeDB($dbCon);
				return false;
			}
			
			/*if($this->re_entry($inArr['signupemail'],'email',$inArr['role'],'role','custom16_signup_users','')){
						
			}else{
			
				  $this->closeDB($dbCon);
				  return false;
			}*/
		
		
			if($role_exists=$this->email_exists($inArr['signupemail'],'custom16_signup_users')){
			
			//echo "Email exists";
			//exit;
				$this->closeDB($dbCon);
				return false;
						
		     }else{
				
		     }
		
		
			$sql = "INSERT INTO custom16_signup_users (`user_id`, `name`, `last_name`, `email`, `role`, `password`, `email_verify`,`reminded`) VALUES (:userid,:name,:last_name,:email,:role,:password,:email_verify,:reminded)";
		
		/*
				INSERT INTO `custom16_clinicalstudynetwork`.`custom16_signup_users` (`user_id`, `name`, `last_name`, `email`, `role`, `password`, `email_verify`) VALUES ('1', 'test', 'test', 'test@test.com', 'test', 'test', '0');*/
			$dbCon=$this->connectDB();
			$q = $dbCon->prepare($sql);

			if($q->execute(array(':userid'=>$user_id,':name'=>'',':last_name'=>'',':email'=>$inArr['signupemail'],':role'=>$inArr['role'],':password'=>md5($inArr['signuppassword']),':email_verify'=>0,':reminded'=>strtotime('now')))){
				
				$this->closeDB($dbCon);
				$token=base64_encode(TOKEN1."-".$user_id."-".TOKEN2);
				return $token;
				//return true;
			}else{
				
				$this->closeDB($dbCon);
				return false;
			}
			
			
	}//END FUNCTION
	function trial_manager_signup($inArr){
		
		//$this->printr($inArr);
		//exit;
		$user_id=$this->MaxId("custom16_signup_users","user_id");
		
		if (!filter_var($inArr['signupemail'], FILTER_VALIDATE_EMAIL) === false) {
			
				//echo("$email is a valid email address");
				//VALID EMAIL
			  } else {
				  
				//echo("$email is not a valid email address");
				//INVALID EMAIL
				$this->closeDB($dbCon);
				return false;
			}
			/*if($this->re_entry($inArr['signupemail'],'email',$inArr['role'],'role','custom16_signup_users','')){
						
		}else{
			
			$this->closeDB($dbCon);
			return false;
		}
		
		*/
			if($role_exists=$this->email_exists($inArr['signupemail'],'custom16_signup_users')){
			
			//echo "Email exists";
			//exit;
				$this->closeDB($dbCon);
				return false;
						
		     }else{
				
		     }
			$sql = "INSERT INTO custom16_signup_users (`user_id`, `name`, `last_name`, `email`, `role`, `password`, `email_verify`, `reminded`) VALUES (:userid,:name,:last_name,:email,:role,:password,:email_verify,:reminded)";
		
		/*
				INSERT INTO `custom16_clinicalstudynetwork`.`custom16_signup_users` (`user_id`, `name`, `last_name`, `email`, `role`, `password`, `email_verify`) VALUES ('1', 'test', 'test', 'test@test.com', 'test', 'test', '0');*/
			$dbCon=$this->connectDB();
			$q = $dbCon->prepare($sql);

			if($q->execute(array(':userid'=>$user_id,':name'=>'',':last_name'=>'',':email'=>$inArr['signupemail'],':role'=>$inArr['role'],':password'=>md5($inArr['signuppassword']),':email_verify'=>0,':reminded'=>strtotime('now')))){
				
				$this->closeDB($dbCon);
				$token=base64_encode(TOKEN1."-".$user_id."-".TOKEN2);
				return $token;
				//return true;
			}else{
				
				$this->closeDB($dbCon);
				return false;
			}
			
	}//END OF FUNCTION
	
	
	function service_provider_signup($inArr){
		$user_id = $this->MaxId("custom16_signup_users","user_id");
		$detid = $this->MaxId("custom16_serviceproviders_details","user_id");
		if (!filter_var($inArr['signupemail'], FILTER_VALIDATE_EMAIL) === false) {
			if($role_exists=$this->email_exists($inArr['signupemail'],'custom16_signup_users')){
				$token="error-email-exists";		
			}else{
				$sql1 = "INSERT INTO custom16_signup_users (`user_id`, `name`, `last_name`, `email`, `role`, `password`, `email_verify`, `reminded`) VALUES (:userid,:name,:last_name,:email,:role,:password,:email_verify,:reminded)";
				$dbCon=$this->connectDB();
				$q = $dbCon->prepare($sql1);
				if($q->execute(array(':userid'=>$user_id,':name'=>'',':last_name'=>'',':email'=>$inArr['signupemail'],':role'=>$inArr['role'],':password'=>md5($inArr['signuppassword']),':email_verify'=>0,':reminded'=>strtotime('now')))){
					$sql2 = "INSERT INTO `custom16_serviceproviders_details` (`detid`, `user_id`, `qualifications`, `country_of_work`,`service_types`, `other_services`, `name_of_organization`, `address_of_organization`, `phone_no`, `mobile_no`, `file_name`, `file_path`, `user_type`, `accept_terms`) VALUES (:detid,:userid,:qualifications,:country,:services,:other_service,:orgname,:orgaddress,:phone,:mobile,:filename,:filepath,:usertype,:term)";
					$q = $dbCon->prepare($sql2);
					$q->execute(array(':detid'=>$detid,':userid'=>$user_id,':qualifications'=>'',':country'=>'',':services'=>$inArr['service_types'],':other_service'=>$inArr['signup-other-service'],':orgname'=>'',':orgaddress'=>'',':phone'=>'',':mobile'=>'',':filename'=>'',':filepath'=>'',':usertype'=>'',':term'=>''));
					$this->closeDB($dbCon);

					/*
					 * Create initial subscription with '0' credit and 'Subscribed' status
					 */
					$usr_array['UserID'] = $user_id;
					$usr_array['CampaignID'] = $inArr['plan_id'];
					$usr_array['SubscriptionDate'] = date('Y-m-d');
					$usr_array['ValidTill'] = date('Y-m-d', strtotime($usr_array['SubscriptionDate']. ' +1 month'));
					$usr_array['RenewDate'] = date('Y-m-d', strtotime($usr_array['ValidTill']. ' +1 day'));
					$usr_array['FreePeriodUsed'] = '0';
					$usr_array['Credits'] = '0';
					$usr_array['Status'] = 'Subscribed';

					if($this->SubscriptionIDExists($user_id, $usr_array['Status'])){
						if($this->CreateSubscription($usr_array)){
							$token['SubscriptionID'] = $this->GetSubscriptionID($user_id, $usr_array['Status']);
							$token['UserID'] = $user_id;
							$token['UserEmail'] = $inArr['signupemail'];
						}else{
							$token = "error-creating-subscription";
						}
					}else{
						$token = "error-subscription-doesnt-exist";
					}
					//$token=base64_encode(TOKEN1."-".$user_id."-".TOKEN2);
				}else{
					$this->closeDB($dbCon);
					$token="error-signing-up";
				}
			}
		} else {
			$token="error-invalid-email";
		}
	return $token;	
	}//END FUNCTION
	
	
		function log_in_user1($inArr){
		
			$error=0;
			if(isset($inArr['login-password'])){
                $email=trim($inArr['login-email']);
                $password=md5(trim($inArr['login-password']));
			}else{
                $email=trim($inArr['login-email']);
                $password=$inArr['downgrade-password'];
			}

			//echo
			$sql = "SELECT * from `custom16_signup_users` WHERE email='$email' AND password='".$password."' AND email_verify=1";
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			if($row=$stmt->rowCount()){
				
				//echo "User name and password match";exit;
				while ($row = $stmt->fetch())
			  	{
			  
					  $res[]=$row;
			  	} 
				
				//$this->printr($res);exit;
				$this->closeDB($dbCon);
				
				return $res;
				
				
			}else{
				
				$this->closeDB($dbCon);
				return false;
			}
		
		
		
		
		
}
	function log_in_user($inArr){
		
		
		
					if(isset($_COOKIE['login_user']) and !empty($_COOKIE['login_user']) and isset($_COOKIE['user_id']) and !empty($_COOKIE['user_id'])){
						
						header("location:login?logout");
						
						exit;

						
					}

			$error=0;
			$email=trim($inArr['login-email']);
			
			$password=trim($inArr['login-password']);
			
			
			//echo
			$sql = "SELECT * from `custom16_signup_users` WHERE email='$email' AND password='".md5($password)."' AND email_verify=1"; 
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if($row=$stmt->rowCount()){
				
				//echo "User name and password match";exit;
				while ($row = $stmt->fetch())
			  	{
			  
					  $res[]=$row;
			  	} 
				
				//$this->printr($res);exit;
				$this->closeDB($dbCon);
				
				return $res;
				
				
			}else{
				
				$this->closeDB($dbCon);
				return false;
			}
		
		
		
		
		
	}//END FUNCTION

    /**
     * Get user information if user with specified email exists
     * @param $email    Email of user
     *
     * @return array
     */
    function getUserInfo($email)
    {
        $sql = "SELECT * 
                FROM `custom16_signup_users` 
                WHERE email='".$email."' 
                LIMIT 1";

        return $this->executeQuery($sql, true);
    }

    /**
     * Get user information user with specified id, if exists
     * @param $id    ID of user
     *
     * @return array
     */
    function getUserInfoById($id, $type='')
    {
        $role_specific_join   = "";
        $role_specific_fields = "";

        if (!empty($type)) {
            if ($type == 'investigator') {
                $role_specific_fields = ", d.country_of_work, d.therpeutic_area_of_work, d.medica_practice_exp, d.ques1, d.ques2 ";
                $role_specific_join   = " LEFT JOIN custom16_investigator_details d ON u.user_id = d.user_id AND d.deleted = '0' ";
            }
        }

        $sql = "SELECT u.* ".$role_specific_fields."
                FROM `custom16_signup_users` u ".$role_specific_join." 
                WHERE u.user_id='".$id."' 
                LIMIT 1";

        return $this->executeQuery($sql, true);
    }

    /**
     * Check if user has verified email
     * @param $email    Email to verify
     *
     * @return array
     */
    function userVerificationInfo($email)
    {
        $sql = "SELECT user_id, email_verify ,stripe_customer_id, role
                FROM `custom16_signup_users` 
                WHERE email='".$email."' 
                LIMIT 1";

        return $this->executeQuery($sql, true);
    }
	
	function reset_password($email)
	{
			$sql1 = "SELECT user_id FROM `custom16_signup_users` WHERE email='$email' AND email_verify=1"; 
			$dbCon = $this->connectDB();
			$stmt  = $dbCon->prepare($sql1);
			$stmt->execute();
			
			if ($stmt->rowCount()) {
				$row = $stmt->fetch();
				//Set password reset status = 1
				//User is not able to login when password reset status = 1
				$sql2 = "UPDATE `custom16_signup_users` SET password_reset_status=1 WHERE user_id=$row[0]"; 
				$stmt = $dbCon->prepare($sql2);
				if (!$stmt->execute()) {
					$this->closeDB($dbCon);		
					return false;
				}
				
				$token = base64_encode(TOKEN1."-".$row[0]."-".TOKEN2);
				
				$this->closeDB($dbCon);		
				return $token;
			} else {
				$this->closeDB($dbCon);
				return false;
			}
		
	}
	
	function change_password_link($token, $email)
	{
		$user_id = $this->decode($token);
		
		$sql1 = "SELECT password_reset_status FROM `custom16_signup_users` WHERE email='$email' AND email_verify=1 AND user_id=$user_id"; 
		
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql1);
			$stmt->execute();
			
			if($stmt->rowCount()){
				
				$row = $stmt->fetch();
				//echo "------>".$row[0];exit;
				if($row[0]==1){
					
					$this->closeDB($dbCon);
					return true;
					
				}else{
					
					$this->closeDB($dbCon);
					return false;
				}
				
			}else{
				
				$this->closeDB($dbCon);
				return false;
			}
		
		//echo $user_id;
		//exit;

		
		
		
	}//END FUNCTION
	
	
	function change_password($token,$email,$newpass){ 
		
		
		
		//echo $token."<br>";
		//echo $email."<br>";
		
		$user_id=$this->decode($token);
		//echo
		$sql1 = "SELECT password_reset_status FROM `custom16_signup_users` WHERE email='".$email."' AND email_verify=1 AND user_id='".$user_id."'"; 
			//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql1);
			$stmt->execute();
			
			if($stmt->rowCount()){
				
				$row = $stmt->fetch();
				//echo "------>".$row[0];exit;
				if($row[0]==1){
				
				//echo md5($newpass);
				//echo
				$sql2 = "UPDATE `custom16_signup_users` SET password='".md5($newpass)."',password_reset_status='0' WHERE user_id=$user_id AND email='$email' AND password_reset_status=1"; 
					//exit;
					$stmt = $dbCon->prepare($sql2);
					$stmt->execute();
					
					$this->closeDB($dbCon);
					return true;
					
				}else{
					
					$this->closeDB($dbCon);
					return false;
				}
				
			}else{
				
				$this->closeDB($dbCon);
				return false;
			}
		
		//echo $user_id;
		//exit;

		
		
		
	}

function subscribe_now($inArr){
	
	if (!filter_var($inArr['email'], FILTER_VALIDATE_EMAIL) === false){
			  } else {
				//$this->closeDB($dbCon);
				return false;
			}
	
			//$this->printr($inArr);
			//exit;
			$sid=$this->MaxId("custom16_email_subscription","sid");
			
			if($this->email_exists($inArr['email'],'custom16_email_subscription')){
				
				//echo "hello";
				//exit;
				return false;
			}
			
			if(isset($inArr['status']) && $inArr['status'] == 1){
                $status=1;
                $token=$sid;
			}else{
                $status=0;
                $token=base64_encode(TOKEN1."-".$sid."-".TOKEN2);
			}
			//echo $sid;exit;
			
	$sql1 = "INSERT INTO custom16_email_subscription (`sid`, `email`, `email_status`) VALUES (:sid,:email,:status)";
	
				$dbCon=$this->connectDB();
				$q = $dbCon->prepare($sql1);
		
				if(!$q->execute(array(':sid'=>$sid,':email'=>$inArr['email'],':status'=>$status))){
			
					return false;
				}
			
			return $token;	
}

function checkRootDomain($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }

    $domain = implode('.', array_slice(explode('.', parse_url($url, PHP_URL_HOST)), -2));
    if ($domain == 'credevo.com') {
        return True;
    } else {
        return False;
    }

}

	function CredevoEncrypt($value=''){
		return base64_encode($value.$this->key);
	}

	function CredevoEncrypt2($value=''){
		return base64_encode($value);
	}

	function CredevoDecrypt($value=''){
		$encrypted = base64_decode($value);
		return substr($encrypted, 0, strlen($this->key));
	}
    
    /**
     * Generates HTML <select> form control
     * 
     * @param $ddlName     string 'Name and id of the select control'
     * @param $options     array  'Options of select control. Ex: $options = array('value'=>'display')'
     * @param $selectedVal string 'Default selected value'
     * @param $callback    array  'Event name as array key, and function name as array value. //To be implemented.'
     * @param $styleClass  string 'css class name'
     *
     * @return $select     string 'HTML <select> input control'
     */
    function CreateDDL($ddlName='', $options=array(), $selectedVal='', $callback=array(), $styleClass='') {
        $select       = '<select id="'.$ddlName.'" name="'.$ddlName.'" class="'.$styleClass.'">';
        
        foreach ($options as $key => $val) {
            $selected = ($key == $selectedVal) ? 'selected="selected"' : '';
            $select  .= '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
        }
        $select      .= '</select>';
        
        return $select;
    }
    
    /**
	 * Takes parameter as key to map with role defined in signup_users table
	 * @key string 'Name of Role in signup_users table'
	 * 
	 * @return role string
	 */
    function DetermineRole($key) {
        $role = array(
            'trial_manager'             => 'Trial Manager',
            'service_provider'          => 'Service Provider',
            'investigator'              => 'Site',
            'clinical_stud_coordinator' => 'Site'
        );
        return $role[$key];
    }

	function GetPlanDetail($PlanID) { 
		$sql = "SELECT * FROM plans WHERE id='".$PlanID."'";
		$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		
		if($row = $stmt->fetch()){
			$this->closeDB($dbCon);
			return $row;
		}else{
			$this->closeDB($dbCon);
			return false;
		}
	}
    
    function GetPlanDetailByRole($role, $plan) {
        $sql = "SELECT p.* 
			FROM `plans` p 
			WHERE `p`.`role` = '".$role."' AND p.campaign_name = '".$plan."'";
		$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
        
        if ($row = $stmt->fetch()) {
			$this->closeDB($dbCon);
			return $row;
		} else {
			$this->closeDB($dbCon);
			return false;
		}
    }

    function GetUserPlan($UserID){
        $sql = "SELECT p.*,ps.status, p.id AS plan_id, `ps`.`id` AS subscription_id, `ps`.`free_period_used`, `ps`.`subscription_date`, `ps`.`valid_till`, 
                `ps`.`renew_date`, `ps`.`discount_subscription`, `ps`.`discount_topup`, 
                `ps`.`credits` AS remaining_credits, `u`.`email` 
			FROM `plan_subscriptions` ps 
			LEFT JOIN plans p ON `ps`.`campaign_id` = `p`.`id` 
			LEFT JOIN custom16_signup_users u ON `ps`.`user_id` = `u`.`user_id` 
			WHERE ps.user_id = '".$UserID."'
            ORDER BY `ps`.`date_created` DESC LIMIT 1";
        $dbCon = $this->connectDB();
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();

        if ($row = $stmt->fetch()) {
            $this->closeDB($dbCon);
            return $row;
        } else {
            $this->closeDB($dbCon);
            return false;
        }
	}
	function GetPlanDetailByUser($UserID, $Status) {
		$sql = "SELECT p.*, p.id AS plan_id, `ps`.`id` AS subscription_id, `ps`.`free_period_used`, `ps`.`subscription_date`, `ps`.`valid_till`, 
                `ps`.`renew_date`, `ps`.`discount_subscription`, `ps`.`discount_topup`, 
                `ps`.`credits` AS remaining_credits, `u`.`email` 
			FROM `plan_subscriptions` ps 
			LEFT JOIN plans p ON `ps`.`campaign_id` = `p`.`id` 
			LEFT JOIN custom16_signup_users u ON `ps`.`user_id` = `u`.`user_id` 
			WHERE `ps`.`status` = '".$Status."' AND ps.user_id = '".$UserID."'
            ORDER BY `ps`.`subscription_date` DESC LIMIT 1";
		$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		
		if ($row = $stmt->fetch()) {
			$this->closeDB($dbCon);
			return $row;
		} else {
			$this->closeDB($dbCon);
			return false;
		}
	}
    
    /**
     * Creates subscription record in plan_subscriptions table
     *
     * @param string $usr_array['UserID']
     * @param string $usr_array['CampaignID']
     * @param string $usr_array['SubscriptionDate']
     * @param string $usr_array['ValidTill']
     * @param string $usr_array['RenewDate']
     * @param string $usr_array['FreePeriodUsed']
     * @param string $usr_array['Credits']
     * @param string $usr_array['Status']
     * @param string $usr_array['Type']
     *
     */
	function CreateSubscription($usr_array){
		$sql = "INSERT INTO `plan_subscriptions` (`campaign_id`, `user_id`, `subscription_date`, `valid_till`, `renew_date`, `free_period_used`, `credits`, `status`) VALUES (:CampaignID, :UserID, :SubscriptionDate, :ValidTill, :RenewDate, :FreePeriodUsed, :Credits, :Status)";
		
	    $con = $this->connectDB();
		$q = $con->prepare($sql);
		if($q->execute(array(':CampaignID'=>$usr_array['CampaignID'], ':UserID'=>$usr_array['UserID'], ':SubscriptionDate'=>$usr_array['SubscriptionDate'], ':ValidTill'=>$usr_array['ValidTill'], ':RenewDate'=>$usr_array['RenewDate'], ':FreePeriodUsed'=>$usr_array['FreePeriodUsed'], ':Credits'=>$usr_array['Credits'], ':Status'=>$usr_array['Status']))){
			$this->closeDB($con);
			return true;
		}else{
			$this->closeDB($con);
			return false;
		}
	}

	/**
	 * Retrieve the active subscripton id of the user
	 * @UserID = Logged in user id
	 * 
	 * @return ID
	 */
	function GetSubscriptionID($UserID, $Status='Active'){
		$sql = "SELECT `id` FROM plan_subscriptions WHERE user_id='".$UserID."' AND `status`='".$Status."' ORDER BY subscription_date DESC LIMIT 1";
		$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		$row = $stmt->fetch();
		$this->closeDB($dbCon);

		return $row[0];
	}

	/**
	 * Checks last subscription id of user with expected subscription status
	 * @UserID string = Logged in user id
	 * @status string = expected status
	 * 
	 * @return boolean
	 */
    function SubscriptionIDExists($UserID, $Status){
		$sql = "SELECT MAX(`id`) FROM plan_subscriptions WHERE user_id='".$UserID."' AND `status`='".$Status."'";
		$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		$row = $stmt->fetch();
		$this->closeDB($dbCon);

		if($row[0] == '' || is_null($row[0])){
			return true;
		}else{
			return false;
		}
	}

	function SetSubscriptionStatus($SubscriptionID, $status){
		$sql = "UPDATE `plan_subscriptions` SET status='".$status."' WHERE id='".$SubscriptionID."'";
		$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		$this->closeDB($dbCon);
		
		return true;
	}

	/**
	 * Get the number of credits for active subscripton of the user
	 * @UserID = Logged in user id
	 * 
	 * @return credits = (number)
	 */
	function GetCreditInfo($UserID){
		//$sql = "SELECT `credits` FROM plan_subscriptions WHERE user_id='".$UserID."' AND status='Active'";
		//$sql = "SELECT SUM(credit_purchased) AS credits FROM `transactions` WHERE credit_type != 'Spent' AND user_id='".$UserID."'";
		$sql = "SELECT IFNULL(SUM(credit_purchased), 0) - IFNULL((SELECT SUM(credit_purchased) AS spent_credits FROM `transactions` WHERE credit_type = 'Spent' AND user_id='".$UserID."'), 0) AS credits FROM `transactions` WHERE credit_type != 'Spent' AND user_id='".$UserID."'";
		$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		$row = $stmt->fetch();
		$this->closeDB($dbCon);
		
		return $row[0];
	}

	/**
	 * Update credits of subscription
	 * @SubscriptionID = Active Subscription ID of logged in user
	 * @credit = Number of credits to add
	 * @ValidTill = Date
	 * @RenewDate = Date
	 * @FreePeriodUsed = boolean
	 * @Status = Subscribed, Unsubscribed, Active, Inactive
	 * 
	 * @return boolean
	 */
	function UpdateCredit($SubscriptionID, $credit='0', $ValidTill, $RenewDate, $FreePeriodUsed, $Status='Active'){
		$ValidTill = ($ValidTill == "") ? "" : "valid_till='".$ValidTill."', ";
		$RenewDate = ($RenewDate == "") ? "" : "renew_date='".$RenewDate."', ";
		//$ValidTill = ($ValidTill == "") ? "" : " valid_till='".$ValidTill."'";

		$sql = "UPDATE `plan_subscriptions` SET ".$ValidTill.$RenewDate."free_period_used='".$FreePeriodUsed."', credits='".$credit."', status='".$Status."' WHERE id='".$SubscriptionID."'";
		$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		$this->closeDB($dbCon);
		
		return true;
	}
    
    function UpdateSubscription2($SubscriptionID, $credit='0', $ValidTill, $RenewDate, $FreePeriodUsed, $Status='Active', $discountSubscription='0.00', $discountTopup='0.00') {
        $ValidTill            = ($ValidTill == "") ? "" : "valid_till='".$ValidTill."', ";
		$RenewDate            = ($RenewDate == "") ? "" : "renew_date='".$RenewDate."', ";
		$discountSubscription = " discount_subscription='".$discountSubscription."', ";
		$discountTopup        = " discount_topup='".$discountTopup."', ";

		$sql = "UPDATE `plan_subscriptions` SET ".$ValidTill.$RenewDate.$discountSubscription.$discountTopup."free_period_used='".$FreePeriodUsed."', credits='".$credit."', status='".$Status."' WHERE id='".$SubscriptionID."'";
		return $this->executeQuery($sql);
    }
    
    function UpdateSubscription($subscriptionId, $credit='0', $validTill, $renewDate, $freePeriodUsed, $status='Active', $discountSubscription='0.00', $discountTopup='0.00') {
        $selectedFields  = '';
        $fieldVals       = array();
        
        if ($validTill != "") {
            $selectedFields .= 'valid_till=?, ';
            $fieldVals[]     = $validTill;
        }
        if ($renewDate != "") {
            $selectedFields .= 'renew_date=?, ';
            $fieldVals[]     = $renewDate;
        }
        if ($discountSubscription != "") {
            $selectedFields .= ' discount_subscription=?, ';
            $fieldVals[]     = $discountSubscription;
        }
        if ($discountTopup != "") {
            $selectedFields .= ' discount_topup=?, ';
            $fieldVals[]     = $discountTopup;
        }
        $fieldVals[] = $freePeriodUsed;
        $fieldVals[] = $credit;
        $fieldVals[] = $status;
        $fieldVals[] = $subscriptionId;

		$sql = "UPDATE `plan_subscriptions` SET ".$selectedFields."free_period_used=?, credits=?, status=? WHERE id=?";
		return $this->executeQuery($sql, true, $fieldVals);
    }
	function UpdateSubscriptionNew($campaign_id,$subscriptionId, $credit='0', $validTill, $renewDate, $freePeriodUsed, $status='Active', $discountSubscription='0.00', $discountTopup='0.00') {
        $selectedFields  = '';
        $fieldVals       = array();

        if ($validTill != "") {
            $selectedFields .= 'valid_till=?, ';
            $fieldVals[]     = $validTill;
        }
        if ($renewDate != "") {
            $selectedFields .= 'renew_date=?, ';
            $fieldVals[]     = $renewDate;
        }
        if ($discountSubscription != "") {
            $selectedFields .= ' discount_subscription=?, ';
            $fieldVals[]     = $discountSubscription;
        }
        if ($discountTopup != "") {
            $selectedFields .= ' discount_topup=?, ';
            $fieldVals[]     = $discountTopup;
        }

        $fieldVals[] = $campaign_id;
        $fieldVals[] = $freePeriodUsed;
        $fieldVals[] = $credit;
        $fieldVals[] = $status;
        $fieldVals[] = $subscriptionId;
        $sql = "UPDATE `plan_subscriptions` SET ".$selectedFields."campaign_id=?,free_period_used=?,credits=?, status=? WHERE id=?";
        return $this->executeQuery($sql, true, $fieldVals);
    }
	/**
	 * To prevent user re-entering same transaction by refreshing page
	 * @TransactionID = Transaction ID returned from payment gateway
	 * 
	 * @return boolean
	 */
	function TxnExists($TransactionID=''){
		$sql = "SELECT `transaction_id` FROM transactions WHERE transaction_id='".$TransactionID."'";
		$dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
		
		$row = $stmt->fetch();
		
		if($row[0] != ''){
			$this->closeDB($dbCon);
			return true;
		}else{
			$this->closeDB($dbCon);
			return false;
		}
	}

	function TxnSave2($txn_array){
		/*
		$transaction_result = array(
	      'UserID' => $res['custom'],
	      'SubscriptionID' => $res[''],
	      'TransactionID' => $res['txn_id'],
	      'InvoiceID' => 'csd-'.date('YYmmdd').'-'.$res['txn_id'];
	      'TransactionDate' => $res['payment_date'],
	      'PaymentDiscount' => $res['discount'],
	      'PaymentFee' => $res['payment_fee'], //mc_fee
	      'PaymentGross' => $res['payment_gross'], //mc_gross
	      'CreditPurchased' => $res['quantity'],
	      'CreditType' => $res['custom'],
	      'Currency' => $res['mc_currency'],
	    );
		*/
		$id  = $this->getRecordId();
		$sql = "INSERT INTO `transactions` (`id`, `user_id`, `subscription_id`, `transaction_id`, `invoice_id`, `transaction_date`, `payment_discount`, `payment_fee`, `payment_gross`, `credit_purchased`, `credit_type`, `currency`) VALUES (:ID, :UserID, :SubscriptionID, :TransactionID, :InvoiceID, :TransactionDate, :PaymentDiscount, :PaymentFee, :PaymentGross, :CreditPurchased, :CreditType, :Currency)";
		
	    $con = $this->connectDB();
		$q   = $con->prepare($sql);
		if($q->execute(array(':ID'=>$id, ':UserID'=>$txn_array['UserID'], ':SubscriptionID'=>$txn_array['SubscriptionID'], ':TransactionID'=>$txn_array['TransactionID'], ':InvoiceID'=>$txn_array['InvoiceID'], ':TransactionDate'=>$txn_array['TransactionDate'], ':PaymentDiscount'=>$txn_array['PaymentDiscount'], ':PaymentFee'=>$txn_array['PaymentFee'], ':PaymentGross'=>$txn_array['PaymentGross'], ':CreditPurchased'=>$txn_array['CreditPurchased'], ':CreditType'=>$txn_array['CreditType'], ':Currency'=>$txn_array['Currency']))){
			$this->closeDB($con);
			return $id;
		}else{
			$this->closeDB($con);
			return false;
		}
		/*if ($q->execute(
				array(
					':ID'=>$id, 
					':UserID'=>$txn_array['UserID'], 
					':SubscriptionID'=>$txn_array['SubscriptionID'], 
					':TransactionID'=>$txn_array['TransactionID'], 
					':InvoiceID'=>$txn_array['InvoiceID'], 
					':TransactionDate'=>$txn_array['TransactionDate'], 
					':PaymentDiscount'=>$txn_array['PaymentDiscount'], 
					':PaymentFee'=>$txn_array['PaymentFee'], 
					':PaymentGross'=>$txn_array['PaymentGross'], 
					':CreditPurchased'=>$txn_array['CreditPurchased'], 
					':CreditType'=>$txn_array['CreditType'], 
					':Currency'=>$txn_array['Currency']
				)
			)
		){
			$this->closeDB($con);
			return $id;
		}else{
			$this->closeDB($con);
			return false;
		}*/
	}

	/**
     * Creates transaction
     * 
     * @param array $txn_array Field values as array
     * 
     * @return string $id Transaction record ID
     */
    function TxnSave($txn_array)
    {
        $sql = "INSERT INTO `transactions` (
                    `id`, `user_id`, `subscription_id`, `transaction_id`, `invoice_id`, `transaction_date`, `quantity`,
                    `payment_discount`, `payment_fee`, `payment_gross`, `credit_purchased`, `credit_type`, `currency`, 
                    `payment_gateway`
                ) 
                VALUES (
                    :ID, :UserID, :SubscriptionID, :TransactionID, :InvoiceID, :TransactionDate, :Quantity, :PaymentDiscount, 
                    :PaymentFee, :PaymentGross, :CreditPurchased, :CreditType, :Currency, :PaymentGateway
                )";

        $dbCon            = $this->connectDB();
        $q                = $dbCon->prepare($sql);
        $id               = $this->getRecordId();
        
        $q->bindParam(":ID", $id, PDO::PARAM_STR);
        $q->bindParam(":UserID", $txn_array['UserID'], PDO::PARAM_STR);
        $q->bindParam(":SubscriptionID", $txn_array['SubscriptionID'], PDO::PARAM_STR);
        $q->bindParam(":TransactionID", $txn_array['TransactionID'], PDO::PARAM_STR);
        $q->bindParam(":InvoiceID", $txn_array['InvoiceID'], PDO::PARAM_STR);
        $q->bindParam(":TransactionDate", $txn_array['TransactionDate'], PDO::PARAM_STR);
        $q->bindParam(":Quantity", $txn_array['Quantity']);
        $q->bindParam(":PaymentDiscount", $txn_array['PaymentDiscount'], PDO::PARAM_STR);
        $q->bindParam(":PaymentFee", $txn_array['PaymentFee']);
        $q->bindParam(":PaymentGross", $txn_array['PaymentGross']); //amount in cents
        $q->bindParam(":CreditPurchased", $txn_array['CreditPurchased']);
        $q->bindParam(":CreditType", $txn_array['CreditType']);
        $q->bindParam(":Currency", $txn_array['Currency']);
        $q->bindParam(":PaymentGateway", $txn_array['PaymentGateway']);

        if ($q->execute()) {
            $this->closeDB($dbCon);
            return $id;
        } else {
            return '';
        }
    }

    /**
     * Retrieves a transaction info.
     * 
     * @param string $transactionId Record ID of the transaction
     */
    function getTransaction($transactionId='')
    {
        $sql = "SELECT tr.*, u.* 
            FROM `transactions` tr 
            LEFT JOIN custom16_signup_users u ON u.user_id = tr.user_id 
            WHERE `tr`.`id` = '".$transactionId."'";

        return $this->executeQuery($sql, true);
    }

    function listTransactions($UserID){
        $sql = "SELECT tr.*, `ps`.`credits` AS remaining_credits 
            FROM `transactions` tr 
            LEFT JOIN plan_subscriptions ps ON `ps`.`id` = `tr`.`subscription_id` 
            WHERE `tr`.`user_id` = '".$UserID."' ORDER BY `transaction_date` ASC";
        $dbCon = $this->connectDB();
		$stmt = $dbCon->prepare($sql);
		$stmt->execute();
        
        $res = array();
		//if($row = $stmt->rowCount()){
			while ($row = $stmt->fetch()){
				$res[] = $row;
			}
			$this->closeDB($dbCon);
			return $res;
		/*}else{
			$this->closeDB($dbCon);
			return false;
		}*/
	}

    /**
     * Get all countries
     * 
     * @return array $row 
     */
    function getCountries() {
        $sql = "SELECT id, country_name FROM countries";

        return $this->executeQuery($sql, false);
    }

    /**
     * Get country name by ID
     * @countryID = ID
     * 
     * @return $row['country_name'] = string
     */
    function getCountryByID($countryID) {
        $sql = "SELECT country_name FROM countries WHERE id='".$countryID."'";

        $dbCon=$this->connectDB();
        $stmt = $dbCon->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        $this->closeDB($dbCon);
        
        return $row['country_name'];
    }
    
    /**
     * Gather emails from a table
     * 
     * @return array Emails
     */
    function getEmailAddressesByTable($table, $emailField)
    {
        $sql = "SELECT DISTINCT(".$emailField.") AS email FROM `".$table."` WHERE ".$emailField." IS NOT NULL";
        $emailsArr = $this->executeQuery($sql, false);
        $emails    = array();
        
        foreach ($emailsArr as $email) {
            if (!empty($email['email'])) {
                $emails[$email['email']] = $email['email'];
            }
        }
        
        return $emails;
    }
    
    /**
     * Gather all email-addresses in the system, and returns which are not registered yet
     * Assuming that, email-addresses entered in the system by users are verified.
     * 
     * @return array Emails of non-registered users
     */
    function getVerifiedUnregisteredEmails()
    {
        $allEmails = array();
        $invOfCsd  = $this->getEmailAddressesByTable('custom16_csd_in_details', 'investigator_email');
        $csdOfInv  = $this->getEmailAddressesByTable('custom16_csd_in_details', 'coordinator_email');
        $miscEmail = $this->getEmailAddressesByTable('verified_email_addresses', 'email_address');
        //$invited  = $this->getEmailAddressesByTable('invite_colleagues', 'colleagues_email');
        $users     = $this->getEmailAddressesByTable('custom16_signup_users', 'email');
        
        $allEmails = array_merge($invOfCsd, $csdOfInv);
        $allEmails = array_merge($allEmails, $miscEmail);
        //$allEmails = array_merge($allEmails, $invited);
        $uniques   = array_diff($allEmails, $users); /*echo '<pre>';print_r($uniques);echo '</pre>';exit;*/
        return $uniques;
    }

    /**
     * Returns list of therapeutic areas matched with array,
     * If the array is empty, returns all therapeutic areas
     * 
     * @param array $ids ID of therapeutic areas as an array
     *
     * @return array Therapeutic Areas
     */
    function getTherapeuticAreas($ids = array())
    {
        if (empty($ids)) {
            $sql   = "SELECT id, name FROM therapeutic_areas";
        } else {
            $ids   = implode(',', $ids);
            $sql   = "SELECT id, name FROM therapeutic_areas WHERE id IN (".$ids.")";
        }
        return $this->executeQuery($sql, false);
    }

    /**
     * Returns random generated code, to use for sharing ref.
     * 
     * @param int $length Length of the code to be generated
     *
     * @return string Random generated code
     */
    function generateRefKey($length = 16)
    {
        return substr(str_shuffle("0123456789-ABCDEFGHIJKLMNOPQRSTUVWXYZ#abcdefghijklmnopqrstuvwxyz"), 0, $length);
    }

	/**
     * Send account activation link to new user
     * 
     * @param string $UserID [Email/Username of the user]
     * @param string $UserEmail [Email of the receiver]
     * @param string $ActivationKey [Encrypted token used to activate account]
     *
     * @return boolean
     */
    function SendActivationMail($UserID, $UserEmail, $ActivationKey){
		$UserPlan = $this->GetUserPlan($UserID);
		$role = $UserPlan['role'];
        $link = SITE_URL."verify-your-email?TOKEN=".$ActivationKey."&email=".urlencode($UserEmail);
        
        $body_html = $this->LoadEmailTemplate('mail_activate_account.html');
		$body_html = str_replace('%link%', $link, $body_html);
		$body_html = str_replace('%username%', $UserEmail, $body_html);
		//$body_html = str_replace('%password%', $link, $body_html);
		$body_html = str_replace('%role%', $role, $body_html);

        $to = trim($UserEmail);
        $subject = 'Please verify your email';

        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Credevo Confirmations <confirmations@credevo.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            //'text'    => $body,
            'html'    => $body_html
        ));

        $id = $result->http_response_body->id;
        if ($id) {
			return true;
		}else{
			return false;
		}
	}

	/**
     * Send transaction information to the user
     * 
     * @param string $UserEmail [Email of the receiver]
     * @param array $TransactionResult [Contains all data related to transaction]
     *
     * @return boolean
     */
    function SendTransactionResult($UserEmail, $TransactionResult){
		$body_html = $this->LoadEmailTemplate('mail_transaction_info.html'); //file_get_contents('../mail/mail_transaction_info.html');
        $body_html = str_replace('%InvoiceID%', $TransactionResult['InvoiceID'], $body_html);
		$body_html = str_replace('%CreditPurchased%', $TransactionResult['CreditPurchased'], $body_html);
		$body_html = str_replace('%PaymentGross%', $TransactionResult['PaymentGross'], $body_html);
		$body_html = str_replace('%Currency%', $TransactionResult['Currency'], $body_html);
        
        $body  = "Thanks for your payment! Here is your transaction details: \r\n\r\n";
        $body .= "Invoice Number: ".$TransactionResult['InvoiceID']."\r\n\r\n";
        $body .= "Credit Purchased: ".$TransactionResult['CreditPurchased']."\r\n\r\n";
        $body .= "Total Paid: ".$TransactionResult['PaymentGross']." ".$TransactionResult['Currency']."\r\n\r\n";
        
        $to = trim($UserEmail);
        $subject = 'Credevo: Transaction details';

        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Credevo Confirmations <confirmations@credevo.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            'text'    => $body,
            'html'    => $body_html
        ));

        $id = $result->http_response_body->id;
        if ($id) {
			return true;
		}else{
			return false;
		}
    }
    /**
     * Send transaction information to the user
     *
     * @param string $UserEmail [Email of the receiver]
     * @param array $TransactionResult [Contains all data related to transaction]
     *
     * @return boolean
     */
    function SendTransactionResultPremium($UserEmail, $TransactionResult,$UserName){
        $body_html = $this->LoadEmailTemplate('transaction_mail_production.html'); //file_get_contents('../mail/mail_transaction_info.html');
        $body_html = str_replace('%invoiceNo%', $TransactionResult['id'], $body_html);
        $body_html = str_replace('%payerName%', $UserName, $body_html);
        $body_html = str_replace('%date%', gmdate('d-M-y',$TransactionResult['created']), $body_html);
        $body_html = str_replace('%offerValue%', number_format(($TransactionResult['amount'] /100), 2, '.', ' '), $body_html);
        $body_html = str_replace('%paymentReceived%', number_format(($TransactionResult['amount'] /100), 2, '.', ' '), $body_html);
        $body_html = str_replace('%total%', number_format(($TransactionResult['amount'] /100), 2, '.', ' '), $body_html);

        $to = trim($UserEmail);
        $subject = 'Receipt of Payment - Premium Connect';

        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Credevo Confirmations <confirmations@credevo.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            'html'    => $body_html
        ));

        $id = $result->http_response_body->id;
        if ($id) {
            return true;
        }else{
            return false;
        }
    }
    /**
     * Send link to receptfrom upgrade plan payment
     *
     * @param string $userEmail [Email of the receiver]
     * @param array $info [Transaction Deteiles for Current Plan]
     *
     * @return boolean
     */
    function SendTransactionResultUpgrade($UserEmail,$info){
        $body_html = $this->LoadEmailTemplate('mail_transaction_upgrade.html'); //file_get_contents('../mail/mail_transaction_info.html');
        $body_html = str_replace('%invoiceNo%', $info['trancID'], $body_html);
        $body_html = str_replace('%payerName%', $info['userName'], $body_html);
        $body_html = str_replace('%planName%', $info['planName'], $body_html);
        $body_html = str_replace('%date%', gmdate('d-M-y',$info['created']), $body_html);
        $body_html = str_replace('%offerValue%', number_format(($info['amount']), 2, '.', ' '), $body_html);
        $body_html = str_replace('%paymentReceived%', number_format(($info['amount']), 2, '.', ' '), $body_html);
        $body_html = str_replace('%total%', number_format(($info['amount']), 2, '.', ' '), $body_html);

        $to = trim($UserEmail);
        $subject = 'Receipt of Payment - '.$info['planName'];

        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Credevo Confirmations <confirmations@credevo.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            'html'    => $body_html
        ));

        $id = $result->http_response_body->id;
        if ($id) {
            return true;
        }else{
            return false;
        }
    }
	/**
     * Send link to reset password
     * 
     * @param string $userEmail [Email of the receiver]
     * @param string $token [Encrypted token used to reset password]
     *
     * @return boolean
     */
    function SendResetPasswordLink($userEmail, $token)
    {
		$emailEncoded = urlencode($userEmail);
		$link         = SITE_URL."reset-your-password-by-email?TOKEN=".$token."&email=".$emailEncoded;
		$body         = $this->LoadEmailTemplate('mail_resetpswrd.html');
		$body         = str_replace('%email%', $emailEncoded, $body);
		$body         = str_replace('%link%', $link, $body);
		$altbody      = "Hello from Credevo! \r\n\r\nIt seems, you've forgotten your password. Use this link(".$link.") to reset it. If you've received this email in error or not changed your password, please contact us quickly at '".SITE_URL."contact?email=".$emailEncoded."'. Team Credevo!";
		
		$to           = trim($userEmail);
        $subject      = 'Credevo: Reset your password';
		
        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Credevo Confirmations <confirmations@credevo.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            'text'    => $altbody,
            'html'    => $body
        ));

        $id = $result->http_response_body->id;
        if ($id) {
			return true;
		}else{
			return false;
		}
	}
    
    /**
     * Nofify to the user after password changed successfully
     * 
     * @param string $UserEmail [Email of the receiver]
     *
     * @return boolean
     */
    function NotifyAfterResetPassword($UserEmail){
		$body = $this->LoadEmailTemplate('mail_confirm_change_pass.html');
		$body = str_replace('%email%', $UserEmail, $body);
		$body = str_replace('%SITE_URL%', SITE_URL, $body);
		$altbody = "Hello from Credevo! You've successfully changed your password. If you've received this email in error or not changed your password, please contact us quickly at 'http://credevo.com/contact?email=";
		$altbody .= $UserEmail;
		$altbody .= "'. Team Credevo!";
		
		$to = trim($UserEmail);
        $subject = 'Credevo: Your password has been changed';
		
        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Credevo Confirmations <confirmations@credevo.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            'text'    => $altbody,
            'html'    => $body
        ));

        $id = $result->http_response_body->id;
        if ($id) {
			return true;
		}else{
			return false;
		}
	}
	
	//Function for check the role with email
	function WhatsRole($email){
		
		//echo
		$sql="SELECT role FROM `custom16_signup_users` WHERE email='$email'";
		//exit;
			$dbCon=$this->connectDB();
			$stmt = $dbCon->prepare($sql);
			$stmt->execute();
			
			if ($row = $stmt->fetch())  {
				$role=$row[0];
				return $row;
				
			}else{
				return false;
				
				//echo "Not found"; exit;
			}
			
		}	



    /**
     * Send welcome email to the investigator after email verified successfully
     * 
     * @param string $UserEmail [Email of the user]
     *
     * @return boolean
     */
    function WelcomeAfterEmailVerify($UserEmail){
    	$role = $this->WhatsRole($UserEmail);
    	//print_r ($role[0]); exit;
		if ($role [0]=='investigator'){
			$body = $this->LoadEmailTemplate('welcome_investigator.html');
			$body = str_replace('%email%', $UserEmail, $body);
		} else if ($role [0]=='clinical_stud_coordinator'){
			$body = $this->LoadEmailTemplate('welcome_coordinator.html');
			$body = str_replace('%email%', $UserEmail, $body);
		} else if ($role [0]=='trial_manager'){
			$body = $this->LoadEmailTemplate('welcome_tm.html');
			$body = str_replace('%email%', $UserEmail, $body);
		} else if ($role [0]=='service_provider'){
			$body = $this->LoadEmailTemplate('welcome_sp.html');
			$body = str_replace('%email%', $UserEmail, $body);
		} else {
			return false;
		}
		$altbody = "Hi, /n/r Welcome to the fastest growing network of clinical trials, investigators and sites. Team Credevo!";
		
		$to = trim($UserEmail);
        $subject = 'Welcome to Credevo!';
		
        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Gracey C. <gracey.c@credevo.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            'text'    => $altbody,
            'html'    => $body
        ));

        $id = $result->http_response_body->id;
        if ($id) {
			return true;
		}else{
			return false;
		}
	}

	/**
     * Send link to reset password
     * 
     * @param string $UserEmail [Email of the receiver]
     * @param string $Token [Encrypted token used to verify subscription]
     *
     * @return boolean
     */
    function SendsubscribeVerifyLink($UserEmail, $Token){
		$link = SITE_URL."verify-your-subscription?TOKEN=".$Token."&email=".$UserEmail;
		$body = $this->LoadEmailTemplate('mail_subscription.html'); //file_get_contents('./mail/mail_resetpswrd.html');
		$body = str_replace('%email%', $UserEmail, $body);
		$body = str_replace('%link%', $link, $body);
		$altbody = "Welcome to CREDEVO!\r\n\r\nThank you for subscribing to our newsletter! In order to confirm the authencity of subscription, please verify your email address by clicking this link (".$link."). Alternatively, please copy the link, paste it in your browser and hit enter. If you've received this email in error or not changed your password, please contact us quickly at '".SITE_URL."contact?email=".$UserEmail."'. Team Credevo!";
		
		$to = trim($UserEmail);
        $subject = 'Please verify your subscription';
		
        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Credevo Confirmations <confirmations@credevo.com>',
            'to'      => $to,
            'bcc'     => MAILER_BCC,
            'subject' => $subject,
            'text'    => $altbody,
            'html'    => $body
        ));

        $id = $result->http_response_body->id;
        if ($id) {
			return true;
		}else{
			return false;
		}
	}

    /**
     * Nofify to helpdesk, aftr someone submitted through contact form
     * 
     * @param array $formValues Posted values by submitting the form
     * @param string $ip User IP address
     * @param string $browser Browser used by the user
     * @param string $referrer URI from which message was posted
     *
     * @return boolean
     */
    function notifyContactFormSubmission($formValues, $ip = 'Unknown', $browser = 'Unknown', $referrer = 'Unknown')
    {
        $user_info = $ip.", ".$browser.", ".$referrer;

        $body = "Hello Credevo Helpdesk, Someone has been tried to contact u. Here is the information that you need. Contact user name: ".$formValues['name'];
        $body .= "; Email: ".$formValues['email'];
        $body .= "; Message: ".$formValues['message'];
        $body .= "; System information: ".$user_info;
        
        $subject = 'Contact Form: Credevo';
        
        # Make the call to the client.
        $result = $this->mailgun->sendMessage(MAILER_DOMAIN, array(
            'from'    => 'Contact Form <no-reply@credevo.com>',
            'to'      => 'helpdesk@credevo.com',
            'subject' => $subject,
            // 'text'    => $altbody,
            'html'    => $body
        ));

        $id = $result->http_response_body->id;
        if ($id) {
            return true;
        }else{
            return false;
        }
    }

}//END OF CLASS

?>
