<?php

//localhost settings
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");

// change name of database, if needed
//define("DB_NAME", "sqltest");
define("DB_NAME", "sql-query-test");

define("ERR_CONN", "Connection attempt failed");
define("ERR_SELECT_DB", "Database selection attempt failed");
define("ERR_QUERY", "Invalid SQL query");
define("NO_DATA_FOUND", "No result found");
define("INVALID_USER_PASS", "Invalid username or password <br />Please contact your administrator");
define("TOKEN1", "@#@#@ACSD116weweddd@#@#@#");
define("TOKEN2", "@#@#@ACSD116weweddd@#@#@#");

//put name of your local instance, as applicable
define("SITE_URL", "http://localhost/cadmin/");

define("MAILGUN_API_KEY", "key-1234");
define("DEFAULT_CURRENCY", "USD");
?>